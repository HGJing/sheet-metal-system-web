# 钣金加工管理系统

> 本项目为大学大三期间从辅导员接的一个项目，项目整体开发完全由个人完成，现将其开源发表于此，以供学习和参考。

### 功能点

1. 管理钣金加工工单
2. 管理用户
3. 管理派车单
4. 与微信公众号集成

### 技术架构

> 整体采用遵循 RESTFul 风格的 API 架构  

**后台技术**
1. Spring MVC
2. Shiro 权限/会话系统
3. MyBatis 数据库持久化
4. 采用 MySQL 作为数据库  

**前端技术(PC端)** 
1. 动态页面：JSP
2. 数据渲染：Jquery
3. 页面UI：EasyUI

**前端技术(H5端-微信集成)**
1. 数据渲染：AngularJS
2. 页面UI：SUI
3. 字体库：font-awesome-4.7.0

### 测试账号
|用户名|密码|
|-|-|
|`admin`|`123456`|

### 其它
开源地址：[https://gitee.com/HGJing/sheet-metal-system-web](https://gitee.com/HGJing/sheet-metal-system-web)  
演示地址：[https://palerock.cn/sms](https://palerock.cn/sms)

### 疑问或建议请评论