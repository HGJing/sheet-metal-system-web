package web.sms.core.exception;

import web.sms.core.pojo.ErrorInfo;

/**
 * Created by Eoly on 2017/7/15.
 */
public class AttributeCheckFailureException extends ServiceException {

    private String attributeName;

    public AttributeCheckFailureException(ErrorInfo errorInfo, String attributeName, Object[] args) {
        super(errorInfo, args);
        this.attributeName = attributeName;
    }

    public AttributeCheckFailureException(String message, String attributeName) {
        super(message);
        this.attributeName = attributeName;
    }

    public AttributeCheckFailureException(ErrorInfo errorInfo, String attributeName) {
        super(errorInfo);
        this.attributeName = attributeName;
    }

    public AttributeCheckFailureException(String message, Integer error, String attributeName) {
        super(message, error);
        this.attributeName = attributeName;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
}
