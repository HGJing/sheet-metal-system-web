package web.sms.core.exception;

import web.sms.core.pojo.ErrorInfo;
import web.sms.core.utils.MessageFormatter;

/**
 * Created by Eoly on 2017/7/12.
 */
public class ServiceException extends RuntimeException {

    protected Integer error;

    public ServiceException(String message) {
        super(message);
        this.error = -1;
    }

    public ServiceException(ErrorInfo errorInfo) {
        super(errorInfo.getMsg());
        if (errorInfo.getError() != null) {
            this.error = errorInfo.getError();
        } else {
            this.error = -1;
        }
    }

    public ServiceException(ErrorInfo errorInfo, Object... args) {
        super(MessageFormatter.arrayFormat(errorInfo.getMsg(), args).getMessage());
        if (errorInfo.getError() != null) {
            this.error = errorInfo.getError();
        } else {
            this.error = -1;
        }
    }

    public ServiceException(String message, Integer error) {
        super(message);
        this.error = error;
    }

    public Integer getError() {
        return error;
    }
}
