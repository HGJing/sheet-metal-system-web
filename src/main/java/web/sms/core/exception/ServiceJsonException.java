package web.sms.core.exception;

import web.sms.core.pojo.ErrorInfo;

/**
 * Created by Eoly on 2017/7/12.
 */
public class ServiceJsonException extends ServiceException {

    public ServiceJsonException(ErrorInfo errorInfo, Object... args) {
        super(errorInfo, args);
    }

    public ServiceJsonException(String message, Integer error) {
        super(message, error);
    }

    public ServiceJsonException(String message) {
        super(message);
    }

    public ServiceJsonException(ErrorInfo errorInfo) {
        super(errorInfo);
    }
}
