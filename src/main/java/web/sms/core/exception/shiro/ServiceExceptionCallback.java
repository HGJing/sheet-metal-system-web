package web.sms.core.exception.shiro;

import cn.palerock.core.shiro.auditor.ExceptionCallBack;
import cn.palerock.core.shiro.useradmin.auditor.UserAuditor;
import web.sms.core.exception.ServiceException;
import web.sms.mvc.constant.UserConstant;

/**
 * 时间： 2017/12/14
 * 在调用UserAuditor检查权限时的默认回调函数
 *
 * @author Eoly
 */
public class ServiceExceptionCallback implements ExceptionCallBack {

    @Override
    public void onException(Integer code, Object... objects) {
        // object[0]为触发异常的用户名 object[1]是没有的相关权限集合
        if (code.equals(UserAuditor.NOT_HAVE_PERMISSION)) {
            // 没有单一权限
            throw new ServiceException(UserConstant.NOT_HAVE_PERMISSION_ERROR, objects);
        } else if (code.equals(UserAuditor.NOT_HAVE_PERMISSIONS)) {
            // 没有系列权限
            throw new ServiceException(UserConstant.NOT_HAVE_PERMISSIONS_ERROR, objects);
        } else if (code.equals(UserAuditor.NOT_HAVE_ROLE)) {
            // 没有单一角色
            throw new ServiceException(UserConstant.NOT_HAVE_ROLE_ERROR, objects);
        } else if (code.equals(UserAuditor.NOT_HAVE_ROLES)) {
            // 没有系列角色
            throw new ServiceException(UserConstant.NOT_HAVE_ROLES_ERROR, objects);
        } else if (code.equals(UserAuditor.USER_NOT_LOGIN)) {
            // 没有登录
            throw new ServiceException(UserConstant.NOT_LOGIN_ERROR, objects);
        }
    }
}
