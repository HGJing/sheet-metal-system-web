package web.sms.core.pojo;

import web.sms.core.utils.MessageFormatter;

/**
 * 时间： 2017/12/14
 *
 * @author Eoly
 */
public class ErrorInfo {
    private Integer error;
    private String msg;

    public ErrorInfo(String msg, Integer error) {
        this.error = error;
        this.msg = msg;
    }

    public ErrorInfo(ErrorInfo errorInfo, String... args) {
        this(MessageFormatter.arrayFormat(errorInfo.getMsg(), args).getMessage(), errorInfo.getError());
    }

    public ErrorInfo(ErrorInfo errorInfo, Integer error, String... args) {
        this(MessageFormatter.arrayFormat(errorInfo.getMsg(), args).getMessage(), error);
    }

    public ErrorInfo() {
    }

    public Integer getError() {
        return error;
    }

    public String getMsg() {
        return msg;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return this.msg;
    }
}
