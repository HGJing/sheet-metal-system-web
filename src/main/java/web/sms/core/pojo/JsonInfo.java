package web.sms.core.pojo;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by Eoly on 2017/6/16.
 */
public class JsonInfo extends JSONObject {

    public JsonInfo() {
    }

    public JsonInfo(Integer error, String msg) {
        this.put("error", error);
        this.put("msg", msg);
    }

    public void addAttribute(String key, Object value) {
        this.put(key, value);

    }

}
