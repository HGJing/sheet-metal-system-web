package web.sms.core.pojo;

public class UrlFilter {
    private Integer filterId;

    private String filterName;

    private String filterDescription;

    private String filterRoles;

    private String filterPerms;

    private Byte filterAvailable = 1;

    private String filterUrl;

    private String configuration;

    public UrlFilter() {
    }

    public UrlFilter(String configuration) {
        this.setConfiguration(configuration);
    }

    public Integer getFilterId() {
        return filterId;
    }

    public void setFilterId(Integer filterId) {
        this.filterId = filterId;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName == null ? null : filterName.trim();
    }

    public String getFilterDescription() {
        return filterDescription;
    }

    public void setFilterDescription(String filterDescription) {
        this.filterDescription = filterDescription == null ? null : filterDescription.trim();
    }

    public String getFilterRoles() {
        return filterRoles;
    }

    public void setFilterRoles(String filterRoles) {
        this.filterRoles = filterRoles == null ? null : filterRoles.trim();
    }

    public String getFilterPerms() {
        return filterPerms;
    }

    public void setFilterPerms(String filterPerms) {
        this.filterPerms = filterPerms == null ? null : filterPerms.trim();
    }

    public Byte getFilterAvailable() {
        return filterAvailable;
    }

    public void setFilterAvailable(Byte filterAvailable) {
        this.filterAvailable = filterAvailable;
    }

    public String getFilterUrl() {
        return filterUrl;
    }

    public void setFilterUrl(String filterUrl) {
        this.filterUrl = filterUrl == null ? null : filterUrl.trim();
    }

    public void setConfiguration(String configuration) {
        this.configuration = configuration;
        parserAll();
    }

    /**
     * 根据配置文字初始化实体
     */
    private void parserAll() {
        String configuration = this.configuration;
        String[] results = configuration.split(" ");
        int count = 0;
        for (String result : results) {
            if ("".equals(result)) {
                continue;
            }
            switch (count++) {
                case 0: {
                    this.setFilterUrl(result);
                    break;
                }
                case 1: {
                    this.setFilterPerms(result);
                    break;
                }
                case 2:
                    this.setFilterRoles(result);
                default:
            }
        }
    }
}