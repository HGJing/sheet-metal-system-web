package web.sms.core.servlet.filter;

import web.sms.utils.HttpUtils;
import web.sms.utils.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 时间： 2018/1/25
 * [已弃用]
 *
 * @author Eoly
 */
public class WechatCheckFilter implements Filter {
    private final String token = "qazwsxedc1234";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = HttpUtils.toHttp(servletRequest);
        HttpServletResponse resp = HttpUtils.toHttp(servletResponse);
        String signature = req.getParameter("signature");
        String timestamp = req.getParameter("timestamp");
        String nonce = req.getParameter("nonce");
        String echostr = req.getParameter("echostr");
        if (StringUtils.checkSignature(token, signature, timestamp, nonce)) {
            HttpUtils.outPrint(resp, echostr);
        } else {
            HttpUtils.outPrint(resp, "wrong request");
        }
    }

    @Override
    public void destroy() {

    }
}
