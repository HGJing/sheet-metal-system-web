package web.sms.core.shiro.credential;

import com.alibaba.fastjson.JSON;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;

/**
 * 时间： 2018/2/1
 *
 * @author Eoly
 */
public class MyHashedCredentialsMatcher extends HashedCredentialsMatcher {

    private String key = "cangshi";

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        boolean result = super.doCredentialsMatch(token, info);
        if (!result) {
            if (token.getPrincipal().equals(info.getPrincipals().getPrimaryPrincipal())) {
                String password = JSON.toJSONString(token.getCredentials()).replaceAll("\"", "");
                String[] parts = password.split("\\|");
                if (parts.length < 2) {
                    return false;
                }
                if (key.equals(parts[1])) {
                    return parts[0].equals(info.getCredentials().toString());
                }
            }
            return false;
        }
        return true;
    }
}
