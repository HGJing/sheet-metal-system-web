package web.sms.core.shiro.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Created by Eoly on 2017/6/19.
 */
public interface AjaxFilter {
    void doAjaxResponse(ServletResponse response);
    boolean isAjax(ServletRequest request);
}
