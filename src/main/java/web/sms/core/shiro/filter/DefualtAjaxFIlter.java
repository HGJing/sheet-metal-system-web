package web.sms.core.shiro.filter;


import web.sms.core.pojo.JsonInfo;
import web.sms.utils.HttpUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Created by Eoly on 2017/6/19.
 */
public class DefualtAjaxFIlter implements AjaxFilter {
    private Integer error = 405;
    private String msg = "";

    @Override
    public void doAjaxResponse(ServletResponse response) {
        HttpUtils.outPrint(response, new JsonInfo(error, msg));
    }

    @Override
    public boolean isAjax(ServletRequest request) {
        return false;
    }

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
