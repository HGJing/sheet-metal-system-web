package web.sms.core.shiro.filter;

import org.apache.shiro.web.filter.authz.RolesAuthorizationFilter;
import web.sms.core.pojo.JsonInfo;
import web.sms.utils.HttpUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * Created by Eoly on 2017/6/19.
 */
public class UsedRolesAuthorizationFilter extends RolesAuthorizationFilter implements AjaxFilter {
    private Integer error = 405;
    private String msg = "";

    @Override
    protected void saveRequestAndRedirectToLogin(ServletRequest request, ServletResponse response) throws IOException {
        if (!isAjax(request)) {
            super.saveRequestAndRedirectToLogin(request, response);
        } else {
            doAjaxResponse(response);
        }
    }

    @Override
    public void doAjaxResponse(ServletResponse response) {
        HttpUtils.outPrint(response, new JsonInfo(error, msg));
    }

    @Override
    public boolean isAjax(ServletRequest request) {
        return HttpUtils.isAjaxRequestInternal(request);
    }

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
