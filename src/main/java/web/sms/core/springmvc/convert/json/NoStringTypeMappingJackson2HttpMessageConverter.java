package web.sms.core.springmvc.convert.json;

import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * 时间： 2018/1/29
 *
 * @author Eoly
 */
public class NoStringTypeMappingJackson2HttpMessageConverter extends MappingJackson2HttpMessageConverter {
    @Override
    protected void writeInternal(Object object, Type type, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        if (object instanceof String) {
            outputMessage.getBody().write(((String) object).getBytes());
            outputMessage.getBody().close();
            return;
        }
        super.writeInternal(object, type, outputMessage);
    }
}
