package web.sms.mvc.constant;


import web.sms.core.pojo.ErrorInfo;

/**
 * 时间： 2017/12/14
 *
 * @author Eoly
 */
public class DepartureConstant {
    public static final ErrorInfo SERVER_RUN_SQL_FAILURE = new ErrorInfo("服务器执行sql语句发生错误：{}", 500);
    public static final String EDIT_DEPARTURE_FAILURE = "修改派车单失败";
    public static final String EMPTY_DEPARTURES_LIST = "没有查询到符合条件的派车单";
    public static final String ADD_DEPARTURE_FAILURE = "添加派车单失败";
    public static final String DELETE_DEPARTURE_FAILURE = "删除派车单失败";
    public static final ErrorInfo ADD_DEPARTURE_ERROR = new ErrorInfo("添加派车单失败:{}", 403);
    public static final String DRIVER_IS_NOT_EXIST = "指定司机不存在";
    public static final String DEPARTURE_NOT_EXIST = "指定派车单不存在";
    public static final ErrorInfo DISPENSE_DRIVER_ERROR = new ErrorInfo("分配司机失败：{}", 403);
    public static final String DEPARTURE_IS_NOT_IN_UNANSWERED = "车单状态不是未接单，无法分配司机";
    public static final String DISPENSE_DRIVER_FAILURE = "分配司机失败";
    public static final ErrorInfo MODIFY_STATUS_ERROR = new ErrorInfo("确认派车单状态失败:{}", 403);
    public static final String IS_NOT_UNANSWERED_STATUS = "当前派车单非未接单状态";
    public static final String MAKE_SURE_RECEIPT_FAILURE = "确定派车单已接单失败";
    public static final ErrorInfo DO_DEPARTURE_ERROR = new ErrorInfo("派车单发车失败：{}", 400);
    public static final ErrorInfo DO_RECEIVE_ERROR = new ErrorInfo("派车单回厂收货失败：{}", 400);
    public static final String ARRIVAL_STATUS_EXCEPT = "操作的派车单状态不是到货";
    public static final String DELIVERING_STATUS_EXCEPT = "操作的派车单状态不是送货中";
    public static final String UNANSWERED_STATUS_EXCEPT = "操作的派车单状态不是未接单";
    public static final String RECEIPT_STATUS_EXCEPT = "操作的派车单状态不是已接单";
    public static final String DO_DEPARTURE_FAILURE = "派车单发车失败";
    public static final String DO_RECEIVE_FAILURE = "派车单回厂收货失败";
    public static final ErrorInfo OPTION_ERROR = new ErrorInfo("操作失败：{}", 403);
    public static final ErrorInfo FILL_IN_RESULT_ERROR = new ErrorInfo("填写超时原因失败:{}", 403);
    public static final String DEPARTURE_DRIVER_IS_NOT_MATCH = "您不是该派车单的司机，无法填写";
    public static final String DEPARTURE_IS_NOT_EXCEPT_TO_FILL_RESULT = "当前车单不能填写超时原因，可能因为尚未回厂收获";
    public static final String RESULT_IS_NOT_TO_BE_EMPTY = "超时原因不能为空";
    public static final String FILL_RESULT_FAILURE = "填写超时原因失败";
    public static final ErrorInfo FIRST_CUSTOMER_CAN_NOT_EMPTY = new ErrorInfo("首达客户不能为空", 403);
    public static final ErrorInfo HAVE_TO_SET_RECEIVER = new ErrorInfo("必须设置回厂收货人", 403);
    public static final String RECEIVER_CAN_NOT_FIND = "没有找到指定的回厂收货人";
    public static final String CUSTOMER_CAN_NOT_FIND = "没有找到指定的客户";
    public static final ErrorInfo FIND_DEPARTURE_ERROR = new ErrorInfo("查询指定派车单失败", 404);
    public static final String DEPARTURE_NOT_OVERTIME = "当前派车并未超时";
    public static final ErrorInfo GET_DRIVER_INFO_ERROR = new ErrorInfo("获取司机信息失败：{}", 403);
    public static final String NOT_EXIST_IN_THIS_TIME = "在规定的时间没有相关司机的信息";
}
