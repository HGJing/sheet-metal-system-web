package web.sms.mvc.constant;

import web.sms.core.pojo.ErrorInfo;

/**
 * 时间： 2017/12/14
 *
 * @author Eoly
 */
public class ProcessConstant {

    public static final ErrorInfo NOT_HAVE_PROCESS_ERROR = new ErrorInfo("没有找到任何加工流程",404);
}
