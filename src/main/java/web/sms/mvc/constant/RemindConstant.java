package web.sms.mvc.constant;

import web.sms.core.pojo.ErrorInfo;

/**
 * 时间： 2017/12/14
 *
 * @author Eoly
 */
public class RemindConstant {

    public static final ErrorInfo CHECK_REMIND_FAILURE = new ErrorInfo("核对提醒实体失败：{}", 500);
    public static final ErrorInfo ERROR_REMIND_TYPE = new ErrorInfo("指定了错误的提醒类型", 500);
    public static final ErrorInfo FAIL_TO_BUILD_REMIND = new ErrorInfo("执行操作失败因为生成提醒失败", 500);
    public static final ErrorInfo REMIND_IS_NOT_BELONG_TO_YOU = new ErrorInfo("操作失败，该提醒不是您的提醒", 403);
    public static final ErrorInfo READ_REMIND_FAILURE = new ErrorInfo("设置该提醒为已读失败，服务器出现错误", 500);
}
