package web.sms.mvc.constant;

import web.sms.core.pojo.ErrorInfo;

/**
 * 时间： 2017/12/14
 *
 * @author Eoly
 */
public class UserConstant {
    public static final ErrorInfo USERNAME_OR_PASSWORD_ERROR = new ErrorInfo("用户名或密码错误", 403);
    public static final ErrorInfo LOGIN_FAILURE_ERROR = new ErrorInfo("登录失败", 403);
    public static final ErrorInfo USER_LOCKED_ERROR = new ErrorInfo("该用户已被冻结", 403);
    public static final ErrorInfo NULL_USER_PROPERTY_ERROR = new ErrorInfo("该用户的[{}]属性为空", 403);
    public static final ErrorInfo USER_NOT_EXIST_ERROR = new ErrorInfo("欲操作用户不存在", 404);
    public static final ErrorInfo NOT_HAVE_PERMISSION_ERROR = new ErrorInfo("用户[{}]没有权限:{}，请联系管理员", 403);
    public static final ErrorInfo NOT_HAVE_PERMISSIONS_ERROR = new ErrorInfo("用户[{}]没有相应权限(或):{}，请联系管理员", 403);
    public static final ErrorInfo NOT_HAVE_ROLE_ERROR = new ErrorInfo("用户[{}]没有角色:{}，请联系管理员", 403);
    public static final ErrorInfo NOT_HAVE_ROLES_ERROR = new ErrorInfo("用户[{}]没有相应角色(或):{}，请联系管理员", 403);
    public static final ErrorInfo NOT_LOGIN_ERROR = new ErrorInfo("用户尚未登录", 403);
    public static final ErrorInfo CAN_NOT_CREATE_ADMIN_USER = new ErrorInfo("不能添加管理员用户", 403);
    public static final ErrorInfo CAN_NOT_TURN_TO_ADMIN_USER = new ErrorInfo("不能转换角色为管理员用户", 403);
    public static final ErrorInfo SERVER_RUN_SQL_FAILURE = new ErrorInfo("服务器执行sql语句发生错误：{}", 500);
    public static final ErrorInfo NOT_EXIST_ROLE_ERROR = new ErrorInfo("欲给用户添加的角色：[{}]不存在", 404);
    public static final String ADDING_USER_FAILURE = "添加用户失败";
    public static final String EDITING_USER_FAILURE = "编辑用户失败";
    public static final String EDITING_USER_PASSWORD_FAILURE = "修改用户密码失败";
    public static final ErrorInfo FAIL_TO_EDIT_USER_PASSWORD_ERROR = new ErrorInfo("修改用户密码失败：{}", 403);
    public static final String OLD_PASSWORD_IS_NULL = "旧密码不能为空";
    public static final String CAN_NOT_MATCH_ODL_PASSWORD = "旧密码不正确";
    public static final String NEW_PASSWORD_IS_NULL = "新密码不能为空";
    public static final ErrorInfo FREEZING_USER_FAIL_ERROR = new ErrorInfo("冻结用户失败：{}", 403);
    public static final ErrorInfo UNFREEZING_USER_FAIL_ERROR = new ErrorInfo("解冻用户失败：{}", 403);
    public static final String ADMIN_USER_IS_INOPERABLE = "该操作不能作用于管理员";
    public static final String USER_IS_FREEZING_OR_NOT_EXIST = "该用户已被冻结或该用户不存在";
    public static final String USER_IS_UNFREEZING_OR_NOT_EXIST = "该用户未被冻结或该用户不存在";
    public static final String FREEZING_USER_FAILURE = "冻结用户失败";
    public static final String UNFREEZING_USER_FAILURE = "解冻用户失败";
    public static final String DELETE_USER_FAILURE = "删除用户失败";
    public static final ErrorInfo EMPTY_USERS_ERROR = new ErrorInfo("当前没有用户", 404);
    public static final ErrorInfo DELETING_USER_FAIL_ERROR = new ErrorInfo("删除用户失败：{}", 403);
    public static final ErrorInfo CAN_NOT_MODIFY_ROLE_OF_ADMIN_USER = new ErrorInfo("超级管理员的角色不能被修改", 403);
    public static final ErrorInfo CUSTOMER_NOT_EXIST_ERROR = new ErrorInfo("指定客户不存在", 404);
    public static final ErrorInfo PRODUCTION_MANAGER_NOT_EXIST_ERROR = new ErrorInfo("指定生产管理不存在", 404);
    public static final ErrorInfo SALES_MANAGER_NOT_EXIST_ERROR = new ErrorInfo("指定销售管理不存在", 404);
    public static final String USER_IS_NOT_CUSTOMER = "指定客户不存在";
    public static final String USER_IS_NOT_PRODUCTION_MANAGER = "指定生产管理不存在";
    public static final String USER_IS_NOT_SALES_MANAGER = "指定销售管理不存在";
    public static final ErrorInfo NOT_FIND_MATCH_USER_ERROR = new ErrorInfo("没有查询到匹配的用户", 404);
    public static final String USER_IS_NOT_WORKER = "该用户不是工人";
    public static final String USER_IS_NOT_LEADER = "您不是该任务所属小组的组长";
    public static final ErrorInfo WRONG_ROLE_NAME = new ErrorInfo("错误的用户角色", 403);
}
