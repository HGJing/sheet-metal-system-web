package web.sms.mvc.constant;

import web.sms.core.pojo.ErrorInfo;

/**
 * 时间： 2017/12/14
 *
 * @author Eoly
 */
public class WorkerGroupConstant {
    public static final ErrorInfo GROUP_ID_IS_NOT_EXIST_ERROR = new ErrorInfo("指定小组的id[{}]不存在", 404);
    public static final ErrorInfo WORKER_IS_NOT_IN_THIS_GROUP = new ErrorInfo("指定用户[id={}]不在该小组[id={}]内", 403);
    public static final ErrorInfo SERVER_RUN_SQL_FAILURE = new ErrorInfo("服务器执行sql语句发生错误：{}", 500);
    public static final String EDITING_GROUP_FAILURE = "修改工人组内容失败";
    public static final ErrorInfo USER_NOT_HAS_WORKER_ROLE = new ErrorInfo("指定用户并不具有工人角色", 403);
    public static final ErrorInfo WORKER_NOT_EXIST = new ErrorInfo("指定组工人没有找到", 403);
    public static final ErrorInfo EMPTY_GROUP_ID_OR_WORKER_ID = new ErrorInfo("未指定工人组或未指定欲加入工人组的工人id", 403);
    public static final ErrorInfo PROCESS_NOT_EXIST = new ErrorInfo("指定任务流程不存在", 403);
    public static final ErrorInfo LEADER_HAS_NOT_PROCESS_ID = new ErrorInfo("该团组长没有指定加工流程，无法执行该操作", 403);
    public static final ErrorInfo NOT_EXIST_CAPTAINS = new ErrorInfo("没有找到工人小组组长", 404);
    public static final ErrorInfo GROUP_OR_LEADER_NOT_EXIST = new ErrorInfo("工人小组或工人组长不存在", 404);
}
