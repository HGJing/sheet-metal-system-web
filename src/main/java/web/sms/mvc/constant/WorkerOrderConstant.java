package web.sms.mvc.constant;

import web.sms.core.pojo.ErrorInfo;

/**
 * 时间： 2017/12/14
 *
 * @author Eoly
 */
public class WorkerOrderConstant {
    public static final ErrorInfo ORDER_NOT_EXIST_ERROR = new ErrorInfo("指定工单不存在", 404);
    public static final ErrorInfo NULL_ORDER_PROPERTY_ERROR = new ErrorInfo("该工单的[{}]属性为空", 403);
    public static final ErrorInfo FAIL_TO_ADD_ORDER_ERROR = new ErrorInfo("添加工单失败：{}", 403);
    public static final String PRICE_CAN_NOT_BE_NEGATIVE = "工单金额不能为负";
    public static final String PROCESS_NOT_EXIST = "欲添加的加工流程不存在";
    public static final String ADD_ORDER_FAILURE = "添加工单失败";
    public static final ErrorInfo SERVER_RUN_SQL_FAILURE = new ErrorInfo("服务器执行sql语句发生错误：{}", 500);
    public static final String PROCESS_NOT_SELECTED = "未指定相应流程";
    public static final ErrorInfo EMPTY_ORDER_LIST_EXCEPTION = new ErrorInfo("工单列表暂时为空", 404);
    public static final ErrorInfo NO_ORDERS_DISPLAYED = new ErrorInfo("没有可展示的工单", 404);
    public static final String DELETE_ORDER_FAILURE = "删除工单失败";
    public static final ErrorInfo CURRENT_USER_IS_NOT_RIGHT_CUSTOMER_ERROR = new ErrorInfo("当前用户非该工单对应的客户，无法执行该操作", 403);
    public static final String EDIT_ORDER_FAILURE = "修改工单失败";
    public static final ErrorInfo CUSTOMER_MESSAGE_IS_EMPTY_ERROR = new ErrorInfo("客户留言不能为空", 403);
    public static final String EDIT_CUSTOMER_MESSAGE_FAILURE = "修改用户留言失败";
    public static final ErrorInfo CHANGE_ORDER_PAYMENT_STATUS_ERROR = new ErrorInfo("修改工单支付状态失败：{}", 403);
    public static final String ORDER_IS_PAID = "工单已支付";
    public static final String CHANGE_ORDER_PAYMENT_STATUS_FAILURE = "修改工单状态失败";
    public static final ErrorInfo ORDER_TASK_NOT_EXIST_ERROR = new ErrorInfo("指定的工单任务流程不存在", 403);
    public static final String DISTRIBUTION_TASK_FAILURE = "分配任务失败";
    public static final ErrorInfo GROUP_IS_NOT_EXIST_ERROR = new ErrorInfo("指定的工人团不存在", 403);
    public static final ErrorInfo PROCESS_IS_NOT_MATCH_ERROR = new ErrorInfo("指定的工人小组和指定的加工任务在流程上不匹配", 403);
    public static final ErrorInfo TASK_IS_DISTRIBUTED_ERROR = new ErrorInfo("该任务流程已分配任务小组", 403);
    public static final ErrorInfo FAIL_TO_FIND_TASKS_ERROR = new ErrorInfo("查询任务流程失败:{}", 403);
    public static final ErrorInfo TASKS_FOR_LEADER_IS_EMPTY_ERROR = new ErrorInfo("当前工人没有所负责的任务，无法完成任务", 403);
    public static final ErrorInfo TASK_ID_IS_REQUIRE_ERROR = new ErrorInfo("完成任务必须指定任务id", 403);
    public static final ErrorInfo TASKS_FOR_GROUP_IS_EMPTY_ERROR = new ErrorInfo("当前小组没有负责加工的任务流程，无法完成任务", 403);
    public static final ErrorInfo FAIL_TO_FINISH_TASK_ERROR = new ErrorInfo("完成任务失败：{}", 403);
    public static final String TASK_IS_NOT_EXPECTED = "该任务的前置任务未完成或该小组还有更高级优先级的任务尚未完成";
    public static final String TASKS_IN_THIS_GROUP_ARE_NOT_TO_BE_FINISH = "该小组的任务都处于不可完成的状态";
    public static final String TASK_HAS_FINISHED = "该任务已完成，无需重复操作";
    public static final String FINISH_TASK_FAILURE = "完成任务失败";
    public static final ErrorInfo THERE_IS_NO_ORDER_ID = new ErrorInfo("未指定工单id", 403);
    public static final ErrorInfo THERE_IS_NO_ORDER_PRIORITY = new ErrorInfo("未指定工单优先级", 403);
    public static final String CHANGE_ORDER_PRIORITY_FAILURE = "修改工单优先级失败";
    public static final ErrorInfo THERE_IS_NO_ORDER_IS_QUICK = new ErrorInfo("未指定该工单的是否快速属性", 403);
    public static final String CHANGE_ORDER_IS_QUICK_FAILURE = "修改工单的是否快速属性失败";
    public static final String REMIND_CUSTOMER_ORDER_BIRTH = "您所申请的工单[{}]已创建。";
    public static final String REMIND_CUSTOMER_ORDER_DELETE = "您的工单[{}]已被管理员删除。";
    public static final String REMIND_CUSTOMER_ORDER_UPDATE = "您的工单[{}]状态已更新:{}。";
    public static final String REMIND_MANAGER_ORDER_UPDATE = "您所负责工单[{}]状态已更新:{}。";
    public static final String REMIND_MANAGER_ORDER_DELETE = "您所负责的工单[{}]已被管理员删除。";
    public static final String REMIND_MANAGER_ORDER_BIRTH = "您所负责的工单[{}]已创建。";
    public static final String REMIND_WORKER_TASK_TO_DO = "工单[{}]的[{}]任务已分配到您的小组";
    public static final String REMIND_TASK_CAN_BE_FINISH = "工单[{}]的[{}]流程已完成，您负责的下一流程正在进行中";
    public static final String REMIND_ORDER_MESSAGE_CHANGED = "工单[{}]的留言已发生改变";
    public static final ErrorInfo WORK_CAN_NOT_SENDING_ERROR = new ErrorInfo("该工单的发货流程不能完成，因为该工单对应的客户未付款", 403);
    public static final ErrorInfo GET_ORDER_INFO_ERROR = new ErrorInfo("获取工人加工工单统计信息失败：{}", 403);
    public static final String WORKER_IS_NOT_EXIST = "查询的工人不存在";
    public static final String NOT_EXIST_IN_THIS_TIME = "在规定的时间没有相关工人的信息";
}
