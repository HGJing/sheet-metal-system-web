package web.sms.mvc.controller;

import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import web.sms.core.exception.ServiceException;
import web.sms.core.pojo.JsonInfo;
import web.sms.mvc.constant.UserConstant;
import web.sms.mvc.entity.Departure;
import web.sms.mvc.service.DepartureService;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 时间： 2018/1/4
 *
 * @author Eoly
 */
@Controller
@RequestMapping("/departure")
public class DepartureController {

    private final DepartureService departureService;

    @Autowired
    public DepartureController(DepartureService departureService) {
        this.departureService = departureService;
    }

    /**
     * 添加派车单
     *
     * @param departure
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public JsonInfo addDeparture(Departure departure, String departureFirstCustomerInfo) {
        if (StringUtils.isNumeric(departureFirstCustomerInfo)) {
            departure.setDepartureFirstCustomerId(Integer.valueOf(departureFirstCustomerInfo));
        } else {
            departure.setDepartureFirstCustomerName(departureFirstCustomerInfo);
        }
        departureService.addDeparture(departure);
        return new JsonInfo(0, "添加成功");
    }

    /**
     * 查询派车单列表
     *
     * @param page
     * @param rows
     * @param roleName
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{roleName}/list", method = RequestMethod.GET)
    public JsonInfo findDeparture(
            Integer page, Integer rows,
            @PathVariable String roleName
    ) {
        PageInfo<Departure> pageInfo;
        if (roleName.equals("clerk")) {
            pageInfo = departureService.findClerkDepartures(page, rows);
        } else if (roleName.equals("driver")) {
            pageInfo = departureService.findDriverDepartures(null, page, rows, null, null);
        } else {
            throw new ServiceException(UserConstant.WRONG_ROLE_NAME);
        }
        JsonInfo jsonInfo = new JsonInfo(0, "查询成功");
        jsonInfo.addAttribute("pageInfo", pageInfo);
        return jsonInfo;
    }

    /**
     * 派车单分配司机
     *
     * @param departureId
     * @param driverId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{departureId}/dispense", method = RequestMethod.POST)
    public JsonInfo doDispenseDriverForDeparture(
            @PathVariable Integer departureId,
            @RequestParam Integer driverId
    ) {
        departureService.doDispenseDriverForDeparture(departureId, driverId);
        return new JsonInfo(0, "分配成功");
    }

    /**
     * 确定修改派车单状态为已接单
     *
     * @param departureId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{departureId}/make-receipt", method = RequestMethod.POST)
    public JsonInfo makeSureDepartureReceipt(
            @PathVariable Integer departureId
    ) {
        departureService.makeSureDepartureReceipt(departureId);
        return new JsonInfo(0, "确定成功");
    }

    /**
     * 派车单确定发车
     *
     * @param departureId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{departureId}/departing", method = RequestMethod.POST)
    public JsonInfo doDeparting(
            @PathVariable Integer departureId
    ) {
        departureService.doDeparting(departureId);
        return new JsonInfo(0, "发车成功");
    }


    /**
     * 派车单确认回厂收货
     *
     * @param departureId
     * @param receiverName
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{departureId}/receive", method = RequestMethod.POST)
    public JsonInfo doReceive(
            @PathVariable Integer departureId,
            @RequestParam String receiverName
    ) {
        departureService.doReceive(departureId, receiverName);
        return new JsonInfo(0, "回厂收货成功");
    }

    /**
     * 填写超时原因
     *
     * @param departureId 派车单id
     * @param result      超时原因
     * @return json信息
     */
    @ResponseBody
    @RequestMapping(value = "/{departureId}/fill-result", method = RequestMethod.POST)
    public JsonInfo doFillInResultOfLater(
            @PathVariable Integer departureId,
            @RequestParam String result
    ) {
        departureService.doFillInResultOfLater(departureId, result);
        return new JsonInfo(0, "填写超时原因成功");
    }

    /**
     * 获取派车单详情接口
     *
     * @param departureId 派车单id
     * @return json信息
     */
    @ResponseBody
    @RequestMapping(value = "/{departureId}/detail", method = RequestMethod.GET)
    public JsonInfo findDepartureDetailInfo(
            @PathVariable Integer departureId
    ) {
        Departure departure = departureService.findDepartureDetailInfo(departureId);
        JsonInfo jsonInfo = new JsonInfo(0, "获取派车单详情成功");
        jsonInfo.put("departure", departure);
        return jsonInfo;
    }

    /**
     * 删除派车单
     *
     * @param departureId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{departureId}/delete", method = RequestMethod.POST)
    public JsonInfo deleteDeparture(
            @PathVariable Integer departureId
    ) {
        departureService.deleteDeparture(departureId);
        return new JsonInfo(0, "删除派车单成功");
    }

    /**
     * 获取派车单详情接口
     *
     * @param departureId 派车单id
     * @return json信息
     */
    @RequestMapping(value = "/{departureId}/detail.model", method = RequestMethod.GET)
    public String findDepartureDetailInfoPage(
            @PathVariable Integer departureId,
            Model model
    ) {
        Departure departure = departureService.findDepartureDetailInfo(departureId);
        model.addAttribute("departure", departure);
        if (departure.getDepartureStartTime() != null) {
            long preEndTime = departure.getDepartureStartTime().getTime()
                    + (long) (departure.getDepartureCostTime() * 3600000);
            model.addAttribute("preEndTime", new Date(preEndTime));
            if (departure.getDepartureBackTime() != null) {
                long subTime = departure.getDepartureBackTime().getTime()
                        - departure.getDepartureStartTime().getTime();
                double subTimeDouble = ((double) subTime) / 3600000;
                // 获取节约用时
                Double saveTime;
                if (departure.getDepartureRealCostTime() != null) {
                    saveTime = departure.getDepartureCostTime()
                            - departure.getDepartureRealCostTime();
                } else {
                    saveTime = departure.getDepartureCostTime() - subTimeDouble;
                }
                model.addAttribute("saveTime", saveTime);
            }
        }
        return "departure/detail";
    }

    /**
     * 获取派车单详情接口
     *
     * @param departureId 派车单id
     * @return json信息
     */
    @RequestMapping(value = "/{departureId}/detail.doc", method = RequestMethod.GET)
    public String findDepartureDetailInfoDoc(
            @PathVariable Integer departureId,
            Model model,
            HttpServletResponse response
    ) {
        response.setContentType("application/octet-stream");
        response.addHeader("Content-Disposition", "attachment;filename=detail_" + departureId + ".doc");
        return this.findDepartureDetailInfoPage(departureId, model);
    }


}
