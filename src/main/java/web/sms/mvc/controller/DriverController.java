package web.sms.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import web.sms.core.pojo.JsonInfo;
import web.sms.mvc.entity.User;
import web.sms.mvc.service.DepartureService;
import web.sms.mvc.service.UserService;

import java.util.Date;

/**
 * 时间： 2018/1/10
 *
 * @author Eoly
 */
@Controller
@RequestMapping("/driver")
public class DriverController {

    private final DepartureService departureService;
    private final UserService userService;

    @Autowired
    public DriverController(DepartureService departureService, UserService userService) {
        this.departureService = departureService;
        this.userService = userService;
    }

    /**
     * 获取司机所有车单的消耗时间和
     *
     * @param driverId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{driverId}/time-cost", method = RequestMethod.GET)
    public JsonInfo getAllTimeCostInADriver(
            @PathVariable Integer driverId
    ) {
        JsonInfo result = new JsonInfo(0, "获取司机车单信息成功");
        result.put("timeCost", departureService.getAllTimeCostInADriver(driverId));
        return result;
    }

    /**
     * 获取司机一段时间内的车单总状况
     *
     * @param driverId
     * @param startTime
     * @param endTime
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{driverId}/departure-info", method = RequestMethod.GET)
    public JsonInfo findDriverInfoInCycle(
            @PathVariable Integer driverId,
            Date startTime,
            Date endTime
    ) {
        JsonInfo result = new JsonInfo(0, "获取司机车单信息成功");
        result.put("info", departureService.findDriverInfoInCycle(driverId, startTime, endTime));
        return result;
    }

    @RequestMapping(value = "/{driverId}/departure-info.model", method = RequestMethod.GET)
    public String driverInfoModel(
            @PathVariable Integer driverId,
            Date startTime,
            Date endTime,
            Model model
    ) {
        model.addAttribute("info", departureService.findDriverInfoInCycle(driverId, startTime, endTime));
        model.addAttribute("departuresInfo", departureService.findDriverDepartures(driverId,
                null, null, startTime, endTime));
        model.addAttribute("timeCost", departureService.getAllTimeCostInADriver(driverId));
        model.addAttribute("startTime", startTime);
        model.addAttribute("endTime", endTime);
        return "/departure/driver/info";
    }

    @RequestMapping(value = "/search.model", method = RequestMethod.GET)
    public String searchDriverPage(
            Model model
    ) {
        User user = new User();
        // 查询所有顾客
        user.setUserRole("driver");
        user.setUserStatus(0);
        model.addAttribute("drivers", userService.findUserByAttributes(user));
        return "/departure/driver/search";
    }
}
