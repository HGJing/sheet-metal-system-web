package web.sms.mvc.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import web.sms.mvc.entity.User;
import web.sms.mvc.service.ProcessService;
import web.sms.mvc.service.RemindService;
import web.sms.mvc.service.UserService;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 时间： 2017/12/11
 *
 * @author Eoly
 */
@Controller
@RequestMapping("")
public class PageController {

    private final UserService userService;
    private final ProcessService processService;
    private final RemindService remindService;

    @Resource
    // 角色对应的登录成功的url映射
    private Map<String, String> roleLoginMap;

    @Autowired
    public PageController(UserService userService, ProcessService processService, RemindService remindService) {
        this.userService = userService;
        this.processService = processService;
        this.remindService = remindService;
    }

    /**
     * 登录用户的首页，该页面会根据用户角色进行跳转
     *
     * @return 重定向
     */
    @RequestMapping(value = "/index.html", method = RequestMethod.GET)
    public String index() {
        Subject subject = SecurityUtils.getSubject();
        User userLogged = (User) subject.getSession().getAttribute("userLogged");
        if (!(subject.isAuthenticated() || subject.isRemembered()) || userLogged == null) {
            return "redirect:/login.html";
        }
        String url = roleLoginMap.get(userLogged.getUserRole());
        return "redirect:" + url;
    }

    /**
     * 处理错误的页面
     *
     * @return jsp
     */
    @RequestMapping(value = "/error.html", method = RequestMethod.GET)
    public String error() {
        return "error";
    }

    /**
     * 用户的欢迎模块
     *
     * @return jsp
     */
    @RequestMapping(value = "/welcome.model", method = RequestMethod.GET)
    public String welcome(Model model) {
        model.addAttribute("notReadCount", remindService.getNotReadReminds());
        return "welcome";
    }


    /**
     * 管理员首页
     *
     * @return jsp
     */
    @RequestMapping("/admin/index.html")
    public String indexAdminPage(Model model) {
        model.addAttribute("treePath", "/resources/directory-tree-admin.json");
        model.addAttribute("roleName", "管理员");
        model.addAttribute("user",
                SecurityUtils.getSubject().getSession().getAttribute("userLogged"));
        model.addAttribute("notReadCount", remindService.getNotReadReminds());
        return "index";
    }

    /**
     * 文员首页
     *
     * @return jsp
     */
    @RequestMapping("/clerk/index.html")
    public String indexClerkPage(Model model) {
        model.addAttribute("treePath", "/resources/directory-tree-clerk.json");
        model.addAttribute("roleName", "文员");
        model.addAttribute("user",
                SecurityUtils.getSubject().getSession().getAttribute("userLogged"));
        model.addAttribute("notReadCount", remindService.getNotReadReminds());
        return "index";
    }

    /**
     * 销售管理首页
     *
     * @return jsp
     */
    @RequestMapping("/sales-manager/index.html")
    public String indexSManagerPage(Model model) {
        model.addAttribute("treePath", "/resources/directory-tree-s-manager.json");
        model.addAttribute("roleName", "销售管理");
        model.addAttribute("user",
                SecurityUtils.getSubject().getSession().getAttribute("userLogged"));
        model.addAttribute("notReadCount", remindService.getNotReadReminds());
        return "index";
    }

    /**
     * 司机首页
     *
     * @return jsp
     */
    @RequestMapping("/driver/index.html")
    public String indexDriverPage(Model model) {
        model.addAttribute("treePath", "/resources/directory-tree-driver.json");
        model.addAttribute("roleName", "司机");
        model.addAttribute("user",
                SecurityUtils.getSubject().getSession().getAttribute("userLogged"));
        model.addAttribute("notReadCount", remindService.getNotReadReminds());
        return "index";
    }

    /**
     * 生产管理首页
     *
     * @return jsp
     */
    @RequestMapping("/production-manager/index.html")
    public String indexPManagerPage(Model model) {
        model.addAttribute("treePath", "/resources/directory-tree-p-manager.json");
        model.addAttribute("roleName", "生产管理");
        model.addAttribute("user",
                SecurityUtils.getSubject().getSession().getAttribute("userLogged"));
        model.addAttribute("notReadCount", remindService.getNotReadReminds());
        return "index";
    }

    /**
     * 客户首页
     *
     * @return jsp
     */
    @RequestMapping("/customer/index.html")
    public String indexCustomerPage(Model model) {
        model.addAttribute("treePath", "/resources/directory-tree-customer.json");
        model.addAttribute("roleName", "客户");
        model.addAttribute("user",
                SecurityUtils.getSubject().getSession().getAttribute("userLogged"));
        model.addAttribute("notReadCount", remindService.getNotReadReminds());
        return "index";
    }

    /**
     * 工人首页
     *
     * @return jsp
     */
    @RequestMapping("/worker/index.html")
    public String indexWorkerPage(Model model) {
        model.addAttribute("treePath", "/resources/directory-tree-worker.json");
        model.addAttribute("roleName", "工人");
        model.addAttribute("user",
                SecurityUtils.getSubject().getSession().getAttribute("userLogged"));
        model.addAttribute("notReadCount", remindService.getNotReadReminds());
        return "index";
    }

    /**
     * 登录页面
     *
     * @return jsp
     */
    @RequestMapping("/login.html")
    public String loginPage() {
        return "login";
    }

    /**
     * 基于用户的修改密码页面模块
     *
     * @return jsp
     */
    @RequestMapping("/user/password/modify.model")
    public String modifyPasswordPage() {
        return "user/password/modify";
    }

    /**
     * 基于管理员的添加用户页面模块
     *
     * @return jsp
     */
    @RequestMapping("/user/user-adding.model")
    public String addingUserModel() {
        return "user/user-adding";
    }

    /**
     * 基于管理员的查看用户页面模块
     *
     * @return jsp
     */
    @RequestMapping("/user/user-list.model")
    public String listUserModel() {
        return "user/user-list";
    }

    /**
     * 基于管理员的编辑用户页面模块
     *
     * @return jsp
     */
    @RequestMapping("/user/user-editing.model")
    public String editingUserModel(Model model, Integer userId) {
        model.addAttribute("user", userService.findUserWithBaseInfo(userId));
        return "user/user-editing";
    }

    /**
     * 基于管理员的编辑用户页面模块
     *
     * @return jsp
     */
    @RequestMapping("/user/worker-group-adding.model")
    public String addingWorkerGroup(Model model) {
        User user = new User();
        // 查询所有工人
        user.setUserRole("worker");
        user.setUserStatus(0);
        model.addAttribute("workers", userService.findUserByAttributes(user));
        model.addAttribute("processes", processService.findAllProcess());
        return "user/worker-group-adding";
    }

    /**
     * 基于文员的添加工单页面模块
     *
     * @return jsp
     */
    @RequestMapping("/order/order-adding.model")
    public String addingWorkOrderModel(Model model) {
        model.addAttribute("processes", processService.findAllProcessExceptNormal());
        User user = new User();
        // 查询所有顾客
        user.setUserRole("customer");
        user.setUserStatus(0);
        model.addAttribute("customers", userService.findUserByAttributes(user));
        user.setUserRole("customer_special");
        model.addAttribute("customersSpecial", userService.findUserByAttributes(user));
        // 查询所有的销售管理
        user.setUserRole("sales_manager");
        model.addAttribute("salesManagers", userService.findUserByAttributes(user));
        return "order/order-adding";
    }

    /**
     * 基于工单列表模块
     *
     * @return jsp
     */
    @RequestMapping("/order/order-list.model")
    public String listOrderModel() {
        return "order/order-list";
    }

    /**
     * 基于工人组长工单列表模块
     *
     * @return jsp
     */
    @RequestMapping("/order/order-list-leader.model")
    public String listLeaderOrderModel() {
        return "order/order-list-leader";
    }

    /**
     * 基于工人组员工单列表模块
     *
     * @return jsp
     */
    @RequestMapping("/order/order-list-member.model")
    public String listMemberOrderModel() {
        return "order/order-list-member";
    }

    /**
     * 基于工人组员工单列表模块
     *
     * @return jsp
     */
    @RequestMapping("/remind/remind.model")
    public String remindModel() {
        return "remind/remind";
    }

    /**
     * 文员可查看的添加派车单模块
     *
     * @return jsp
     */
    @RequestMapping("/departure/departure-adding.model")
    public String departureClerkAdd(Model model) {
        User user = new User();
        // 查询所有司机
        user.setUserRole("driver");
        user.setUserStatus(0);
        model.addAttribute("drivers", userService.findUserByAttributes(user));
        // 查询所有顾客
        user.setUserRole("customer");
        model.addAttribute("customers", userService.findUserByAttributes(user));
        user.setUserRole("customer_special");
        model.addAttribute("customersSpecial", userService.findUserByAttributes(user));
        return "departure/adding";
    }


    /**
     * 文员可查看的派车单列表
     *
     * @return jsp
     */
    @RequestMapping("/departure/clerk/list.model")
    public String departureClerkList(Model model) {
        model.addAttribute("role", "clerk");
        User user = new User();
        user.setUserRole("driver");
        user.setUserStatus(0);
        model.addAttribute("drivers", userService.findUserByAttributes(user));
        return "departure/list";
    }

    /**
     * 司机可查看的派车单列表
     *
     * @return jsp
     */
    @RequestMapping("/departure/driver/list.model")
    public String departureDriverList(Model model) {
        model.addAttribute("role", "driver");
        return "departure/list";
    }


    /**
     * 工单总销售和查看页面
     *
     * @return jsp
     */
    @RequestMapping("/order/search.model")
    public String orderTotalPriceModel() {
        return "order/search-price";
    }
}
