package web.sms.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import web.sms.core.pojo.JsonInfo;
import web.sms.mvc.dao.ProcessMapper;
import web.sms.mvc.entity.Process;

import java.util.List;

/**
 * 时间： 2017/12/19
 *
 * @author Eoly
 */
@Controller
@RequestMapping("/process")
public class ProcessController {
    private final ProcessMapper processMapper;

    @Autowired
    public ProcessController(ProcessMapper processMapper) {
        this.processMapper = processMapper;
    }

    /**
     * 获取加工流程集合
     *
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/list")
    public JsonInfo list() {
        JsonInfo jsonInfo = new JsonInfo(0, "获取成功");
        List<Process> processes = processMapper.findAllProcess();
        jsonInfo.put("list", processes);
        return jsonInfo;
    }

    /**
     * 获取加工流程集合
     *
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/list-except")
    public JsonInfo listExcept() {
        JsonInfo jsonInfo = new JsonInfo(0, "获取成功");
        List<Process> processes = processMapper.findAllProcessExceptNormal();
        jsonInfo.put("list", processes);
        return jsonInfo;
    }
}
