package web.sms.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import web.sms.core.pojo.JsonInfo;
import web.sms.mvc.service.RemindService;

/**
 * Created by Eoly on 2017/6/2.
 */
@Controller
@RequestMapping("/remind")
public class RemindController {

    private final RemindService remindService;

    @Autowired
    public RemindController(RemindService remindService) {
        this.remindService = remindService;
    }

    /**
     * 提醒列表
     *
     * @param page
     * @param rows
     * @return
     */
    @ResponseBody
    @RequestMapping("/list")
    public JsonInfo list(Integer page, Integer rows) {
        if (page == null || page < 1) {
            page = 1;
        }
        if (rows == null || rows < 1) {
            rows = 10;
        }
        JsonInfo jsonInfo = new JsonInfo(0, "获取成功");
        jsonInfo.put("pageInfo", remindService.findUserReminds(page, rows));
        return jsonInfo;
    }

    @ResponseBody
    @RequestMapping("/{remindId}/read")
    public JsonInfo read(@PathVariable Integer remindId) {
        JsonInfo jsonInfo = new JsonInfo(0, "已读");
        remindService.doReadRemind(remindId);
        return jsonInfo;
    }
}
