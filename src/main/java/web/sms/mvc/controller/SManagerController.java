package web.sms.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import web.sms.mvc.entity.User;
import web.sms.mvc.service.UserService;
import web.sms.mvc.service.WorkOrderService;

import java.util.Date;

/**
 * 时间： 2018/1/10
 *
 * @author Eoly
 */
@Controller
@RequestMapping("/order/sales-manager")
public class SManagerController {

    private final WorkOrderService workOrderService;
    private final UserService userService;

    @Autowired
    public SManagerController(WorkOrderService workOrderService, UserService userService) {
        this.workOrderService = workOrderService;
        this.userService = userService;
    }

    /**
     * 获取销售管理一段时间内的工单
     *
     * @param sManagerId
     * @param startTime
     * @param endTime
     * @param model
     * @return
     */
    @RequestMapping(value = "/{sManagerId}/order-info.model", method = RequestMethod.GET)
    public String sManagerInfoModel(
            @PathVariable Integer sManagerId,
            Date startTime,
            Date endTime,
            Model model
    ) {
        model.addAttribute("orders", workOrderService.findSManageOrders(sManagerId, startTime, endTime));
        model.addAttribute("startTime", startTime);
        model.addAttribute("endTime", endTime);
        model.addAttribute("sum", 0);
        return "/order/sales-manager/info";
    }

    /**
     * 销售管理的工单总销检索页面
     *
     * @return jsp
     */
    @RequestMapping("/search.model")
    public String searchSManagerModel(Model model) {
        User user = new User();
        user.setUserStatus(0);
        // 查询所有的销售管理
        user.setUserRole("sales_manager");
        model.addAttribute("salesManagers", userService.findUserByAttributes(user));
        return "order/sales-manager/search";
    }
}
