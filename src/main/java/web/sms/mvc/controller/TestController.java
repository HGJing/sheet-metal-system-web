package web.sms.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Eoly on 2017/6/2.
 */
@Controller
@RequestMapping("/")
public class TestController {
    @RequestMapping("/test")
    public String testPage(Model model) {
        model.addAttribute("words", "hello world!");
        return "test";
    }
}
