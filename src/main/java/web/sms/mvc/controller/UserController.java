package web.sms.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import web.sms.core.pojo.JsonInfo;
import web.sms.mvc.entity.User;
import web.sms.mvc.service.RoleService;
import web.sms.mvc.service.UserService;
import web.sms.mvc.service.WorkerGroupService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 时间： 2017/12/14
 * 用户控制器，用于控制用户动作
 *
 * @author Eoly
 */
@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService userService;
    private final RoleService roleService;
    private final WorkerGroupService workerGroupService;

    @Autowired
    public UserController(UserService userService, RoleService roleService, WorkerGroupService workerGroupService) {
        this.userService = userService;
        this.roleService = roleService;
        this.workerGroupService = workerGroupService;
    }

    /**
     * 用户登录
     *
     * @param username 用户名
     * @param password 密码
     * @return Json对象，包含登录信息和登录成功的连接
     */
    @ResponseBody
    @RequestMapping("/login")
    public JsonInfo login(String username, String password, Boolean rememberMe) {
        User user = userService.doLogin(username, password, rememberMe);
        JsonInfo jsonInfo = new JsonInfo(0, "登录成功");
        jsonInfo.put("url", "/index.html");
        return jsonInfo;
    }

    /**
     * 当前用户注销
     *
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/logout")
    public JsonInfo logout() {
        userService.doLogout();
        return new JsonInfo(0, "注销成功");
    }

    /**
     * 查询用户
     *
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/list")
    public JsonInfo selectUsers(Integer page, Integer rows,
                                String userRealName, String userRole) {
        if (page == null || page < 1) {
            page = 1;
        }
        if (rows == null || rows < 1) {
            rows = 10;
        }
        JsonInfo jsonInfo = new JsonInfo(0, "获取成功");
        jsonInfo.put("pageInfo", userService.findUsersByPageAndNameAndRole(page, rows, userRealName, userRole));
        return jsonInfo;
    }

    /**
     * 根据角色查询用户
     *
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/by-role/{roleCode}/list")
    public JsonInfo selectUsers(@PathVariable String roleCode) {
        JsonInfo jsonInfo = new JsonInfo(0, "获取成功");
        User user = new User();
        user.setUserRole(roleCode);
        jsonInfo.put("list", userService.findUserByAttributes(user));
        return jsonInfo;
    }

    /**
     * 获取用户所有角色
     *
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/roles")
    public JsonInfo findAllUserRoles() {
        JsonInfo jsonInfo = new JsonInfo(0, "获取成功");
        jsonInfo.put("roles", roleService.findAllRoles());
        return jsonInfo;
    }

    /**
     * 删除用户信息
     *
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/delete/{userId}")
    public JsonInfo deleteUser(@PathVariable Integer userId) {
        JsonInfo jsonInfo = new JsonInfo(0, "删除成功");
        userService.deleteUser(userId);
        return jsonInfo;
    }

    /**
     * 修改用户信息
     *
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/edit")
    public JsonInfo editUser(User user) {
        JsonInfo jsonInfo = new JsonInfo(0, "修改成功");
        userService.editUser(user);
        return jsonInfo;
    }

    /**
     * 修改用户信息
     *
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/has-login-name/{userLoginName}")
    public JsonInfo hasLoginName(@PathVariable String userLoginName) {
        JsonInfo jsonInfo = new JsonInfo(0, "查询成功");
        jsonInfo.put("hasLoginName", userService.isLoginNameExist(userLoginName));
        return jsonInfo;
    }

    /**
     * 添加用户信息
     *
     * @param user 用户实体
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/add")
    public JsonInfo addUser(User user) {
        userService.addUser(user);
        return new JsonInfo(0, "添加成功");
    }

    /**
     * 修改用户自身密码
     *
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping(value = "/password/modify", method = RequestMethod.POST)
    public JsonInfo modifyUserPassword(
            String oldPassword,
            String newPassword
    ) {
        userService.editPassword(null, oldPassword, newPassword);
        userService.doLogout();
        return new JsonInfo(0, "修改成功,由于密码已修改，请重新登录");
    }

    /**
     * 添加工人小组信息
     *
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/worker-group/add")
    public JsonInfo addWorkerGroup(Integer leaderWorkerId, Integer processId, Integer[] workersId) {
        JsonInfo jsonInfo = new JsonInfo(0, "添加成功");
        List<Integer> workerIdList = new ArrayList<Integer>();
        if (workersId != null && workersId.length > 0) {
            workerIdList.addAll(Arrays.asList(workersId));
        }
        Integer id = workerGroupService.addNewWorkerGroup(workerIdList, leaderWorkerId, processId);
        jsonInfo.put("groupId", id);
        return jsonInfo;
    }

    /**
     * 获取所有工人小组信息
     *
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/worker-group/captains/list")
    public JsonInfo findWorkerCaptains() {
        JsonInfo jsonInfo = new JsonInfo(0, "获取成功");
        jsonInfo.put("list", workerGroupService.findAllCaptain());
        return jsonInfo;
    }

    /**
     * 获取工人小组信息
     *
     * @param processId 加工流程id
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/worker-group/captains/list/{processId}")
    public JsonInfo findWorkerCaptains(@PathVariable Integer processId) {
        JsonInfo jsonInfo = new JsonInfo(0, "获取成功");
        jsonInfo.put("list", workerGroupService.findCaptainsByProcessId(processId));
        return jsonInfo;
    }

    /**
     * 获取工人小组组长的信息
     *
     * @param groupId 工人组id
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping("/worker-group/captain/{groupId}")
    public JsonInfo findWorkerCaptain(@PathVariable Integer groupId) {
        JsonInfo jsonInfo = new JsonInfo(0, "查询成功");
        jsonInfo.put("captain", workerGroupService.findCaptainByGroupId(groupId));
        return jsonInfo;
    }

    /**
     * 查询用户信息的模块
     *
     * @param groupId 小组id
     * @return jsp页面
     */
    @RequestMapping("/worker-group/info.model")
    public String groupInfoModel(@RequestParam Integer groupId, Model model) {
        model.addAttribute("workers", workerGroupService.findGroupInfo(groupId));
        return "/user/worker-group/info";
    }
}
