package web.sms.mvc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import web.sms.core.pojo.JsonInfo;
import web.sms.mvc.entity.WorkOrder;
import web.sms.mvc.service.TaskAssignmentService;
import web.sms.mvc.service.WorkOrderService;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * 时间： 2017/12/19
 *
 * @author Eoly
 */
@Controller
@RequestMapping("/order")
public class WorkOrderController {

    private Logger logger = LoggerFactory.getLogger(WorkOrderController.class);
    private final WorkOrderService workOrderService;
    private final TaskAssignmentService taskAssignmentService;

    @Autowired
    public WorkOrderController(WorkOrderService workOrderService, TaskAssignmentService taskAssignmentService) {
        this.workOrderService = workOrderService;
        this.taskAssignmentService = taskAssignmentService;
    }

    /**
     * 添加工单接口
     *
     * @param workOrder 工单实体
     * @param processId 加工流程的数组
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public JsonInfo addOrder(WorkOrder workOrder, Integer[] processId) {
        Set<Integer> processIdSet = new HashSet<Integer>();
        // 判断id是否存在
        if (processId != null && processId.length > 0) {
            processIdSet = new HashSet<Integer>(Arrays.asList(processId));
        }
        workOrderService.addWorkOrder(workOrder, processIdSet);
        return new JsonInfo(0, "添加工单成功");
    }

    /**
     * 分页获取工单
     *
     * @param checkOrder 查询条件对象
     * @param page       第几页
     * @param rows       每页多少条数据
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public JsonInfo list(Integer page, Integer rows, WorkOrder checkOrder) {
        JsonInfo jsonInfo = new JsonInfo(0, "查询成功");
        if (page == null || page < 1) {
            page = 1;
        }
        if (rows == null || rows < 1) {
            rows = 10;
        }
        jsonInfo.put("pageInfo", workOrderService.findWorkOrderByPage(checkOrder, page, rows));
        return jsonInfo;
    }

    /**
     * 分配工单任务流程接口
     *
     * @param taskId  任务流程id
     * @param groupId 工人组id
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping(value = "/distribution/{taskId}-{groupId}")
    public JsonInfo distribution(
            @PathVariable Integer taskId,
            @PathVariable Integer groupId
    ) {
        taskAssignmentService.doDistributionTask(taskId, groupId);
        return new JsonInfo(0, "分配成功");
    }

    /**
     * 删除工单的接口
     *
     * @param orderId 工单id
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping(value = "/delete/{orderId}")
    public JsonInfo deleteOrder(
            @PathVariable Integer orderId
    ) {
        workOrderService.deleteWorkOrder(orderId);
        return new JsonInfo(0, "删除成功");
    }

    /**
     * 查询组长负责的任务列表
     *
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping(value = "/task/list/leader")
    public JsonInfo listLeaderTasks() {
        JsonInfo jsonInfo = new JsonInfo(0, "查询成功");
        jsonInfo.addAttribute("list", taskAssignmentService.findLeaderTasks());
        return jsonInfo;
    }

    /**
     * 完成某一任务
     *
     * @param taskId  需要完成的任务的id
     * @param groupId 负责该任务的团队id
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping(value = "/task/{taskId}/finish")
    public JsonInfo finishLeaderTask(
            @RequestParam Integer groupId,
            @PathVariable Integer taskId
    ) {
        JsonInfo jsonInfo = new JsonInfo(0, "任务已完成");
        taskAssignmentService.doFinishTask(taskId, groupId);
        return jsonInfo;
    }

    /**
     * 改变工单留言
     *
     * @param orderId  工单id
     * @param priority 欲改变的优先级
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping(value = "/{orderId}/change-priority")
    public JsonInfo changeOrderPriority(
            @RequestParam Integer priority,
            @PathVariable Integer orderId
    ) {
        JsonInfo jsonInfo = new JsonInfo(0, "优先级已变更");
        workOrderService.changeOrderPriority(orderId, priority);
        return jsonInfo;
    }

    /**
     * 改变工单的是否快速执行选项
     *
     * @param orderId 工单id
     * @param isQuick 是否快速执行
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping(value = "/{orderId}/change-is-quick")
    public JsonInfo changeOrderIsQuick(
            @RequestParam Integer isQuick,
            @PathVariable Integer orderId
    ) {
        JsonInfo jsonInfo = new JsonInfo(0, "是否快速执行状态已变更");
        workOrderService.changeOrderIsQuick(orderId, isQuick);
        return jsonInfo;
    }

    /**
     * 改变工单留言
     *
     * @param orderId 工单id
     * @param message 欲改变的留言
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping(value = "/{orderId}/change-message")
    public JsonInfo changeOrderMessage(
            @RequestParam String message,
            @PathVariable Integer orderId
    ) {
        JsonInfo jsonInfo = new JsonInfo(0, "留言已变更");
        workOrderService.editWorkOrderLeftMessage(orderId, message);
        return jsonInfo;
    }

    /**
     * 变更指定订单的支付状态为清欠
     *
     * @param orderId 订单id
     * @return 响应结果对应的JSON对象
     */
    @ResponseBody
    @RequestMapping(value = "/{orderId}/paid")
    public JsonInfo makeOrderPaid(
            @PathVariable Integer orderId
    ) {
        JsonInfo jsonInfo = new JsonInfo(0, "已更改工单付款状态为清欠");
        workOrderService.makeOrderPaid(orderId);
        return jsonInfo;
    }

    @ResponseBody
    @RequestMapping(value = "/task/pre/{taskId}/list")
    public JsonInfo findPreTasks(
            @PathVariable Integer taskId
    ) {
        JsonInfo jsonInfo = new JsonInfo(0, "获取成功");
        jsonInfo.put("list", taskAssignmentService.findPreTasks(taskId));
        return jsonInfo;
    }

    @ResponseBody
    @RequestMapping(value = "/total-price")
    public JsonInfo getSumOfOrderPriceInCycle(
            Date startTime,
            Date endTime
    ) {
        JsonInfo jsonInfo = new JsonInfo(0, "获取成功");
        jsonInfo.put("price", workOrderService.getSumOfOrderPriceInCycle(startTime, endTime));
        return jsonInfo;
    }

    /**
     * 获取订单详情
     *
     * @param orderId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{orderId}/detail")
    public JsonInfo getOrderDetail(
            @PathVariable Integer orderId
    ) {
        JsonInfo jsonInfo = new JsonInfo(0, "获取成功");
        jsonInfo.put("order", workOrderService.findOrderDetail(orderId));
        return jsonInfo;
    }
}
