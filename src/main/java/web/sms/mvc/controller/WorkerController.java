package web.sms.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import web.sms.core.pojo.JsonInfo;
import web.sms.mvc.entity.User;
import web.sms.mvc.service.UserService;
import web.sms.mvc.service.WorkOrderService;

import java.util.Date;

/**
 * 时间： 2018/1/10
 *
 * @author Eoly
 */
@Controller
@RequestMapping("/worker")
public class WorkerController {

    private final WorkOrderService workOrderService;
    private final UserService userService;

    @Autowired
    public WorkerController(WorkOrderService workOrderService, UserService userService) {
        this.workOrderService = workOrderService;
        this.userService = userService;
    }

    /**
     * 获取工人一段时间内的工单总状况
     *
     * @param workerId
     * @param startTime
     * @param endTime
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/{workerId}/order-info", method = RequestMethod.GET)
    public JsonInfo findOrderInfoWithWorkerInCycle(
            @PathVariable Integer workerId,
            Date startTime,
            Date endTime
    ) {
        JsonInfo result = new JsonInfo(0, "获取工人工单信息成功");
        result.put("info", workOrderService.findOrderInfoWithWorkerInCycle(
                workerId, startTime, endTime));
        return result;
    }

    @RequestMapping(value = "/{workerId}/departure-info.model", method = RequestMethod.GET)
    public String findOrderInfoWithWorkerInCycleModel(
            @PathVariable Integer workerId,
            Date startTime,
            Date endTime,
            Model model
    ) {
        model.addAttribute("info", workOrderService.findOrderInfoWithWorkerInCycle(
                workerId, startTime, endTime));
        model.addAttribute("startTime", startTime);
        model.addAttribute("endTime", endTime);
        return "/order/worker/info";
    }

    @RequestMapping(value = "/search.model", method = RequestMethod.GET)
    public String searchDriverPage(
            Model model
    ) {
        User user = new User();
        // 查询所有顾客
        user.setUserRole("worker");
        user.setUserStatus(0);
        model.addAttribute("workers", userService.findUserByAttributes(user));
        return "/order/worker/search";
    }
}
