package web.sms.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import web.sms.mvc.service.WorkOrderService;
import web.sms.mvc.service.WorkerGroupService;

import java.util.Date;

/**
 * 时间： 2018/1/10
 *
 * @author Eoly
 */
@Controller
@RequestMapping("/order/worker-group")
public class WorkerGroupController {

    private final WorkOrderService workOrderService;
    private final WorkerGroupService workerGroupService;


    @Autowired
    public WorkerGroupController(WorkOrderService workOrderService, WorkerGroupService workerGroupService) {
        this.workOrderService = workOrderService;
        this.workerGroupService = workerGroupService;
    }

    /**
     * 获取销售管理一段时间内的工单
     *
     * @param groupId
     * @param startTime
     * @param endTime
     * @param model
     * @return
     */
    @RequestMapping(value = "/{groupId}/order-info.model", method = RequestMethod.GET)
    public String sManagerInfoModel(
            @PathVariable Integer groupId,
            Date startTime,
            Date endTime,
            Model model
    ) {
        model.addAttribute("captain", workerGroupService.findCaptainDetailWithinTime(
                groupId, startTime, endTime
        ));
        model.addAttribute("startTime", startTime);
        model.addAttribute("endTime", endTime);
        model.addAttribute("sum", 0);
        return "/order/worker-group/info";
    }

    /**
     * 销售管理的工单总销检索页面
     *
     * @return jsp
     */
    @RequestMapping("/search.model")
    public String searchSManagerModel(Model model) {
        // 查询所有的队长
        model.addAttribute("captains", workerGroupService.findAllCaptain());
        return "order/worker-group/search";
    }
}
