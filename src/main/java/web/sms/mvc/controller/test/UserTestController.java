package web.sms.mvc.controller.test;

import com.alibaba.fastjson.JSON;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import web.sms.core.pojo.JsonInfo;
import web.sms.mvc.entity.User;
import web.sms.mvc.service.UserService;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 时间： 2017/12/12
 *
 * @author Eoly
 */
@RestController("tC")
@RequestMapping("/test/user")
public class UserTestController {

    private final UserService userService;

    @Resource
    private Map<String, String> roleLoginMap;

    @Autowired
    public UserTestController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 测试登录
     *
     * @param username
     * @param password
     * @return
     */
    @RequestMapping("/login")
    public JsonInfo login(String username, String password, Boolean rememberMe) {
        User user = userService.doLogin(username, password, rememberMe);
        JsonInfo jsonInfo = new JsonInfo(0, "登录成功");
        jsonInfo.put("url", roleLoginMap.get(user.getUserRole()));
        return jsonInfo;
    }

    /**
     * 测试添加用户
     *
     * @param user
     * @return
     */
    @RequestMapping("/add")
    public JsonInfo addUser(User user) {
        userService.addUser(user);
        return new JsonInfo(0, "添加成功");
    }

    @RequestMapping("/info")
    @ResponseBody
    public JsonInfo userInfo() {
        return new JsonInfo(0, "当前用户:" +
                JSON.toJSONString(SecurityUtils.getSubject().getSession().getAttribute("userLogged")));
    }
}
