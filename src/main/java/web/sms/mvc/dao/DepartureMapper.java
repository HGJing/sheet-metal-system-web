package web.sms.mvc.dao;

import org.apache.ibatis.annotations.Param;
import web.sms.mvc.entity.Departure;
import web.sms.mvc.entity.pojo.DriverDepartureInfo;

import java.util.Date;
import java.util.List;

public interface DepartureMapper {
    int deleteByPrimaryKey(Integer departureId);

    int insert(Departure record);

    int insertSelective(Departure record);

    Departure selectByPrimaryKey(Integer departureId);

    int updateByPrimaryKeySelective(Departure record);

    int updateByPrimaryKey(Departure record);

    /**
     * 查询所有的派车单
     *
     * @return
     */
    List<Departure> selectAllDepartures();

    /**
     * 查询所有符合条件的派车单
     *
     * @return
     */
    List<Departure> selectDeparturesByAttribute(Departure record);

    /**
     * 查询司机所有的派车单
     *
     * @param driverId
     * @return
     */
    List<Departure> selectDriverDepartures(@Param("driverId") Integer driverId,
                                           @Param("startTime") Date startTime,
                                           @Param("endTime") Date endTime);

    /**
     * 查询详细的派车单信息
     *
     * @return
     */
    Departure selectDetailDepartureInfo(Integer departureId);

    /**
     * 查询司机派车的总时间(h)
     *
     * @param driverId
     * @return
     */
    Double selectTotalCostTimeByDriverId(Integer driverId);

    /**
     * 查询司机一定时间段的派车详情
     *
     * @param driverId
     * @param startTime
     * @param endTime
     * @return
     */
    DriverDepartureInfo selectDriverDepartureInfo(
            @Param("driverId") Integer driverId,
            @Param("startTime") Date startTime,
            @Param("endTime") Date endTime
    );


}