package web.sms.mvc.dao;

import web.sms.mvc.entity.Process;

import java.util.List;

public interface ProcessMapper {
    int deleteByPrimaryKey(Integer processId);

    int insert(Process record);

    int insertSelective(Process record);

    Process selectByPrimaryKey(Integer processId);

    int updateByPrimaryKeySelective(Process record);

    int updateByPrimaryKey(Process record);

    /**
     * 查询所有流程除开打包和发货
     *
     * @return 流程实体集合
     */
    List<Process> findAllProcess();

    /**
     * 查询所有流程除开打包和发货 除开打包和发货
     *
     * @return 流程实体集合
     */
    List<Process> findAllProcessExceptNormal();
}