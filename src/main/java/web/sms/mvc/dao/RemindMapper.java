package web.sms.mvc.dao;

import web.sms.mvc.entity.Remind;

import java.util.List;

public interface RemindMapper {
    int deleteByPrimaryKey(Integer remindId);

    int insert(Remind record);

    int insertSelective(Remind record);

    Remind selectByPrimaryKey(Integer remindId);

    int updateByPrimaryKeySelective(Remind record);

    int updateByPrimaryKey(Remind record);

    List<Remind> selectRemindsByUserId(Integer userId);

    /**
     * 获取指定用户未读提醒的数量
     *
     * @param userId
     * @return
     */
    Integer selectCountNotReadRemindsByUserId(Integer userId);
}