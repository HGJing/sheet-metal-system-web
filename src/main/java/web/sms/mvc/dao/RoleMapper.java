package web.sms.mvc.dao;

import web.sms.mvc.entity.Role;

import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(String roleCode);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(String roleCode);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    List<Role> selectAllWithoutAdmin();
}