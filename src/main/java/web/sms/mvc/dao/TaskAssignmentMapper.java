package web.sms.mvc.dao;

import web.sms.mvc.entity.TaskAssignment;

import java.util.List;

public interface TaskAssignmentMapper {
    int deleteByPrimaryKey(Integer taskAssignmentId);

    int insert(TaskAssignment record);

    int insertSelective(TaskAssignment record);

    TaskAssignment selectByPrimaryKey(Integer taskAssignmentId);

    int updateByPrimaryKeySelective(TaskAssignment record);

    int updateByPrimaryKey(TaskAssignment record);

    /**
     * 通过组长id查询任务加工流程
     *
     * @param leaderId 组长id
     * @return 流程列表
     */
    List<TaskAssignment> selectByLeaderId(Integer leaderId);

    /**
     * 获取小组对应的任务
     *
     * @param groupId 小组id
     * @return 任务实体集合
     */
    List<TaskAssignment> selectTasksByGroupId(Integer groupId);

    /**
     * 查询工单对应任务中优先级最高的一条
     *
     * @param orderNo 工单编号
     * @return 任务实体
     */
    TaskAssignment selectTheMostPriorityTask(String orderNo);

    /**
     * 查询谋条任务的前置任务
     *
     * @param taskId 任务id
     * @return 任务流程实体集合
     */
    List<TaskAssignment> selectPreTasks(Integer taskId);



}