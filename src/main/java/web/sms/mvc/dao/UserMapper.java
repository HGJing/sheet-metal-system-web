package web.sms.mvc.dao;

import org.apache.ibatis.annotations.Param;
import web.sms.mvc.entity.User;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    /**
     * 通过用户登录名查询用户实体全部属性
     *
     * @param loginName 用户名
     * @return 用户实体
     */
    User selectByLoginName(String loginName);

    /**
     * 通过用户登录名查询用户实体认证授权属性
     * user_id, user_open_id, user_login_name, user_password, user_role, user_nick_name, user_status
     *
     * @param loginName 用户名
     * @return 用户实体
     */
    User selectAuthenticationByLoginName(String loginName);

    /**
     * 通过用户登录名查询用户实体认证授权属性
     * user_id, user_open_id, user_login_name, user_password, user_role, user_real_name, user_status
     *
     * @param userId 用户id
     * @return 用户实体
     */
    User selectAuthenticationByPrimaryKey(Integer userId);

    /**
     * 查询用户id
     *
     * @param loginName 登录名
     * @return 用户id
     */
    Integer selectUserIdByUserLoginName(String loginName);

    /**
     * 查询用户基本信息
     * 除了password,并且有用户信息
     *
     * @param userId 用户id
     * @return 用户实体
     */
    User selectUserBaseInfoByUserId(Integer userId);

    /**
     * 查询所有用户基本信息
     *
     * @return 用户实体集合
     */
    List<User> selectAllUsers();

    /**
     * 查询所有用户的数量
     *
     * @return 用户实体集合
     */
    Integer selectAllUsersCount();

    /**
     * 按用户属性查找所有用户[多个属性]
     *
     * @param user 用户对象
     * @param sort 排序
     * @return 用户实体集合
     */
    List<User> selectAllUserByAttributes(@Param("user") User user,
                                         @Param("sort") String sort);

    int updateAttributesToNull(@Param("userId") Integer userId,
                               @Param("name") String name);
}