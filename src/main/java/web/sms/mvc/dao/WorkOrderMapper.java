package web.sms.mvc.dao;

import org.apache.ibatis.annotations.Param;
import web.sms.mvc.entity.WorkOrder;
import web.sms.mvc.entity.pojo.WorkerOrderInfo;

import java.util.Date;
import java.util.List;

public interface WorkOrderMapper {
    int deleteByPrimaryKey(Integer orderId);

    int insert(WorkOrder record);

    int insertSelective(WorkOrder record);

    WorkOrder selectByPrimaryKey(Integer orderId);

    WorkOrder selectByOrderNo(String OrderNo);

    int updateByPrimaryKeySelective(WorkOrder record);

    int updateByPrimaryKey(WorkOrder record);

    /**
     * 根据客户id查询客户对应的工单信息
     *
     * @param customerId 客户id
     * @return 工单实体
     */
    List<WorkOrder> selectOrderByCustomerId(
            @Param("cowNumber") Integer cowNumber,
            @Param("cowSize") Integer cowSize,
            @Param("customerId") Integer customerId);

    Integer selectOrderCountByCustomerId(Integer customerId);

    /**
     * 根据销售管理id查询销售管理对应的工单信息
     *
     * @param sManagerId 销售管理id
     * @return 工单实体
     */
    List<WorkOrder> selectOrderBySManagerId(
            @Param("cowNumber") Integer cowNumber,
            @Param("cowSize") Integer cowSize,
            @Param("sManagerId") Integer sManagerId,
            @Param("startTime") Date startTime,
            @Param("endTime") Date endTime,
            @Param("needFinish") Boolean needFinish);

    Integer selectOrderCountBySManagerId(Integer sManagerId);

    /**
     * 根据生产管理id查询生产管理对应的工单信息
     *
     * @param pManagerId 生产管理id
     * @return 工单实体
     */
    List<WorkOrder> selectOrderByPManagerId(
            @Param("cowNumber") Integer cowNumber,
            @Param("cowSize") Integer cowSize,
            @Param("pManagerId") Integer pManagerId
    );

    /**
     * 查询工人组长对应的工单及加工任务
     *
     * @param workerId 工人id
     * @return 工单实体
     */
    List<WorkOrder> selectOrderByWorkerId(
            @Param("cowNumber") Integer cowNumber,
            @Param("cowSize") Integer cowSize,
            @Param("workerId") Integer workerId);

    Integer selectOrderCountByPManagerId(Integer pManagerId);

    /**
     * 查询所有的工单信息
     *
     * @return 工单实体
     */
    List<WorkOrder> selectAllOrders(
            @Param("cowNumber") Integer cowNumber,
            @Param("cowSize") Integer cowSize
    );

    /**
     * 根据条件查询工单
     *
     * @param workOrder
     * @param cowNumber
     * @param cowSize
     * @return
     */
    List<WorkOrder> selectOrdersByAttribute(
            @Param("theOrder") WorkOrder workOrder,
            @Param("cowNumber") Integer cowNumber,
            @Param("cowSize") Integer cowSize
    );

    /**
     * 查询总的订单数量
     *
     * @return 数量
     */
    Integer selectAllOrdersCount();

    /**
     * 查询总的订单数量
     *
     * @return 数量
     */
    Integer selectOrdersCountByAttribute(
            @Param("theOrder") WorkOrder workOrder
    );

    /**
     * 更新工单状态
     *
     * @param workOrder 工单实体，只包含工单编号和工单欲改变的工单状态两个属性
     * @return 更新数量
     */
    Integer updateOrderStatus(WorkOrder workOrder);

    /**
     * 获取工人一段时间内加工过的工单统计信息
     * todo 工单完成才能计入查询
     *
     * @param workerId
     * @param startTime
     * @param endTime
     * @return
     */
    WorkerOrderInfo selectWorkerOrderInfoByWorkerId(
            @Param("workerId") Integer workerId,
            @Param("startTime") Date startTime,
            @Param("endTime") Date endTime
    );

    /**
     * 获取一段时间内所有已完成的工单统计信息
     *
     * @param startTime
     * @param endTime
     * @return
     */
    Double selectAllPrice(
            @Param("startTime") Date startTime,
            @Param("endTime") Date endTime
    );
}