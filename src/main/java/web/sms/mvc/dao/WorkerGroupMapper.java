package web.sms.mvc.dao;

import org.apache.ibatis.annotations.Param;
import web.sms.mvc.entity.pojo.WorkerCaptain;
import web.sms.mvc.entity.WorkerGroup;
import web.sms.mvc.entity.WorkerGroupKey;
import web.sms.mvc.entity.pojo.WorkerOrderInfo;

import java.util.Date;
import java.util.List;

public interface WorkerGroupMapper {
    int deleteByPrimaryKey(WorkerGroupKey key);

    int insert(WorkerGroup record);

    int insertSelective(WorkerGroup record);

    WorkerGroup selectByPrimaryKey(WorkerGroupKey key);

    int updateByPrimaryKeySelective(WorkerGroup record);

    int updateByPrimaryKey(WorkerGroup record);

    /**
     * 通过小组id获取多个该小组成员实体
     *
     * @param groupId 小组id
     * @return 小组成员组合实体
     */
    List<WorkerGroup> selectWorkerGroupByGroupId(Integer groupId);

    /**
     * 通过小组id获取多个该小组成员实体及其用户信息
     *
     * @param groupId 小组id
     * @return 小组成员组合实体
     */
    List<WorkerGroup> selectWorkerGroupInfoByGroupId(Integer groupId);

    /**
     * 查询所有的队长信息
     *
     * @return 队长实体集合
     */
    List<WorkerCaptain> selectAllGroupCaptains();

    /**
     * 根据指定任务流程查询所有的队长信息
     *
     * @param processId 任务流程
     * @return 队长实体集合
     */
    List<WorkerCaptain> selectAllGroupCaptainsByProcessId(Integer processId);

    /**
     * 查询指定小组的组长
     *
     * @param groupId 小组id
     * @return 组长实体
     */
    WorkerCaptain selectWorkerCaptainByGroupId(Integer groupId);

    /**
     * 查询某个小组在指定时间内的加工数据
     *
     * @param groupId   小组id
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return
     */
    WorkerCaptain selectWorkerCaptainWithinTime(
            @Param("groupId") Integer groupId,
            @Param("startTime") Date startTime,
            @Param("endTime") Date endTime,
            @Param("needFinish") Boolean needFinish
    );

    /**
     * 查询工单编号对应的所有加工小组，按加工优先级
     *
     * @param taskNo 工单编号
     * @return 工人小组实体集合
     */
    List<WorkerGroup> selectWorkerCaptainGroupsInOrder(String taskNo);

    /**
     * 查询工单编号对应的任务未完成加工小组，按加工优先级
     *
     * @param taskNo 工单编号
     * @return 工人小组实体集合
     */
    List<WorkerGroup> selectWorkerCaptainGroupsUnFinishInOrder(String taskNo);
}