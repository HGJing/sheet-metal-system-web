package web.sms.mvc.entity;

import java.util.Date;

public class Departure {
    private Integer departureId;

    private Integer departureInitiatorId;
    /*派车单发起者的信息*/
    private User departureInitiator;

    private Integer departureDriverId;
    /*派车单司机的信息*/
    private User departureDriver;

    private Integer departureFirstCustomerId;
    /*首达客户的名字*/
    private String departureFirstCustomerName;

    private Date departureStartTime;

    private String departureAimPlace;

    private String departureTransition;

    private Double departureCostMoney;

    private String departureCostMoneyDesc;

    private Double departureCostTime;

    private String departureCostTimeDesc;

    private Date departureBackTime;

    private Integer departureReceiverId;
    /*回厂收货人的名字*/
    private String departureReceiverName;

    /*订单状态*/
    private Integer departureStatus;
    public final static int NOT_EXIST = -1;  // 被删除
    public final static int UNANSWERED = 0;  // 未接单
    public final static int RECEIPT = 1;     // 已接单
    public final static int DELIVERING = 2;  // 送货中
    public final static int ARRIVAL = 3;     // 到货


    private Double departureRealCostTime;

    private String departureOvertimeResult;

    public Integer getDepartureId() {
        return departureId;
    }

    public void setDepartureId(Integer departureId) {
        this.departureId = departureId;
    }

    public Integer getDepartureInitiatorId() {
        return departureInitiatorId;
    }

    public void setDepartureInitiatorId(Integer departureInitiatorId) {
        this.departureInitiatorId = departureInitiatorId;
    }

    public Integer getDepartureDriverId() {
        return departureDriverId;
    }

    public void setDepartureDriverId(Integer departureDriverId) {
        this.departureDriverId = departureDriverId;
    }

    public Integer getDepartureFirstCustomerId() {
        return departureFirstCustomerId;
    }

    public void setDepartureFirstCustomerId(Integer departureFirstCustomerId) {
        this.departureFirstCustomerId = departureFirstCustomerId;
    }

    public Date getDepartureStartTime() {
        return departureStartTime;
    }

    public void setDepartureStartTime(Date departureStartTime) {
        this.departureStartTime = departureStartTime;
    }

    public String getDepartureAimPlace() {
        return departureAimPlace;
    }

    public void setDepartureAimPlace(String departureAimPlace) {
        this.departureAimPlace = departureAimPlace == null ? null : departureAimPlace.trim();
    }

    public String getDepartureTransition() {
        return departureTransition;
    }

    public void setDepartureTransition(String departureTransition) {
        this.departureTransition = departureTransition == null ? null : departureTransition.trim();
    }

    public Double getDepartureCostMoney() {
        return departureCostMoney;
    }

    public void setDepartureCostMoney(Double departureCostMoney) {
        this.departureCostMoney = departureCostMoney;
    }

    public String getDepartureCostMoneyDesc() {
        return departureCostMoneyDesc;
    }

    public void setDepartureCostMoneyDesc(String departureCostMoneyDesc) {
        this.departureCostMoneyDesc = departureCostMoneyDesc == null ? null : departureCostMoneyDesc.trim();
    }

    public Double getDepartureCostTime() {
        return departureCostTime;
    }

    public void setDepartureCostTime(Double departureCostTime) {
        this.departureCostTime = departureCostTime;
    }

    public String getDepartureCostTimeDesc() {
        return departureCostTimeDesc;
    }

    public void setDepartureCostTimeDesc(String departureCostTimeDesc) {
        this.departureCostTimeDesc = departureCostTimeDesc == null ? null : departureCostTimeDesc.trim();
    }

    public Date getDepartureBackTime() {
        return departureBackTime;
    }

    public void setDepartureBackTime(Date departureBackTime) {
        this.departureBackTime = departureBackTime;
    }

    public Integer getDepartureReceiverId() {
        return departureReceiverId;
    }

    public void setDepartureReceiverId(Integer departureReceiverId) {
        this.departureReceiverId = departureReceiverId;
    }

    public Integer getDepartureStatus() {
        return departureStatus;
    }

    public void setDepartureStatus(Integer departureStatus) {
        this.departureStatus = departureStatus;
    }

    public Double getDepartureRealCostTime() {
        return departureRealCostTime;
    }

    public void setDepartureRealCostTime(Double departureRealCostTime) {
        this.departureRealCostTime = departureRealCostTime;
    }

    public String getDepartureOvertimeResult() {
        return departureOvertimeResult;
    }

    public void setDepartureOvertimeResult(String departureOvertimeResult) {
        this.departureOvertimeResult = departureOvertimeResult == null ? null : departureOvertimeResult.trim();
    }

    public User getDepartureInitiator() {
        return departureInitiator;
    }

    public void setDepartureInitiator(User departureInitiator) {
        this.departureInitiator = departureInitiator;
    }

    public User getDepartureDriver() {
        return departureDriver;
    }

    public void setDepartureDriver(User departureDriver) {
        this.departureDriver = departureDriver;
    }

    public String getDepartureFirstCustomerName() {
        return departureFirstCustomerName;
    }

    public void setDepartureFirstCustomerName(String departureFirstCustomerName) {
        this.departureFirstCustomerName = departureFirstCustomerName;
    }

    public String getDepartureReceiverName() {
        return departureReceiverName;
    }

    public void setDepartureReceiverName(String departureReceiverName) {
        this.departureReceiverName = departureReceiverName;
    }
}