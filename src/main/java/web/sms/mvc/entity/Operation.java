package web.sms.mvc.entity;

import java.util.Date;

public class Operation {
    private Integer operationId;

    private String operationDesc;

    private Date operationTime;

    private Integer operationOperatorId;

    private Integer operationType;

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public String getOperationDesc() {
        return operationDesc;
    }

    public void setOperationDesc(String operationDesc) {
        this.operationDesc = operationDesc == null ? null : operationDesc.trim();
    }

    public Date getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(Date operationTime) {
        this.operationTime = operationTime;
    }

    public Integer getOperationOperatorId() {
        return operationOperatorId;
    }

    public void setOperationOperatorId(Integer operationOperatorId) {
        this.operationOperatorId = operationOperatorId;
    }

    public Integer getOperationType() {
        return operationType;
    }

    public void setOperationType(Integer operationType) {
        this.operationType = operationType;
    }
}