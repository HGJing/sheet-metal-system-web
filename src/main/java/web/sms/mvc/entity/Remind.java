package web.sms.mvc.entity;

import web.sms.core.utils.MessageFormatter;

import java.util.Date;

public class Remind {
    private Integer remindId;

    private Integer remindUserId;

    private String remindContent;

    private Integer remindType;
    public final static int NORMAL_TYPE = 0;
    public final static int URL_REMIND_TYPE = 1;
    public final static int ORDER_REMIND_TYPE = 2;
    public final static int TASK_REMIND_TYPE = 3;
    public final static int USER_REMIND_TYPE = 4;
    public final static int GROUP_REMIND_TYPE = 5;

    private Integer remindTargetId;

    private String remindTargetUrl;

    private Date remindTime;

    private Integer remindStatus;
    public final static int NOT_READ_STATUS = -1;
    public final static int HAVE_READ_STATUS = 0;

    public Remind(Integer remindUserId, String remindContent) {
        this.remindUserId = remindUserId;
        this.remindContent = remindContent;
    }

    // 构造方法提出参数
    public Remind(Integer remindUserId, String remindContent, Object... args) {
        this(remindUserId, MessageFormatter.arrayFormat(remindContent, args).getMessage());
    }

    public Remind() {
    }

    public Remind(Integer remindUserId, String remindContent, Integer remindTargetId) {
        this(remindUserId, remindContent);
        this.remindTargetId = remindTargetId;
    }

    public Remind(Integer remindUserId, String remindContent, String remindTargetUrl) {
        this(remindUserId, remindContent);
        this.remindTargetUrl = remindTargetUrl;
    }

    public Integer getRemindId() {
        return remindId;
    }

    public void setRemindId(Integer remindId) {
        this.remindId = remindId;
    }

    public Integer getRemindUserId() {
        return remindUserId;
    }

    public void setRemindUserId(Integer remindUserId) {
        this.remindUserId = remindUserId;
    }

    public String getRemindContent() {
        return remindContent;
    }

    public void setRemindContent(String remindContent) {
        this.remindContent = remindContent == null ? null : remindContent.trim();
    }

    public Integer getRemindType() {
        return remindType;
    }

    public void setRemindType(Integer remindType) {
        this.remindType = remindType;
    }

    public Integer getRemindTargetId() {
        return remindTargetId;
    }

    public void setRemindTargetId(Integer remindTargetId) {
        this.remindTargetId = remindTargetId;
    }

    public String getRemindTargetUrl() {
        return remindTargetUrl;
    }

    public void setRemindTargetUrl(String remindTargetUrl) {
        this.remindTargetUrl = remindTargetUrl == null ? null : remindTargetUrl.trim();
    }

    public Date getRemindTime() {
        return remindTime;
    }

    public void setRemindTime(Date remindTime) {
        this.remindTime = remindTime;
    }

    public Integer getRemindStatus() {
        return remindStatus;
    }

    public void setRemindStatus(Integer remindStatus) {
        this.remindStatus = remindStatus;
    }
}