package web.sms.mvc.entity;

public class TaskAssignment {
    private Integer taskAssignmentId;

    private String taskNo;

    private Integer taskProcessId;

    private Process taskProcess;

    private Integer taskStatus;

    private Integer taskWorkerGroupId;

    // 客户基本属性
    private User customer;

    // 自定义属性，用于统计任务前置未完成的任务
    private Integer preDemandNum;

    public static final int TASK_FINISHED = 0;
    public static final int TASK_NOT_START = 1;

    public Integer getTaskAssignmentId() {
        return taskAssignmentId;
    }

    public void setTaskAssignmentId(Integer taskAssignmentId) {
        this.taskAssignmentId = taskAssignmentId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo == null ? null : taskNo.trim();
    }

    public Integer getTaskProcessId() {
        return taskProcessId;
    }

    public void setTaskProcessId(Integer taskProcessId) {
        this.taskProcessId = taskProcessId;
    }

    public Integer getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(Integer taskStatus) {
        this.taskStatus = taskStatus;
    }

    public Integer getTaskWorkerGroupId() {
        return taskWorkerGroupId;
    }

    public Process getTaskProcess() {
        return taskProcess;
    }

    public void setTaskProcess(Process taskProcess) {
        this.taskProcess = taskProcess;
    }

    public Integer getPreDemandNum() {
        return preDemandNum;
    }

    public void setPreDemandNum(Integer preDemandNum) {
        this.preDemandNum = preDemandNum;
    }

    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }

    public void setTaskWorkerGroupId(Integer taskWorkerGroupId) {
        this.taskWorkerGroupId = taskWorkerGroupId;
    }
}