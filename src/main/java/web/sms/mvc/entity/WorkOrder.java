package web.sms.mvc.entity;

import java.util.Date;
import java.util.List;

public class WorkOrder {

    public final static int NOT_EXIST_STATUS = -1;
    public final static int NOT_DELIVERY_STATUS = 0;
    public final static int IN_THE_LIBRARY_STATUS = 1;
    public final static int COMPLETED_STATUS = 2;
    public final static int SHIPPED_STATUS = 3;
    public final static int ENDED_STATUS = 4;
    public final static int NOT_PAID_STATUS = -1;
    public final static int PAID_STATUS = 0;

    private Integer orderId;

    private String orderNumber;

    private Date orderAddTime;

    private Date orderFinishTime;

    private Integer orderPaymentStatus;

    private String orderTaskNo;

    private List<TaskAssignment> orderTasks;

    private Integer orderCustomerId;

    private Integer orderPriority;

    private Integer orderStatus;

    private Double orderPrice;

    private String orderCustomerName;

    private Integer orderChargeManagerId;

    private Integer orderIsQuick;

    private Integer orderProductionManagerId;

    private String orderCustomerMessage;

    private String orderProductionDesc;

    private String orderProductionColor;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber == null ? null : orderNumber.trim();
    }

    public Date getOrderAddTime() {
        return orderAddTime;
    }

    public void setOrderAddTime(Date orderAddTime) {
        this.orderAddTime = orderAddTime;
    }

    public Date getOrderFinishTime() {
        return orderFinishTime;
    }

    public void setOrderFinishTime(Date orderFinishTime) {
        this.orderFinishTime = orderFinishTime;
    }

    public Integer getOrderPaymentStatus() {
        return orderPaymentStatus;
    }

    public void setOrderPaymentStatus(Integer orderPaymentStatus) {
        this.orderPaymentStatus = orderPaymentStatus;
    }

    public String getOrderTaskNo() {
        return orderTaskNo;
    }

    public void setOrderTaskNo(String orderTaskNo) {
        this.orderTaskNo = orderTaskNo == null ? null : orderTaskNo.trim();
    }

    public Integer getOrderCustomerId() {
        return orderCustomerId;
    }

    public void setOrderCustomerId(Integer orderCustomerId) {
        this.orderCustomerId = orderCustomerId;
    }

    public Integer getOrderPriority() {
        return orderPriority;
    }

    public void setOrderPriority(Integer orderPriority) {
        this.orderPriority = orderPriority;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Double getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(Double orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getOrderCustomerName() {
        return orderCustomerName;
    }

    public void setOrderCustomerName(String orderCustomerName) {
        this.orderCustomerName = orderCustomerName == null ? null : orderCustomerName.trim();
    }

    public Integer getOrderChargeManagerId() {
        return orderChargeManagerId;
    }

    public void setOrderChargeManagerId(Integer orderChargeManagerId) {
        this.orderChargeManagerId = orderChargeManagerId;
    }

    public Integer getOrderIsQuick() {
        return orderIsQuick;
    }

    public void setOrderIsQuick(Integer orderIsQuick) {
        this.orderIsQuick = orderIsQuick;
    }

    public Integer getOrderProductionManagerId() {
        return orderProductionManagerId;
    }

    public void setOrderProductionManagerId(Integer orderProductionManagerId) {
        this.orderProductionManagerId = orderProductionManagerId;
    }

    public String getOrderCustomerMessage() {
        return orderCustomerMessage;
    }

    public void setOrderCustomerMessage(String orderCustomerMessage) {
        this.orderCustomerMessage = orderCustomerMessage == null ? null : orderCustomerMessage.trim();
    }

    public String getOrderProductionDesc() {
        return orderProductionDesc;
    }

    public void setOrderProductionDesc(String orderProductionDesc) {
        this.orderProductionDesc = orderProductionDesc == null ? null : orderProductionDesc.trim();
    }

    public List<TaskAssignment> getOrderTasks() {
        return orderTasks;
    }

    public void setOrderTasks(List<TaskAssignment> orderTasks) {
        this.orderTasks = orderTasks;
    }

    public String getOrderProductionColor() {
        return orderProductionColor;
    }

    public void setOrderProductionColor(String orderProductionColor) {
        this.orderProductionColor = orderProductionColor == null ? null : orderProductionColor.trim();
    }
}