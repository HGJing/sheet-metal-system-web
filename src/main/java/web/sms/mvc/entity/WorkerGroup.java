package web.sms.mvc.entity;

public class WorkerGroup extends WorkerGroupKey {
    private Boolean workerIsLeader;
    private Integer workerProcessId;

    public WorkerGroup(WorkerGroupKey workerGroupKey, Boolean workerIsLeader) {
        this.setWorkerGroupId(workerGroupKey.getWorkerGroupId());
        this.setWorkerId(workerGroupKey.getWorkerId());
        this.workerIsLeader = workerIsLeader;
    }

    public WorkerGroup() {
    }

    public Boolean getWorkerIsLeader() {
        return workerIsLeader;
    }

    public void setWorkerIsLeader(Boolean workerIsLeader) {
        this.workerIsLeader = workerIsLeader;
    }

    public Integer getWorkerProcessId() {
        return workerProcessId;
    }

    public void setWorkerProcessId(Integer workerProcessId) {
        this.workerProcessId = workerProcessId;
    }
}