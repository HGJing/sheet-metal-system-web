package web.sms.mvc.entity;

public class WorkerGroupKey {
    private Integer workerGroupId;
    private Integer workerId;

    // 用于查询工人团的所有用户时
    private User worker;

    public WorkerGroupKey(Integer workerGroupId, Integer workerId) {
        this.workerGroupId = workerGroupId;
        this.workerId = workerId;
    }

    public WorkerGroupKey() {
    }

    public Integer getWorkerGroupId() {
        return workerGroupId;
    }

    public void setWorkerGroupId(Integer workerGroupId) {
        this.workerGroupId = workerGroupId;
    }

    public Integer getWorkerId() {
        return workerId;
    }

    public User getWorker() {
        return worker;
    }

    public void setWorker(User worker) {
        this.worker = worker;
    }

    public void setWorkerId(Integer workerId) {
        this.workerId = workerId;
    }
}