package web.sms.mvc.entity.pojo;

import web.sms.mvc.entity.User;

import java.util.Date;

/**
 * 时间： 2018/1/1
 * 司机派车单信息
 *
 * @author Eoly
 */
public class DriverDepartureInfo {
    /*司机信息*/
    private User driverInfo;
    /*总耗时*/
    private Double totalTimeCost = 0d;
    /*总派车费用*/
    private Double totalMoneyCost = 0d;
    /*总派车节约用时*/
    private Double totalSavingTime = 0d;
    /*筛选范围：起始时间*/
    private Date startTime;
    /*筛选范围：结束时间*/
    private Date endTime;

    public DriverDepartureInfo() {
    }

    public DriverDepartureInfo(Date startTime, Date endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public User getDriverInfo() {
        return driverInfo;
    }

    public void setDriverInfo(User driverInfo) {
        this.driverInfo = driverInfo;
    }

    public Double getTotalTimeCost() {
        return totalTimeCost;
    }

    public void setTotalTimeCost(Double totalTimeCost) {
        this.totalTimeCost = totalTimeCost;
    }

    public Double getTotalMoneyCost() {
        return totalMoneyCost;
    }

    public void setTotalMoneyCost(Double totalMoneyCost) {
        this.totalMoneyCost = totalMoneyCost;
    }

    public Double getTotalSavingTime() {
        return totalSavingTime;
    }

    public void setTotalSavingTime(Double totalSavingTime) {
        this.totalSavingTime = totalSavingTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
