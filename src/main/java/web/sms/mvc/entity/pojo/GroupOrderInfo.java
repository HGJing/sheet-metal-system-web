package web.sms.mvc.entity.pojo;

import web.sms.mvc.entity.User;
import web.sms.mvc.entity.WorkOrder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 时间： 2018/1/1
 * 一段时间内该工人加加工工单统计
 *
 * @author Eoly
 */
public class GroupOrderInfo {

    /*工单产值和*/
    private Double orderPriceSum = 0d;
    /*工单总数*/
    private Integer orderNumSum = 0;

    /*工单列表*/
    private List<WorkOrder> orders = new ArrayList<WorkOrder>();

    public GroupOrderInfo() {
    }

    public Double getOrderPriceSum() {
        return orderPriceSum;
    }

    public void setOrderPriceSum(Double orderPriceSum) {
        this.orderPriceSum = orderPriceSum;
    }

    public Integer getOrderNumSum() {
        return orderNumSum;
    }

    public List<WorkOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<WorkOrder> orders) {
        this.orders = orders;
    }

    public void setOrderNumSum(Integer orderNumSum) {
        this.orderNumSum = orderNumSum;
    }
}
