package web.sms.mvc.entity.pojo;

import web.sms.mvc.entity.User;

/**
 * 时间： 2017/12/23
 * 队长实体
 *
 * @author Eoly
 */
public class WorkerCaptain extends User {
    private Integer groupId;
    private Integer groupSize;
    private Integer processId;
    private GroupOrderInfo groupOrderInfo = new GroupOrderInfo();

    public Integer getGroupId() {
        return groupId;
    }

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getGroupSize() {
        return groupSize;
    }

    public GroupOrderInfo getGroupOrderInfo() {
        return groupOrderInfo;
    }

    public void setGroupOrderInfo(GroupOrderInfo groupOrderInfo) {
        this.groupOrderInfo = groupOrderInfo;
    }

    public void setGroupSize(Integer groupSize) {
        this.groupSize = groupSize;
    }
}
