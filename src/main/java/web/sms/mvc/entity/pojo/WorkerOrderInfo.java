package web.sms.mvc.entity.pojo;

import web.sms.mvc.entity.User;

import java.util.Date;

/**
 * 时间： 2018/1/1
 * 一段时间内该工人加加工工单统计
 *
 * @author Eoly
 */
public class WorkerOrderInfo {

    /*工人信息*/
    private User workerInfo;
    /*工单产值和*/
    private Double orderPriceSum = 0d;
    /*工单总数*/
    private Integer orderNumSum = 0;

    /*筛选范围：起始时间*/
    private Date startTime;
    /*筛选范围：结束时间*/
    private Date endTime;

    public WorkerOrderInfo() {
    }

    public WorkerOrderInfo(Date startTime, Date endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public User getWorkerInfo() {
        return workerInfo;
    }

    public void setWorkerInfo(User workerInfo) {
        this.workerInfo = workerInfo;
    }

    public Double getOrderPriceSum() {
        return orderPriceSum;
    }

    public void setOrderPriceSum(Double orderPriceSum) {
        this.orderPriceSum = orderPriceSum;
    }

    public Integer getOrderNumSum() {
        return orderNumSum;
    }

    public void setOrderNumSum(Integer orderNumSum) {
        this.orderNumSum = orderNumSum;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
