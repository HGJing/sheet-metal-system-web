package web.sms.mvc.service;

import com.github.pagehelper.PageInfo;
import web.sms.mvc.entity.Departure;
import web.sms.mvc.entity.pojo.DriverDepartureInfo;

import java.util.Date;

/**
 * 时间： 2018/1/1
 * 派车单业务接口
 *
 * @author Eoly
 */
public interface DepartureService {

    /**
     * 添加派车单
     * 权限要求：
     * departure:admin|departure:add
     *
     * @param departure 派车单实体[未列出属性是不可添加的]
     *                  departureFirstCustomerId    首达客户id[2选1，该项优先]
     *                  departureFirstCustomerName  首达客户名[2选1]
     *                  departureDriverId           [必须]派车司机id
     *                  departureAimPlace           [必须]目的地
     *                  departureTransition         [必须]转场
     *                  departureCostMoney          [必须]预计经费
     *                  departureCostMoneyDesc      [必须]预计经费的详情
     *                  departureCostTime           [必须]预计时间
     *                  departureCostTimeDesc       [必须]预计时间详情
     */
    void addDeparture(Departure departure);

    /**
     * 重新分配派车单所对应的司机，只能在计时开始前
     * 权限要求:
     * departure:admin|发起车单的文员
     *
     * @param departureId 车单id
     * @param driverId    司机id
     */
    void doDispenseDriverForDeparture(Integer departureId, Integer driverId);

    /**
     * 查询所有的派车单集合
     * 权限要求：
     * departure:admin|departure:find
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @return 包含派车单实体集合的页面信息实体
     */
    PageInfo<Departure> findClerkDepartures(Integer pageNum, Integer pageSize);

    /**
     * 查询司机所对应的派车单集合
     *
     * @param driverId  司机id（为空则查询当前司机）
     * @param pageNum   页码 （为空则不分页）
     * @param pageSize  每页记录数（为空则不分页）
     * @param startTime 开始时间（为空则不查开始时间）
     * @param endTime   结束时间（为空则不查结束时间）
     * @return 包含派车单实体集合的页面信息实体
     */
    PageInfo<Departure> findDriverDepartures(Integer driverId, Integer pageNum,
                                             Integer pageSize, Date startTime, Date endTime);

    /**
     * 查询派车单的详细信息
     *
     * @param departureId 派车单id
     * @return 派车单实体
     */
    Departure findDepartureDetailInfo(Integer departureId);

    /**
     * 确立派车单已接单
     * 权限要求
     * departure:admin|departure:add|司机本身
     *
     * @param departureId 车单id
     */
    void makeSureDepartureReceipt(Integer departureId);

    /**
     * 派车单对应文员选择派车单发车动作，派车时间开始计时
     *
     * @param departureId 派车单id
     */
    void doDeparting(Integer departureId);

    /**
     * 回场收货计时结束
     *
     * @param departureId 车单id
     * @param receiverId  收货人id
     */
    void doReceive(Integer departureId, Integer receiverId);

    /**
     * 回场收货计时结束
     *
     * @param departureId  车单id
     * @param receiverName 收货人名字
     */
    void doReceive(Integer departureId, String receiverName);

    /**
     * 特殊情况下派车单的修改，若司机接单则不能修改
     *
     * @param departure 派车单实体
     */
    void changeDeparture(Departure departure);

    /**
     * 填写超时原因
     * 车单状态需要为货到且超时
     *
     * @param departureId 派车单实体，必须与操作司机匹配
     * @param result      超时原因
     */
    void doFillInResultOfLater(Integer departureId, String result);

    /**
     * 删除派车单
     * 权限要求:
     * departure:admin|departure:delete
     *
     * @param departureId 派车单id
     */
    void deleteDeparture(Integer departureId);

    /**
     * 获得某司机的车单总时间
     * 权限要求：
     * driver:find|司机自身
     *
     * @param driverId 司机id
     * @return 时间单位（h）
     */
    Double getAllTimeCostInADriver(Integer driverId);

    /**
     * 查询指定司机在一定时间内的派车状况统计
     * 权限要求：
     * user:admin|user:find|司机自身
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 司机信息实体
     */
    DriverDepartureInfo findDriverInfoInCycle(Integer driverId, Date startTime, Date endTime);

}
