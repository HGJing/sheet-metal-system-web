package web.sms.mvc.service;

import web.sms.mvc.entity.Process;

import java.util.List;

/**
 * 时间： 2017/12/19
 *
 * @author Eoly
 */
public interface ProcessService {
    /**
     * 获取所有的加工流程
     *
     * @return 流程实体
     */
    List<Process> findAllProcessExceptNormal();

    /**
     * 获取所有的加工流程
     * 除去打包和发货
     *
     * @return 流程实体
     */
    List<Process> findAllProcess();
}
