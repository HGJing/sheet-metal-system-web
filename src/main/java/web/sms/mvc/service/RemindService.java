package web.sms.mvc.service;

import com.github.pagehelper.PageInfo;
import web.sms.mvc.entity.Remind;


/**
 * 时间： 2017/12/28
 *
 * @author Eoly
 */
public interface RemindService {
    /**
     * 生成提醒
     *
     * @param remind 提醒实体
     *               remindUserId       [必须]
     *               remindContent      [必须]
     *               remindTargetId     [若type=0,无需;若type>1,必须]
     *               remindTargetUrl    [若type=0,无需;若type=1,必须]
     * @param type   提醒类型
     */
    void doRemind(Remind remind, Integer type);

    /**
     * 查询用户的提醒
     *
     * @param pageNum  第几页
     * @param pageSize 每页的提醒数量
     * @return 提醒实体集合
     */
    PageInfo<Remind> findUserReminds(Integer pageNum, Integer pageSize);

    /**
     * 将指定提醒设定为已读
     *
     * @param remindId 提醒id
     */
    void doReadRemind(Integer remindId);

    /**
     * 获取相应用户未读的提醒数量
     *
     * @return 提醒的条数
     */
    Integer getNotReadReminds();

}
