package web.sms.mvc.service;

import web.sms.mvc.entity.Role;

import java.util.List;

/**
 * 时间： 2017/12/17
 *
 * @author Eoly
 */
public interface RoleService {
    /**
     * 查询所有的角色
     * 不包括admin
     *
     * @return 角色实体列表
     */
    List<Role> findAllRoles();
}
