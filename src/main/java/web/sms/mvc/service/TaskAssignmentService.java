package web.sms.mvc.service;

import web.sms.mvc.entity.TaskAssignment;
import web.sms.mvc.entity.User;

import java.util.List;

/**
 * 时间： 2017/12/23
 * 加工流程任务分配业务逻辑
 *
 * @author Eoly
 */
public interface TaskAssignmentService {
    /**
     * 分配加工任务流程到工人组
     * 权限要求
     * order:admin|order:edit
     *
     * @param taskId  任务流程id
     * @param groupId 工人组id
     */
    void doDistributionTask(Integer taskId, Integer groupId);

    /**
     * 查询当前工人组长所负责的任务流程
     *
     * @return 任务流程实体集合
     */
    List<TaskAssignment> findLeaderTasks();

    /**
     * 完成指定任务
     * 权限要求
     * 该任务对应的工人组长
     *
     * @param taskId  任务id
     * @param groupId 该工人组id
     */
    void doFinishTask(Integer taskId, Integer groupId);

    /**
     * 查询指定任务的前置任务
     *
     * @param taskId 任务id
     * @return 任务实体集合
     */
    List<TaskAssignment> findPreTasks(Integer taskId);
}
