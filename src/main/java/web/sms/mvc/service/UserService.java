package web.sms.mvc.service;

import com.github.pagehelper.PageInfo;
import web.sms.core.pojo.ErrorInfo;
import web.sms.mvc.entity.User;

import java.util.List;

/**
 * 时间： 2017/12/12
 * 用户业务接口
 *
 * @author Eoly
 */
public interface UserService {

    /**
     * 登录
     * 默认
     * rememberMe:false
     *
     * @param username 用户名
     * @param password 密码
     * @return 登录的用户实体
     */
    User doLogin(String username, String password);

    /**
     * 登录
     *
     * @param username   用户名
     * @param password   密码
     * @param rememberMe 记住我
     * @return 登录的用户实体
     */
    User doLogin(String username, String password, Boolean rememberMe);

    /**
     * 用户注销
     */
    void doLogout();

    /**
     * 添加用户信息
     * 权限要求:
     * user:admin|user:add
     *
     * @param user 用户实体
     *             userLoginName    [必须]
     *             userPassword     [必须]
     *             userRole         [必须]
     *             userRealName     [必须]
     *             userNickName
     *             userPhoneNumber
     *             userEmail
     */
    void addUser(User user);

    /**
     * 编辑用户信息
     * 权限要求：
     * user:admin|user:edit|用户自身
     *
     * @param user 用户实体
     *             userId           [必须]
     *             userRealName
     *             userNickName
     *             userPhoneNumber
     *             userRole         [当操作用户具有权限时可修改]
     *             userEmail
     */
    void editUser(User user);

    /**
     * 修改用户密码
     * 权限要求：
     * user:admin|user:edit|用户自身
     *
     * @param userId      用户id
     *                    若为null则是修改当前登录用户
     * @param oldPassword 旧密码
     *                    当欲修改的用户自身没有密码时，可为null
     *                    当管理员修改用户密码时，可为null
     * @param newPassword 新密码
     *                    不可为空
     *                    不可为null
     */
    void editPassword(Integer userId, String oldPassword, String newPassword);

    /**
     * 冻结用户
     * 权限要求：
     * user:admin|user:edit
     *
     * @param userId 用户id
     */
    void doFreezeUser(Integer userId);

    /**
     * 用户解冻
     * 权限要求：
     * user:admin|user:edit
     *
     * @param userId 用户id
     */
    void doUnfreezeUser(Integer userId);

    /**
     * 删除用户
     * 权限要求：
     * user:admin|user:edit|user:delete
     *
     * @param userId 用户id
     */
    void deleteUser(Integer userId);

    /**
     * 检查用户名是否已被使用
     * 权限需求：无
     *
     * @param loginName 用户名
     * @return true:已被使用，false:未被使用
     */
    Boolean isLoginNameExist(String loginName);

    /**
     * 根据用户id获取单个用户的基本信息
     * 权限要求：
     * 无
     *
     * @param userId 用户id
     * @return 用户实体
     * 基本信息：
     * userId
     * userOpenId
     * userLoginName
     * userRole
     * userRealName
     * userNickName
     * userPhoneNumber
     * userEmail
     */
    User findUserWithBaseInfo(Integer userId);

    /**
     * 查询用户的授权信息
     *
     * @param loginName 登录名
     * @return 用户实体
     */
    User findUserWithAuthenticationInfo(String loginName);

    /**
     * 查询用户的授权信息
     *
     * @param userId 用户id
     * @return 用户实体
     */
    User findUserWithAuthenticationInfo(Integer userId);

    /**
     * 按用户属性查询相应用户
     * 权限要求：
     * user:admin|user:find
     *
     * @param checkUser 用户实体，包含需要匹配的属性
     * @return 查询出的用户实体列表
     */
    List<User> findUserByAttributes(User checkUser);

    /**
     * 分页查询用户信息
     * 权限要求：
     * user:admin|user:find
     *
     * @param pageNumber 页码
     * @param pageSize   每页的大小
     * @return 包含用户集合的Page对象
     * 可查询除用户密码之外的所有属性
     */
    PageInfo<User> findUsersByPage(Integer pageNumber, Integer pageSize);

    /**
     * 分页查询用户信息
     * 权限要求：
     * user:admin|user:find
     *
     * @param pageNumber   页码
     * @param pageSize     每页的大小
     * @param userRealName 用户姓名筛选
     * @param userRole     用户角色筛选
     * @return 包含用户集合的Page对象
     * 可查询除用户密码之外的所有属性
     */
    PageInfo<User> findUsersByPageAndNameAndRole(Integer pageNumber, Integer pageSize,
                                                 String userRealName, String userRole);

    /**
     * 查询所有用户的数量
     *
     * @return 用户数量
     */
    Integer findAllUserCount();

    /**
     * 判断用户是否有角色
     *
     * @param userId   用户id
     * @param roleCode 角色代码
     * @return true：有，false：没有
     */
    Boolean hasRole(Integer userId, String roleCode);

    /**
     * 判断用户是否有角色
     *
     * @param userId    用户id
     * @param roleCode  角色代码
     * @param errorInfo 自定义错误信息
     * @return true：有，false：没有
     */
    Boolean hasRole(Integer userId, String roleCode, ErrorInfo errorInfo);

    /**
     * 获取当前登录的用户实体
     *
     * @return 用户实体
     */
    User getLoggedUser();

    /**
     * 通过openid获得用户
     *
     * @param openid
     * @return
     */
    User findUserByOpenId(String openid);
}
