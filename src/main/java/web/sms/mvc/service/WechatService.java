package web.sms.mvc.service;

/**
 * 时间： 2018/1/31
 *
 * @author Eoly
 */
public interface WechatService {

    /**
     * 获取access_token
     *
     * @param isReGet 是否重新获取（反之从缓存获取）
     * @return access_token
     */
    String getAccessToken(Boolean isReGet);

    /**
     * 绑定用户到当前微信号
     *
     * @param loginName 登录名
     * @param password  密码
     */
    void doBindUser(String loginName, String password);

    /**
     * 解绑用户与当前绑定微信号的关系
     * 需要权限：
     * user:admin|用户自身
     *
     * @param loginName 需要解绑的用户名（null为解绑自身）
     */
    void doFreeUser(String loginName);
}
