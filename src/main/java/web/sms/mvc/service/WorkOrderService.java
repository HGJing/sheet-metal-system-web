package web.sms.mvc.service;

import com.github.pagehelper.PageInfo;
import web.sms.mvc.entity.WorkOrder;
import web.sms.mvc.entity.pojo.WorkerOrderInfo;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 时间： 2017/12/18
 * 工单业务逻辑
 *
 * @author Eoly
 */
public interface WorkOrderService {

    /**
     * 文员添加一个工单
     * [生成操作记录]
     * 权限要求:
     * order:admin|order:add
     * <p>
     * 工单实体中的属性说明
     * orderId                  工单id                     [不可指定]
     * orderNumber              工单编号                   [不可指定]
     * orderPaymentStatus       工单结算状态
     * *                        -1：未收                   [默认]
     * *                        0：清欠
     * orderCustomerId          工单客户id                 [必须]
     * orderPriority            工单优先级
     * *                        0：正常                    [默认]
     * *                        1：急
     * *                        2：加急
     * orderStatus              工单状态                   [不可指定]
     * *                        -1：已删除
     * *                        0：未做                    [默认]
     * *                        1：在库
     * *                        2：完工
     * *                        3：已发
     * orderPrice               工单对应价格                [必须]
     * orderCustomerName        工单对应客户的名字           [不可指定]
     * orderProductionManagerId 工单对应的销售管理id         [必须]
     * orderChargeManagerId      工单对应的生产管理id         [不可指定]
     * orderIsQuick             工单是否处于最大优先级        [不可指定，默认:false]
     * orderCustomerMessage     用户留言                    [不可指定]
     * <p>
     * 工单任务流程分配实体集合属性说明
     * taskAssignmentId         实体id                     [不可指定]
     * taskNo                   任务编号                    [必须，且集合中的所有任务中必须相同]
     * taskProcessId            任务对应的加工流程的id       [必须，且集合中所有任务中不能相同]
     * taskStatus               任务状态                   [不可指定]
     * *                        -1：未完成                 [默认]
     * *                        0：已完成
     * taskWorkerGroupId        任务对应的加工工人组         [必须，且集合中的所有任务中必须相同]
     *
     * @param workOrder        工单实体
     * @param taskProcessIdSet 工单对应流程的id集合
     */
    Integer addWorkOrder(WorkOrder workOrder, Set<Integer> taskProcessIdSet);

    /**
     * 查询与用户相关的工单[可按条件]
     *
     * @param checkOrder 按条件查询[目前不对顾客和销售管理起作用]
     * @param pageNum    页码
     * @param pageSize   每页工单条数
     * @return 工单实体
     */
    PageInfo<WorkOrder> findWorkOrderByPage(WorkOrder checkOrder, Integer pageNum, Integer pageSize);

    /**
     * 查询销售管理一段时间内已完成的工单
     * 权限要求：
     * order:admin|order:find
     *
     * @param sManagerId 销售管理id (null即查询当前销售管理)
     * @param startTime  开始时间 (null及不规定时间范围，下同)
     * @param endTime    结束时间
     * @return 工单列表
     */
    List<WorkOrder> findSManageOrders(Integer sManagerId, Date startTime, Date endTime);

    /**
     * 根据工单id删除工单
     * 权限需求
     * order:admin|order:delete
     *
     * @param orderId 工单id
     */
    void deleteWorkOrder(Integer orderId);

    /**
     * 客户变更工单留言
     * 权限要求
     * 工单对应的客户本身
     *
     * @param orderId 工单id
     * @param message 新的留言
     */
    void editWorkOrderLeftMessage(Integer orderId, String message);

    /**
     * 使工单的支付状态从未收变为清欠
     * 权限要求
     * order:admin|clerk:admin
     *
     * @param orderId 工单id
     */
    void makeOrderPaid(Integer orderId);

    /**
     * 更改工单优先级
     * 权限要求
     * order:admin|smanager:admin
     *
     * @param orderId  工单id
     * @param priority 欲更改的优先级
     */
    void changeOrderPriority(Integer orderId, Integer priority);

    /**
     * 更改工单是否快速执行
     * 权限要求
     * order:admin|pmanager:admin
     *
     * @param orderId 工单id
     * @param isQuick 生产优先级
     */
    void changeOrderIsQuick(Integer orderId, Integer isQuick);

    /**
     * 查询工单详情
     *
     * @param orderId 工单id
     * @return 工单实体
     */
    WorkOrder findOrderDetail(Integer orderId);

    /**
     * 查询某个工人一段时间内加工的工单统计
     * 权限要求：
     * user:admin|user:find|工人自身
     *
     * @param workerId  工人id
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 统计信息实体
     */
    WorkerOrderInfo findOrderInfoWithWorkerInCycle(Integer workerId, Date startTime, Date endTime);

    /**
     * 获取一段时间内的所有已完成的工单的产值和
     * 权限要求：
     * order:admin|order:find
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 产值和
     */
    Double getSumOfOrderPriceInCycle(Date startTime, Date endTime);
}
