package web.sms.mvc.service;

import web.sms.mvc.entity.pojo.WorkerCaptain;
import web.sms.mvc.entity.WorkerGroup;
import web.sms.mvc.entity.WorkerGroupKey;

import java.util.Date;
import java.util.List;

/**
 * 时间： 2017/12/18
 * 工人组业务逻辑
 *
 * @author Eoly
 */
public interface WorkerGroupService {
    /**
     * 添加一个新的工人组
     * 权限要求
     * worker:admin|worker:add
     *
     * @param userIdList 指定的工人用户集合
     *                   指定用户的角色必须是worker
     * @param leaderId   组长的id
     *                   组长必须在该工人组中
     *                   并且组长有且只能有一个
     * @param processId  该组工人负责的加工流程
     * @return 新生成的工人组的id
     */
    Integer addNewWorkerGroup(List<Integer> userIdList, Integer leaderId, Integer processId);


    /**
     * 添加工人到工人组
     * worker:admin|worker:add
     *
     * @param workerGroupId 工人组id[工人组必须存在]
     * @param workerId      欲加入的工人id[指定用户的角色必须是worker]
     */
    void addToWorkerGroup(Integer workerId, Integer workerGroupId);

    /**
     * 添加多个用户到工人组
     * worker:admin|worker:add
     *
     * @param workerGroupId 工人组id[工人组必须存在]
     * @param userIdList    欲加入的工人id集合[指定用户的角色必须是worker]
     */
    void addToWorkerGroup(Integer workerGroupId, List<Integer> userIdList);


    /**
     * 令指定工人组的某个工人成为组长
     * worker:admin|worker:edit
     *
     * @param workerGroupKey 工人组实体
     *                       workerId      欲指定组长的工人id[该工人必须在工人组中]
     *                       workerGroupId 工人组id[工人组必须存在]
     */
    void makeWorkerToBeLeaderInWorkerGroup(WorkerGroupKey workerGroupKey);

    /**
     * 获取所有的工人队长以及其小组信息
     *
     * @return WorkerCaptain 实体
     */
    List<WorkerCaptain> findAllCaptain();

    /**
     * 通过某一加工流程查询与该加工流程有关的小组的组长
     *
     * @param processId 加工流程id
     * @return 组长实体集合
     */
    List<WorkerCaptain> findCaptainsByProcessId(Integer processId);

    /**
     * 查询指定小组的组长信息
     *
     * @param groupId 小组id
     * @return 组长实体
     */
    WorkerCaptain findCaptainByGroupId(Integer groupId);

    /**
     * 查询指定时间内工人组的工单统计
     * 权限要求：
     * order:admin|order:find
     *
     * @param groupId   工人组id
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 队长代表的工人组信息
     */
    WorkerCaptain findCaptainDetailWithinTime(
            Integer groupId,
            Date startTime,
            Date endTime
    );

    /**
     * 查询小组信息以及成员信息
     *
     * @param groupId 小组id
     * @return 小组成员实体集合
     */
    List<WorkerGroup> findGroupInfo(Integer groupId);


}
