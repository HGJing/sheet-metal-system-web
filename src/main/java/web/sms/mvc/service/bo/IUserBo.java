package web.sms.mvc.service.bo;

import cn.palerock.core.shiro.useradmin.bo.UserBo;
import cn.palerock.utils.StringUtils;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import web.sms.core.exception.ServiceException;
import web.sms.mvc.dao.UserMapper;
import web.sms.mvc.entity.User;

import javax.annotation.Resource;
import java.util.*;

/**
 * 时间： 2017/12/11
 * 该业务类主要用于权限认证授权时调用
 *
 * @author Eoly
 */
@Component("userBo")
public class IUserBo implements UserBo {

    private final UserMapper userMapper;

    @Resource(name = "permsMap")
    private Map<String, String> permsMap;

    @Autowired
    public IUserBo(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public String getPassword(String loginName) {
        User user = userMapper.selectAuthenticationByLoginName(loginName);
        if (isInvalidUser(user)) {
            throw new UnknownAccountException("用户找不到或信息不完整");
        }
        // 筛选用户状态0：正常，-1：冻结，-2已删除[方便于数据找回]
        switch (user.getUserStatus()) {
            case -1:
                throw new LockedAccountException("该用户已被冻结，如需解冻请咨询管理员");
            case -2:
                // -2表示用户已被删除，即不存在该用户
                throw new UnknownAccountException("用户找不到或信息不完整");
            default:
        }
        return user.getUserPassword();
    }

    @Override
    public Set<String> getRoles(String loginName) {
        User user = userMapper.selectAuthenticationByLoginName(loginName);
        if (isInvalidUser(user)) {
            throw new ServiceException("用户找不到或信息不完整");
        }
        return new HashSet<String>(Collections.singletonList(user.getUserRole()));
    }

    @Override
    public Set<String> getPermissions(String loginName) {
        User user = userMapper.selectAuthenticationByLoginName(loginName);
        if (isInvalidUser(user)) {
            throw new ServiceException("用户找不到或信息不完整");
        }
        String role = user.getUserRole();
        String perms = permsMap.get(role);
        return StringUtils.parseStringifyStr(perms);
    }

    @Override
    public Object getUniqueKey(String loginName) {
        User user = userMapper.selectAuthenticationByLoginName(loginName);
        if (isInvalidUser(user)) {
            return null;
        }
        return user.getUserId();
    }

    @Override
    public Boolean isFreezing(String loginName) {
        User user = userMapper.selectAuthenticationByLoginName(loginName);
        if (isInvalidUser(user)) {
            throw new ServiceException("用户找不到或信息不完整");
        }
        if (user.getUserStatus() != null && user.getUserStatus() < User.NORMAL_USER_STATUS) {
            return true;
        }
        return false;
    }

    /**
     * 判断获得的实体是否是有效的用户实体
     *
     * @param user 用户实体
     */
    private boolean isInvalidUser(User user) {
        if (user == null || user.getUserId() == null || user.getUserPassword() == null || user.getUserRole() == null || user.getUserStatus() == null) {
            return true;
        }
        return false;
    }

}
