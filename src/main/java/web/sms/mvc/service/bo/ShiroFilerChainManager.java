package web.sms.mvc.service.bo;

import org.apache.shiro.web.filter.mgt.DefaultFilterChainManager;
import org.apache.shiro.web.filter.mgt.NamedFilterList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import web.sms.core.pojo.UrlFilter;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用于管理url对应的权限拦截
 */
@Service
public class ShiroFilerChainManager {
    private final DefaultFilterChainManager filterChainManager;

    private Map<String, NamedFilterList> defaultFilterChains;

    @Autowired
    public ShiroFilerChainManager(DefaultFilterChainManager filterChainManager) {
        this.filterChainManager = filterChainManager;
    }

    @PostConstruct
    public void init() {
        defaultFilterChains = new HashMap<String, NamedFilterList>(filterChainManager.getFilterChains());
    }

    public void initFilterChains(List<UrlFilter> urlFilters) {
        //1、首先删除以前老的filter chain并注册默认的
        filterChainManager.getFilterChains().clear();
        if (defaultFilterChains != null) {
            filterChainManager.getFilterChains().putAll(defaultFilterChains);
        }

        //2、循环URL Filter 注册filter chain
        for (UrlFilter urlFilter : urlFilters) {
            String url = urlFilter.getFilterUrl();
            if (urlFilter.getFilterAvailable() != null && urlFilter.getFilterAvailable() == 0) {
                continue;
            }
            //注册roles filter
            if (!StringUtils.isEmpty(urlFilter.getFilterRoles())) {
                filterChainManager.addToChain(url, "roles", urlFilter.getFilterRoles());
            }
            //注册perms filter
            if (!StringUtils.isEmpty(urlFilter.getFilterPerms())) {
                filterChainManager.addToChain(url, "perms", urlFilter.getFilterPerms());
            }
        }
    }
}
