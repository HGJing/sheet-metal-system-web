package web.sms.mvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.sms.core.exception.ServiceException;
import web.sms.mvc.constant.ProcessConstant;
import web.sms.mvc.dao.ProcessMapper;
import web.sms.mvc.entity.Process;
import web.sms.mvc.service.ProcessService;

import java.util.List;

/**
 * 时间： 2017/12/19
 *
 * @author Eoly
 */
@Service
public class ProcessServiceImpl implements ProcessService {

    private final ProcessMapper processMapper;

    @Autowired
    public ProcessServiceImpl(ProcessMapper processMapper) {
        this.processMapper = processMapper;
    }

    @Override
    public List<Process> findAllProcessExceptNormal() {
        List<Process> processes = processMapper.findAllProcessExceptNormal();
        if (processes == null || processes.size() < 1) {
            throw new ServiceException(ProcessConstant.NOT_HAVE_PROCESS_ERROR);
        }
        return processes;
    }

    @Override
    public List<Process> findAllProcess() {
        List<Process> processes = processMapper.findAllProcess();
        if (processes == null || processes.size() < 1) {
            throw new ServiceException(ProcessConstant.NOT_HAVE_PROCESS_ERROR);
        }
        return processes;
    }
}
