package web.sms.mvc.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.sms.core.exception.ServiceException;
import web.sms.core.pojo.ErrorInfo;
import web.sms.mvc.constant.RemindConstant;
import web.sms.mvc.constant.UserConstant;
import web.sms.mvc.dao.RemindMapper;
import web.sms.mvc.entity.Remind;
import web.sms.mvc.entity.User;
import web.sms.mvc.service.RemindService;
import web.sms.utils.ObjectUtils;

import java.util.List;

/**
 * 时间： 2017/12/28
 *
 * @author Eoly
 */
@Service
public class RemindServiceImpl implements RemindService {

    private final RemindMapper remindMapper;

    @Autowired
    public RemindServiceImpl(RemindMapper remindMapper) {
        this.remindMapper = remindMapper;
    }

    @Override
    public void doRemind(Remind remind, Integer type) {
        // 检查相应属性
        ObjectUtils.checkObjectProperties(remind, RemindConstant.CHECK_REMIND_FAILURE,
                "remindUserId", "remindContent");
        // 清空相应属性
        ObjectUtils.emptyUserProperties(remind, RemindConstant.CHECK_REMIND_FAILURE,
                "remindId", "remindType", "remindTime", "remindStatus");
        switch (type) {
            case Remind.NORMAL_TYPE:
                // 普通提示类型：只提示文字
                ObjectUtils.emptyUserProperties(remind, RemindConstant.CHECK_REMIND_FAILURE,
                        "remindTargetId", "remindTargetUrl");
                break;
            case Remind.URL_REMIND_TYPE:
                // Url提示类型，附加提示来源Url
                ObjectUtils.emptyUserProperties(remind, RemindConstant.CHECK_REMIND_FAILURE,
                        "remindTargetId");
                break;
            default:
                if (type >= Remind.ORDER_REMIND_TYPE) {
                    // 若提示类型为指定id
                    ObjectUtils.emptyUserProperties(remind, RemindConstant.CHECK_REMIND_FAILURE,
                            "remindTargetUrl");
                } else {
                    throw new ServiceException(RemindConstant.ERROR_REMIND_TYPE);
                }
                break;
        }
        remind.setRemindType(type);
        this.addRemind(remind, RemindConstant.FAIL_TO_BUILD_REMIND);
    }

    /**
     * 添加提醒
     *
     * @param remind 提醒实体
     */
    private void addRemind(Remind remind, ErrorInfo errorInfo) {
        int result = remindMapper.insertSelective(remind);
        if (result < 1) {
            throw new ServiceException(errorInfo);
        }
    }

    /**
     * 修改提醒
     *
     * @param remind 提醒实体
     */
    private void editRemind(Remind remind, ErrorInfo errorInfo) {
        int result = remindMapper.updateByPrimaryKeySelective(remind);
        if (result < 1) {
            throw new ServiceException(errorInfo);
        }
    }

    @Override
    public PageInfo<Remind> findUserReminds(Integer pageNum, Integer pageSize) {
        User currentUser = (User) SecurityUtils.getSubject()
                .getSession().getAttribute("userLogged");
        if (currentUser == null) {
            throw new ServiceException(UserConstant.NOT_LOGIN_ERROR);
        }
        PageHelper.startPage(pageNum, pageSize);
        List<Remind> reminds = remindMapper.selectRemindsByUserId(currentUser.getUserId());
        return new PageInfo<Remind>(reminds);
    }

    @Override
    public void doReadRemind(Integer remindId) {
        Remind remind = remindMapper.selectByPrimaryKey(remindId);
        ObjectUtils.checkObjectProperties(remind, RemindConstant.CHECK_REMIND_FAILURE,
                "remindUserId");
        // 获取当前用户
        User currentUser = (User) SecurityUtils.getSubject()
                .getSession().getAttribute("userLogged");
        if (currentUser == null) {
            throw new ServiceException(UserConstant.NOT_LOGIN_ERROR);
        }
        if (!remind.getRemindUserId().equals(currentUser.getUserId())) {
            throw new ServiceException(RemindConstant.REMIND_IS_NOT_BELONG_TO_YOU);
        }
        Remind editRemind = new Remind();
        editRemind.setRemindId(remindId);
        editRemind.setRemindStatus(Remind.HAVE_READ_STATUS);
        this.editRemind(editRemind, RemindConstant.READ_REMIND_FAILURE);
    }

    @Override
    public Integer getNotReadReminds() {
        // 获取当前用户
        User currentUser = (User) SecurityUtils.getSubject()
                .getSession().getAttribute("userLogged");
        if (currentUser == null || currentUser.getUserId() == null) {
            throw new ServiceException(UserConstant.NOT_LOGIN_ERROR);
        }
        // 获取未读提醒数量
        Integer count = remindMapper.selectCountNotReadRemindsByUserId(currentUser.getUserId());
        if (count == null) {
            return 0;
        }
        return count;
    }
}
