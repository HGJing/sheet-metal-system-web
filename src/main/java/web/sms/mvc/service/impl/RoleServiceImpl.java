package web.sms.mvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.sms.core.exception.ServiceException;
import web.sms.mvc.dao.RoleMapper;
import web.sms.mvc.entity.Role;
import web.sms.mvc.service.RoleService;

import java.util.List;

/**
 * 时间： 2017/12/17
 *
 * @author Eoly
 */
@Service
public class RoleServiceImpl implements RoleService {
    private final RoleMapper roleMapper;

    @Autowired
    public RoleServiceImpl(RoleMapper roleMapper) {
        this.roleMapper = roleMapper;
    }

    @Override
    public List<Role> findAllRoles() {
        List<Role> roles = roleMapper.selectAllWithoutAdmin();
        if (roles == null || roles.size() == 0) {
            throw new ServiceException("不能找到用户角色，需要重新初始化数据库", 500);
        }
        return roles;
    }
}
