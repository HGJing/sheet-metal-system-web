package web.sms.mvc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.sms.mvc.dao.UserMapper;
import web.sms.mvc.entity.User;
import web.sms.mvc.service.TestService;

/**
 * 时间： 2017/12/20
 *
 * @author Eoly
 */
@Service
public class TestServiceImpl implements TestService {

    private final UserMapper userMapper;

    @Autowired
    public TestServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public void testTransaction() {
        User user = new User();
        user.setUserLoginName("asdasd");
        user.setUserRole("admin");
        throw new RuntimeException("asddas");
    }
}
