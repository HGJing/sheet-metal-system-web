package web.sms.utils;

import com.alibaba.fastjson.JSONObject;
import web.sms.core.pojo.JsonInfo;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Created by Eoly on 2017/6/16.
 */
public class HttpUtils {
    private HttpUtils() {
    }

    public static void outPrint(ServletResponse response, JSONObject jsonObject) {
        PrintWriter out = null;
        try {
            response.setCharacterEncoding("UTF-8");//设置编码
            response.setContentType("application/json");//设置返回类型
            out = response.getWriter();
            out.println(jsonObject);//输出
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != out) {
                out.flush();
                out.close();
            }
        }
    }

    public static void outPrint(ServletResponse response, String msg) {
        PrintWriter out = null;
        try {
            response.setCharacterEncoding("UTF-8");//设置编码
            out = response.getWriter();
            out.println(msg);//输出
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != out) {
                out.flush();
                out.close();
            }
        }
    }

    public static boolean isAjaxRequestInternal(ServletRequest request) {
        String header = ((HttpServletRequest) request).getHeader("X-Requested-With");
        if ("XMLHttpRequest".equalsIgnoreCase(header)) {
            return Boolean.TRUE;
        }
        header = toHttp(request).getParameter("X_Requested_With");
        if ("XMLHttpRequest".equalsIgnoreCase(header)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public static void sendJsonResultByCheckAjax(
            JsonInfo jsonInfo,
            ServletRequest request,
            ServletResponse response

    ) {
        if (HttpUtils.isAjaxRequestInternal(request)) {
            outPrint(response, jsonInfo);
        }
    }

    public static HttpServletRequest toHttp(ServletRequest request) {
        return (HttpServletRequest) request;
    }

    public static HttpServletResponse toHttp(ServletResponse response) {
        return (HttpServletResponse) response;
    }

    public static void forwardAndSentAttributes(HttpServletRequest req, HttpServletResponse resp,
                                                 String path, Map<String, Object> attributes) {
        if (attributes != null) {
            for (String keys : attributes.keySet()) {
                req.getSession().setAttribute(keys, attributes.get(keys));
            }
        }
        try {
            req.getRequestDispatcher(path).forward(req,resp);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
