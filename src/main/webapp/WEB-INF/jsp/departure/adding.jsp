<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="add-departure" style="padding: 10px;">
    <div class="easyui-panel" style="width:480px;padding-left: 50px;padding-right: 50px;"
         data-options="title:'添加派车单信息'">
        <form id="add-departure-form" class="easyui-form" method="post"
              action="${pageContext.request.contextPath}/departure/add"
              data-options="novalidate:true,ajax:true">
            <table cellpadding="5" style="width: 380px;font-size: 14px;">
                <tr>
                    <td>派车司机*:</td>
                    <td><select id="sales-selector" class="easyui-combobox" style="width:253px;;"
                                name="departureDriverId"
                                data-options="required:true,missingMessage:'必须分配该派车单的司机'">
                        <option value=""></option>
                        <c:forEach items="${drivers}" var="drivers">
                            <option value="${drivers.userId}">${drivers.userRealName}[${drivers.userLoginName}]</option>
                        </c:forEach>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td>目的地*:</td>
                    <td>
                        <input class="easyui-textbox" name="departureAimPlace" data-options="multiline:true,
                            required:true,missingMessage:'目的地不能为空'" style="width:253px;;height:44px;">
                    </td>
                </tr>
                <tr>
                    <td>转场*:</td>
                    <td>
                        <input class="easyui-textbox" name="departureTransition" data-options="multiline:true,
                            required:true,missingMessage:'转场不能为空'" style="width:253px;;height:44px;">
                    </td>
                </tr>
                <tr>
                    <td>预计经费描述*:</td>
                    <td>
                        <input class="easyui-textbox" name="departureCostMoneyDesc" data-options="multiline:true,
                            required:true,missingMessage:'预计经费描述不能为空'" style="width:253px;;height:44px;">
                    </td>
                </tr>
                <tr>
                    <td>预计经费（￥）*:</td>
                    <td>
                        <input class="easyui-numberbox" style="width:253px;;" type="text"
                               name="departureCostMoney"
                               data-options="required:true,min:0,precision:2,missingMessage:'必须设置预计经费'">
                    </td>
                </tr>
                <tr>
                    <td>预计用时描述*:</td>
                    <td>
                        <input class="easyui-textbox" name="departureCostTimeDesc" data-options="multiline:true,
                            required:true,missingMessage:'预计用时描述不能为空'" style="width:253px;;height:44px;">
                    </td>
                </tr>
                <tr>
                    <td>预计用时（h）*:</td>
                    <td>
                        <input class="easyui-numberbox" style="width:253px;;" type="text"
                               name="departureCostTime"
                               data-options="required:true,min:0,precision:2,missingMessage:'必须设置预计用时'">
                    </td>
                </tr>
                <tr>
                    <td>首达客户:</td>
                    <td><select id="customer-selector" class="easyui-combobox" style="width:253px;;"
                                name="departureFirstCustomerInfo"
                                data-options="required:true,missingMessage:'首达客户不能为空'">
                        <option value=""></option>
                        <c:forEach items="${customers}" var="customer">
                            <option value="${customer.userId}">${customer.userRealName}[${customer.userLoginName}]</option>
                        </c:forEach>
                        <c:forEach items="${customersSpecial}" var="customer">
                            <option value="${customer.userId}">${customer.userRealName}[${customer.userLoginName}]</option>
                        </c:forEach>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="hidden" name="X_Requested_With" value="XMLHttpRequest">
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitDepartureForm()">添加</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearConfirmDepartureForm()">清空</a>
                    </td>
                </tr>
            </table>
        </form>
    </div>

</div>
<script>
    /**
     * 表单的提交
     */
    function submitDepartureForm() {
        $.messager.confirm("添加派车单", "确定添加派车单？", function (r) {
            if (r) {
                $('#add-departure-form').form('submit', {
                    onSubmit: function (param) {
                        var isOk = $(this).form('enableValidation').form('validate');
                        if (isOk) {
                            $.messager.progress({
                                title: '请稍候',
                                msg: '添加派车单信息中...'
                            });
                        }
                        return isOk;
                    },
                    success: function (data) {
                        console.log(data);
                        data = $.parseJSON(data);
                        $.messager.progress('close');
                        if (!data.error) {
                            $.messager.alert("添加派车单", data.msg, 'info');
                            clearDepartureForm();
                        } else {
                            $.messager.alert("添加派车单", data.msg, "error");
                        }
                    }, onLoadError: function () {
                        $.messager.progress('close');
                    }
                });
            }
        });
    }
    function clearDepartureForm() {
        $('#add-departure-form').form('clear');
    }
    function clearConfirmDepartureForm() {
        $.messager.confirm("清空", "确定清空填写的派车单？", function (r) {
            if (r) {
                clearDepartureForm();
            }
        });
    }
</script>