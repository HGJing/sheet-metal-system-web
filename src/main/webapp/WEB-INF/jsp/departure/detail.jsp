<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div style="padding: 10px;">
    <table cellspacing="0" cellpadding="12" border="1">
        <thead align="center">
        <tr>
            <th colspan="4" style="font-size: 22px;">派车单</th>
        </tr>
        </thead>
        <tbody align="center">
        <tr>
            <td>派遣司机</td>
            <td width="300">${departure.departureDriver.userRealName}</td>
            <td>发车时间</td>
            <td width="300">
                <fmt:formatDate value="${departure.departureStartTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
            </td>
        </tr>
        <tr>
            <td>目的地</td>
            <td width="300">${departure.departureAimPlace}</td>
            <td>首达客户</td>
            <td width="300">${departure.departureFirstCustomerName}</td>
        </tr>
        <tr>
            <td>转场</td>
            <td width="300" colspan="3">${departure.departureTransition}</td>
        </tr>
        <tr>
            <td>合计派车费</td>
            <td width="300">
                ${departure.departureCostMoneyDesc}
                <br>
                合计（￥）：${departure.departureCostMoney}
            </td>
            <td>预计耗时（h）</td>
            <td width="300">
                ${departure.departureCostTimeDesc}
                <br>
                总共（h）：${departure.departureCostTime}
            </td>
        </tr>
        <tr>
            <td>预计返回时间</td>
            <td width="300">
                <fmt:formatDate value="${preEndTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>

            </td>
            <td>实际返回时间</td>
            <td width="300">
                <fmt:formatDate value="${departure.departureBackTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
            </td>
        </tr>
        <tr>
            <td>派车人</td>
            <td width="300">${departure.departureInitiator.userRealName}</td>
            <td>回厂收货</td>
            <td width="300">${departure.departureReceiverName}</td>
        </tr>
        <tr>
            <td>实际耗时（h）</td>
            <td width="300">
                <fmt:formatNumber value="${departure.departureRealCostTime}" pattern="0.00" maxFractionDigits="2"/>
            </td>
            <td>节约用时（h）</td>
            <td width="300">
                <fmt:formatNumber value="${saveTime}" pattern="0.00" maxFractionDigits="2"/>
            </td>
        </tr>
        <tr>
            <td>超时原因</td>
            <td width="300" colspan="3">
                ${departure.departureOvertimeResult}
            </td>
        </tr>
        <tr>
            <td colspan="2">超时批准人</td>
            <td colspan="2">总经办或派车人签字</td>
        </tr>
        </tbody>
    </table>
</div>