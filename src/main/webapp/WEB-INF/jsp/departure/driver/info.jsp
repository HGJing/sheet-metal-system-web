<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div style="padding: 10px;">
    <c:if test="${not empty startTime}">
        <p>查询区间：
            <c:if test="${not empty endTime}">
                <fmt:formatDate value="${startTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
                到
                <fmt:formatDate value="${endTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
            </c:if>
            <c:if test="${empty endTime}">
                <fmt:formatDate value="${startTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/> 之后
            </c:if>
        </p>
    </c:if>
    <c:if test="${empty startTime}">
        <c:if test="${not empty endTime}">
            <p>查询区间：
                <fmt:formatDate value="${endTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/> 之前
            </p>
        </c:if>
    </c:if>
    <c:if test="${empty startTime}">
        <c:if test="${empty endTime}">
            <p>查询区间：无时间限制</p>
        </c:if>
    </c:if>
    <table cellspacing="0" cellpadding="12" border="1">
        <thead>
        <tr>
            <th colspan="4" style="font-size: 22px;">
                司机信息统计表（只统计该司机已完成的派车单）
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>司机姓名</td>
            <td width="100">${info.driverInfo.userRealName}</td>
            <td>司机登录名</td>
            <td width="100">${info.driverInfo.userLoginName}</td>
        </tr>
        <tr>
            <td>司机邮箱</td>
            <td>${info.driverInfo.userEmail}</td>
            <td>司机电话</td>
            <td>${info.driverInfo.userPhoneNumber}</td>
        </tr>
        <tr>
            <td width="100">该段时间内派车经费消耗总和(￥)</td>
            <td>
                <fmt:formatNumber value="${info.totalMoneyCost}" pattern="0.00" maxFractionDigits="2"/>
            </td>
            <td width="100">该段时间内派车时间消耗总和(h)</td>
            <td>
                <fmt:formatNumber value="${info.totalTimeCost}" pattern="0.00" maxFractionDigits="2"/>
            </td>
        </tr>
        <tr>
            <td width="100">该段时间内派车总节约用时(h)</td>
            <td>
                <fmt:formatNumber value="${info.totalSavingTime}" pattern="0.00" maxFractionDigits="2"/>
            </td>
            <td width="100">司机总派车时间（h）</td>
            <td>
                <fmt:formatNumber value="${timeCost}" pattern="0.00" maxFractionDigits="2"/>
            </td>
        </tr>
        </tbody>
    </table>
    <h2>该段时间内该司机的派车单列表</h2>
    <div style="margin:20px 0;">
        <table class="easyui-datagrid" id="departure-list-in-grid"
               data-options="singleSelect:true,collapsible:true">
            <thead>
            <tr>
                <th field="id">id</th>
                <th field="departureInitiator">车单发起者</th>
                <th field="departureAimPlace">派车目的地</th>
                <th field="departureTransition">派车转场</th>
                <th field="departureCostTime">预计派车耗时（h）</th>
                <th field="departureRealCostTime">实际派车耗时（h）</th>
                <th field="saveTime">节约用时（h）</th>
                <th field="departureCostMoney">预计派车经费（￥）</th>
                <th field="isOverTime">是否超时</th>
                <th field="detail">查看详情</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${departuresInfo.list}" var="departure">
                <tr>
                    <td>${departure.departureId}</td>
                    <td>${departure.departureInitiator.userRealName}[${departure.departureInitiator.userLoginName}]</td>
                    <td>${departure.departureAimPlace}</td>
                    <td>${departure.departureTransition}</td>
                    <td>
                        <fmt:formatNumber value="${departure.departureCostTime}" pattern="0.00" maxFractionDigits="2"/>
                    </td>
                    <td>
                        <fmt:formatNumber value="${departure.departureRealCostTime}" pattern="0.00"
                                          maxFractionDigits="2"/>
                    </td>
                    <td>
                        <fmt:formatNumber value="${departure.departureCostTime - departure.departureRealCostTime}"
                                          pattern="0.00" maxFractionDigits="2"/>
                    </td>
                    <td>
                        <fmt:formatNumber value="${departure.departureCostMoney}" pattern="0.00" maxFractionDigits="2"/>
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${departure.departureCostTime - departure.departureRealCostTime >=0}">
                                <c:out value="否"/>
                            </c:when>
                            <c:otherwise>
                                <c:out value="是"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <a href="javascript:;" style="color: #005;"
                           onclick="turnTap('departureDetail', ${departure.departureId})">详情</a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>