<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div style="padding: 10px;">
    <div class="easyui-panel" style="width:400px;padding-left: 50px;padding-right: 50px;"
         data-options="title:'查询司机详情'">
        <form id="search-driver-form" class="easyui-form"
              data-options="novalidate:true,ajax:true">
            <table cellpadding="5" style="width: 300px;font-size: 14px;">
                <tr>
                    <td>查询司机*:</td>
                    <td>
                        <select id="searchDriverId" class="easyui-combobox" style="width:173px;"
                                name="driverId"
                                data-options="required:true,missingMessage:'查询的司机不能为空'">
                            <option value=""></option>
                            <c:forEach items="${drivers}" var="driver">
                                <option value="${driver.userId}">${driver.userRealName}[${driver.userLoginName}]</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>开始时间:</td>
                    <td>
                        <input id="searchStartTime" class="easyui-datetimebox" type="text"
                               style="width:173px;"
                               name="searchStartTime">
                    </td>
                </tr>
                <tr>
                    <td>结束时间:</td>
                    <td>
                        <input id="searchEndTime" class="easyui-datetimebox" type="text"
                               style="width:173px;"
                               name="searchEndTime">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="hidden" name="X_Requested_With" value="XMLHttpRequest">
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitSearch()">查询</a>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script>
    function submitSearch() {
        $('#search-driver-form').form('submit', {
            onSubmit: function (param) {
                var isOk = $(this).form('enableValidation').form('validate');
                if (isOk) {
                    var driver = $('#searchDriverId');
                    var st = $('#searchStartTime');
                    var et = $('#searchEndTime');
                    var id = driver.textbox('getValue');
                    var startTime = st.textbox('getValue');
                    var endTime = et.textbox('getValue');
                    var node = {
                        text: '司机[' + driver.textbox('getText') + ']',
                        attributes: {
                            url: '/driver/' + id + '/departure-info.model?startTime=' + startTime + '&endTime=' + endTime
                        }
                    };
                    tabs.turnTab(node);
                }
                return false;
            }
        });
    }
</script>