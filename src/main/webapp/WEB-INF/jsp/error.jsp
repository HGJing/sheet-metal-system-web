<%--
  Created by IntelliJ IDEA.
  User: Eoly
  Date: 2017/7/12
  Time: 13:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>您的访问出错了</title>
</head>
<body>
<description style="display: none;">该页面用于先显示异常信息</description>
<div style="padding: 10px;">
    <p>${e.get("msg")}</p>
</div>
</body>
</html>
