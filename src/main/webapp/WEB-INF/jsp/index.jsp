<%--
  Created by IntelliJ IDEA.
  User: Eoly
  Date: 2017/12/11
  Time: 16:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="shrio" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>后台首页</title>
    <script>
        // 规定项目路径用于后续url链接
        window.projectPath = '${pageContext.request.contextPath}';
        window._directory_tree_path = window.projectPath + '${treePath}'
        window._user_role = '${user.userRole}'
    </script>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/lib/jquery-easyui-1.5.3/themes/metro/easyui.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/lib/jquery-easyui-1.5.3/themes/icon.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/lib/font-awesome-4.7.0/css/font-awesome.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/resources/index/css/index.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/lib/jquery-easyui-1.5.3/jquery.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/lib/jquery-easyui-1.5.3/jquery.easyui.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/lib/jquery-easyui-1.5.3/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/index/js/index.js"></script>
    <script>
        /**
         * 用户名是否存在验证
         */
        $.extend($.fn.validatebox.defaults.rules, {
            loginNameExist: {
                validator: function (value, param) {
                    var isOk = false;
                    // 查询用户名是否存在
                    $.ajax({
                        url: window.projectPath + '/user/has-login-name/' + value,
                        method: 'get',
                        async: false,
                        success: function (res) {
                            // console.log(res);
                            res = typeof res == 'string' ? $.parseJSON(res) : res;
                            if (!res.error) {
                                isOk = !res.hasLoginName;
                            } else {
                                console.warn(res.msg);
                            }
                        }
                    });
                    return isOk;
                },
                message: '该用户名已存在!'
            }
        });
        /**
         * 验证字符一致
         */
        $.extend($.fn.validatebox.defaults.rules, {
            equals: {
                validator: function (value, param) {
                    return value == $(param[0]).val();
                },
                message: '两次输入的密码不一致.'
            }
        });
        /**
         * 检查输入只能是大小写字母、数字或'_'
         */
        $.extend($.fn.validatebox.defaults.rules, {
            loginName: {
                validator: function (value) {
                    return /^\w+$/.test(value);
                },
                message: '输入只能是大小写字母、数字或\'_\'.'
            }
        });
        /**
         * 正则表达式检测
         */
        $.extend($.fn.validatebox.defaults.rules, {
            reg: {
                validator: function (value, params) {
                    return new RegExp(params[0]).test(value);
                },
                message: '{1}'
            }
        });
        /**
         * 检查输入长度
         */
        $.extend($.fn.validatebox.defaults.rules, {
            lengthBetween: {
                validator: function (value, params) {
                    var len = $.trim(value).length;
                    return len >= params[0] && len <= params[1];
                },
                message: '{2}字符长度需要在{0}-{1}之间'
            }
        });
    </script>
</head>
<body class="easyui-layout" style="width: 100%">
<div data-options="region:'north',border:false" style="height:60px;">
    <p style="line-height: 60px;float: left;padding-left: 50px;margin: 0;">
        ${user.userRealName}[${roleName}]，你好。&nbsp;
        <a href="javascript:;"
           onclick="turnTap('remind')">未读通知(<span id="notReadCount">${notReadCount}</span>)</a>
    </p>
    <p style="line-height: 60px;float: right;padding-right: 50px;margin: 0;">
        &nbsp;<a id="logout" href="javascript:;" class="easyui-linkbutton">注销</a>
    </p>
</div>
<div data-options="region:'west',title:'操作目录',split:true" style="width: 200px;">
    <div class="easyui-panel" data-options="border:false">
        <ul class="easyui-tree" id="option-tree" data-options="animate:true"></ul>
    </div>
</div>
<div data-options="region:'center'">
    <div id="tab" class="easyui-tabs" data-options="border:false" style="height: 100%;width: 100%">
    </div>
</div>
</body>
</html>
