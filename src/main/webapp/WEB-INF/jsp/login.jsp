<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script>
        // 规定项目路径用于后续url链接
        window.projectPath = '${pageContext.request.contextPath}';
    </script>
    <title>登录到钣金加工网站系统</title>
    <link href="${pageContext.request.contextPath}/resources/login/res/style_log.css" rel="stylesheet" type="text/css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/lib/jquery-easyui-1.5.3/jquery.min.js"></script>
</head>
<body class="login">
<div class="login_m">
    <div class="login-outer">
        <div class="login_logo" style="height: 100px;line-height: 100px;">
            <span style="display: inline-block;font-size: 24px;font-weight: bolder">登录到钣金加工网站系统</span>
            <div style="float: right;height: 100px;">
                <img src="${pageContext.request.contextPath}/resources/login/res/logo.png" height="80">
            </div>
        </div>
        <form name="login_form" onsubmit="onSubmit();return false;"
              action="${pageContext.request.contextPath}/test/login">
            <div class="login_boder">
                <div class="login_padding" id="login_model">
                    <h2>用户名：</h2>
                    <label>
                        <input type="text" id="username" name="username" class="txt_input txt_input2"
                               placeholder="请输入用户名" value="">
                    </label>
                    <h2>密码：</h2>
                    <label>
                        <input type="password" name="password" id="userpwd" placeholder="请输入密码"
                               class="txt_input"
                               value="">
                    </label>
                    <p class="forgot"><a id="iforget" href="javascript:void(0);">忘记密码?</a></p>
                    <div class="rem_sub">
                        <div class="rem_sub_l">
                            <input type="checkbox" name="rememberMe" value="true" id="save_me">
                            <label for="save_me">记住我</label>
                        </div>
                        <label>
                            <input type="submit" class="sub_button" id="loginButton" value="登录"
                                   style="opacity: 0.7;">
                        </label>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
<script>
    function onSubmit() {
        var formData = $(document.forms.namedItem("login_form"));
        var loginButton = $('#loginButton');
        loginButton.val('登录中...');
        $.ajax({
            url: '${pageContext.request.contextPath}/user/login',
            method: 'post',
            data: formData.serialize(),
            success: function (res) {
                if (typeof res == 'string') {
                    // 转换json对象
                    res = $.parseJSON(res);
                }
                // console.log(res);
                if (!res.error) {
                    // 页面跳转
                    loginButton.val('跳转中..');
                    location.href = projectPath + res.url;
                } else {
                    alert(res.msg);
                    loginButton.val('登录');
                }

            },
            error: function () {
                alert('网络或设置错误');
                loginButton.val('登录');
            }
        });
        return false;
    }
</script>
</html>