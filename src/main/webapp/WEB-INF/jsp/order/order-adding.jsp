<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="add-user" style="padding: 10px;">
    <div class="easyui-panel" style="width:400px;padding-left: 50px;padding-right: 50px;"
         data-options="title:'添加工单信息'">
        <form id="add-order-form" class="easyui-form" method="post"
              action="${pageContext.request.contextPath}/order/add"
              data-options="novalidate:true,ajax:true">
            <table cellpadding="5" style="width: 300px;font-size: 14px;">
                <tr>
                    <td>客户*:</td>
                    <td><select id="customer-selector" class="easyui-combobox" style="width:173px;"
                                name="orderCustomerId"
                                data-options="required:true,missingMessage:'客户不能为空'">
                        <option value=""></option>
                        <c:forEach items="${customers}" var="customer">
                            <option value="${customer.userId}">${customer.userRealName}[${customer.userLoginName}]</option>
                        </c:forEach>
                        <c:forEach items="${customersSpecial}" var="customer">
                            <option value="${customer.userId}">${customer.userRealName}[${customer.userLoginName}]</option>
                        </c:forEach>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td>销售管理*:</td>
                    <td><select id="sales-selector" class="easyui-combobox" style="width:173px;"
                                name="orderChargeManagerId"
                                data-options="required:true,missingMessage:'必须设置该工单的销售管理'">
                        <option value=""></option>
                        <c:forEach items="${salesManagers}" var="salesManager">
                            <option value="${salesManager.userId}">${salesManager.userRealName}[${salesManager.userLoginName}]</option>
                        </c:forEach>
                    </select>
                    </td>
                </tr>
                <%--<tr>--%>
                <%--<td>生产管理*[不需要设定]:</td>--%>
                <%--<td><select id="production-selector" class="easyui-combobox" style="width:173px;"--%>
                <%--name="orderProductionManagerId"--%>
                <%--data-options="required:true,missingMessage:'必须设置该工单的生产管理'">--%>
                <%--<option value=""></option>--%>
                <%--<c:forEach items="${productionManagers}" var="productionManager">--%>
                <%--<option value="${productionManager.userId}">${productionManager.userRealName}[${productionManager.userLoginName}]</option>--%>
                <%--</c:forEach>--%>
                <%--</select>--%>
                <%--</td>--%>
                <%--</tr>--%>
                <tr>
                    <td>加工流程*:</td>
                    <td id="processInput">
                        <c:forEach items="${processes}" var="process">
                            <label style="display: block;" title="${process.processDesc}">
                                <input type="checkbox" name="processId" value="${process.processId}">
                                    ${process.processName}
                            </label>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <td>镀钛颜色(若有):</td>
                    <td><input class="easyui-textbox" style="width:173px;" type="text"
                               name="orderProductionColor">
                    </td>
                </tr>
                <tr>
                    <td>加工优先级*:</td>
                    <td><select class="easyui-combobox" style="width:173px;"
                                name="orderPriority"
                                data-options="panelHeight:75,required:true,missingMessage:'加工优先级不能为空'">
                        <option value="0">正常</option>
                        <option value="1">急</option>
                        <option value="2">加急</option>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td>工单价格*:</td>
                    <td>
                        <input class="easyui-numberbox" style="width:173px;" type="text"
                               name="orderPrice"
                               data-options="required:true,min:0,precision:2,missingMessage:'必须设置工单价格'">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="hidden" name="X_Requested_With" value="XMLHttpRequest">
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitOrderForm()">添加</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearOrderForm()">清空</a>
                    </td>
                </tr>
            </table>
        </form>
    </div>

</div>
<script>
    //    /**
    //     * 在指定节点下添加复选框
    //     */
    //    function addCheckBox(node, option) {
    //        var checkBox = '<input type="checkbox" title="' + option.desc + '" name="' + option.name
    //            + '" value="' + option.value + '"><span>' + option.show + '</span><br>';
    //        node.append(checkBox);
    //    }

    //    /**
    //     * 获取加工流程并显示在表单中
    //     */
    //    $.ajax({
    //        url: window.projectPath + '/process/list',
    //        method: 'get',
    //        success: function (res) {
    //            res = typeof res == 'string' ? $.parseJSON(res) : res;
    //            if (!res.error) {
    //                var list = res.list;
    //                var node = $("#processInput");
    //                for (var i = 0; i < list.length; i++) {
    //                    addCheckBox(node, {
    //                        show: list[i].processName,
    //                        desc: list[i].processDesc,
    //                        name: 'processId',
    //                        value: list[i].processId
    //                    });
    //                }
    //            } else {
    //                $.messager.alert("加载错误", res.msg, "error");
    //            }
    //        }
    //    });

    /**
     * 表单的提交
     */
    function submitOrderForm() {
        $('#add-order-form').form('submit', {
            onSubmit: function (param) {
                var isOk = $(this).form('enableValidation').form('validate');
                if (isOk) {
                    $.messager.progress({
                        title: '请稍候',
                        msg: '添加工单信息中...'
                    });
                }
                return isOk;
            },
            success: function (data) {
                data = $.parseJSON(data);
                $.messager.progress('close');
                if (!data.error) {
                    $.messager.alert("添加工单", data.msg, 'info');
                    clearOrderForm();
                } else {
                    $.messager.alert("添加工单", data.msg, "error");
                }
            }, onLoadError: function () {
                $.messager.progress('close');
            }
        });
    }
    function clearOrderForm() {
        $('#add-order-form').form('clear');
    }
</script>