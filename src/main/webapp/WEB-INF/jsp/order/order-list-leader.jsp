<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<div style="padding: 10px">
    <h2>任务流程列表</h2>
    <div style="margin:20px 0;"></div>
    <table class="easyui-propertygrid" id="order-list-leader-grid" title="所负责的任务流程信息[任务按小组分组][每组的任务需要按优先级由上往下完成]"
           data-options="singleSelect:true,collapsible:true" style="max-height: 400px;">
    </table>
    <script>
        var orderListLeaderGrid = $('#order-list-leader-grid');
        window.taskFinish = function (index) {
            if (!orderListLeaderGrid.innerRows) {
                return;
            }
            var rows = orderListLeaderGrid.innerRows.rows;
            var id = rows[index].taskAssignmentId;
            var orderNumber = rows[index].taskNo;
            var name = rows[index].taskProcess.processName;
            $.messager.confirm("完成工单任务流程[id=" + id + "]", "确认：工单号[" + orderNumber + "]所对应的任务流程["
                + name + "]将被完成？", function (r) {
                if (r) {
                    $.messager.progress({
                        title: '请稍候',
                        msg: '任务完成中...'
                    });
                    $.ajax({
                        url: '${pageContext.request.contextPath}/order/task/' + id
                        + '/finish?groupId=' + rows[index].taskWorkerGroupId,
                        method: 'get',
                        success: function (res) {
                            $.messager.progress('close');
                            res = typeof res == 'string' ? $.parseJSON(res) : res;
                            if (!res.error) {
                                $.messager.alert("完成任务", res.msg, 'info', function () {
                                    tabs.refreshCurrent();
                                });
                            } else {
                                $.messager.alert("完成任务", res.msg, 'error');
                            }
                        },
                        error: function () {
                            $.messager.progress('close');
                        }
                    });
                }
            });
        };
        /**
         * 用于判断任务是否可以完成
         */
        orderListLeaderGrid.canBeFinish = function (index) {
            if (!orderListLeaderGrid._groups) {
                return false;
            }
            var groups = orderListLeaderGrid._groups;
            var startIndexs = [];
            for (var i = 0; i < groups.length; i++) {
                startIndexs.push(groups[i].startIndex);
            }
            return Math.between(index, startIndexs, function (i, startIndex) {
                var rows = groups[i].rows;
                var realIndex = index - startIndex;
                // preDemandNum 代表该任务的前置任务未完成的个数
                // 工人组长能完成任务的条件是必须在优先级较高的任务处于进行中且未完成时，不能完成优先级较低的任务
                // 优先级越高在列表中显示越靠前
                var row = rows[realIndex];
                if (!row.preDemandNum) {
                    if (realIndex == 0) {
                        row.canFinish = true;
                        return true;
                    }
                    if (rows[realIndex - 1].preDemandNum && !rows[realIndex - 1]) {
                        row.canFinish = true;
                        return true;
                    }
                }
                row.canFinish = false;
                return false;
            });


        };
        /**
         * 初始化任务流程表格
         */
        orderListLeaderGrid.propertygrid({
            url: window.projectPath + "/order/task/list/leader",
            method: 'get',
            showGroup: true,
            groupField: 'taskWorkerGroupId',
            loadMsg: '正在加载中...',
            emptyMsg: '没有您负责的任务处于未完成状态。',
            onLoadSuccess: function () {
                orderListLeaderGrid._groups = orderListLeaderGrid.propertygrid('groups');
            },
            loadFilter: function (data) {
                // console.log(data);
                var result = {
                    rows: [],
                    total: 0
                };
                if (!data.error) {
                    result.rows = data.list;
                } else {
                    if (data.error == 404) {
                        $.messager.alert('提示', '暂时没有您扶着的相关的任务', 'info');
                    } else {
                        $.messager.alert('加载错误', '加载任务列表出错!' + data.msg, 'error');
                    }
                }
                orderListLeaderGrid.innerRows = result;
                return result;
            },
            columns: [[
                {field: 'taskAssignmentId', title: '任务id'},
                {field: 'taskNo', title: '工单号'},
                {
                    field: 'taskProcess', title: '任务流程',
                    formatter: function (value, row, index) {
                        return value.processName;
                    }
                },
                {
                    field: 'taskStatus', title: '任务状态',
                    formatter: function (value, row, index) {
                        switch (value) {
                            case -1:
                                var message = !row.preDemandNum ? '进行中' : '前置任务流程尚未完成';
                                if (!row.preDemandNum) {
                                    if (!orderListLeaderGrid.canBeFinish(index)) {
                                        message = '高优先级的任务尚未完成'
                                    }
                                }
                                return "未完成：" + message;
                            case 0:
                                return "已完成";
                            default:
                                return "未知"
                        }
                    }
                },
                {
                    field: 'preTaskInfo', title: '前置任务',
                    formatter: function (value, row, index) {
                        var taskId = row.taskAssignmentId;
                        var tasks = [];
                        $.ajax({
                            url: window.projectPath + '/order/task/pre/' + taskId + '/list',
                            method: 'get',
                            async: false,
                            success: function (res) {
                                res = typeof res == 'string' ? $.parseJSON(res) : res;
                                if (!res.error) {
                                    tasks = res.list;
                                }
                            }
                        });
                        if (tasks.length == 0) {
                            return '没有前置任务';
                        }
                        var aEles = '';
                        for (var j = 0; j < tasks.length; j++) {
                            aEles = aEles + ('<a href="javascript:;" style="color:#005;cursor:pointer;" onclick="turnTap(\'taskInfo\',' + tasks[j].taskAssignmentId + ')">' + tasks[j].taskProcess.processName + '</a>&nbsp;');
                        }
                        return aEles;
                    }
                },
                {
                    field: 'customer', title: '客户',
                    formatter: function (value, row, index) {
                        return value.userRealName;
                    }
                },
                {
                    field: 'taskGroupInForMember', title: '小组信息及成员',
                    formatter: function (value, row, index) {
                        return '<a href="javascript:;" style="color:#005;cursor:pointer;" onclick="turnTap(\'groupInfo\',' + row.taskWorkerGroupId + ')">点击查看</a>';
                    }
                },
                {
                    field: 'option', title: '操作',
                    formatter: function (value, row, index) {
                        if (orderListLeaderGrid.canBeFinish(index)) {
                            return '\
                                <shiro:hasRole name="worker">\
                                <a href="javascript:;" style="color: #005;cursor: pointer;" onclick="taskFinish(' + index + ')"title="完成任务">完成</a>\
                               </shiro:hasRole>';
                        }
                        return '\
                                <shiro:hasRole name="worker">\
                                <a href="javascript:;" class="disabled" style="color: #005;cursor: pointer;">完成</a>\
                               </shiro:hasRole>';
                    }
                }
            ]]
        });
    </script>
</div>