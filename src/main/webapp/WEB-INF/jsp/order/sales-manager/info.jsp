<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div style="padding: 10px;">
    <c:if test="${not empty startTime}">
        <p>查询区间：
            <c:if test="${not empty endTime}">
                <fmt:formatDate value="${startTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
                到
                <fmt:formatDate value="${endTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
            </c:if>
            <c:if test="${empty endTime}">
                <fmt:formatDate value="${startTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/> 之后
            </c:if>
        </p>
    </c:if>
    <c:if test="${empty startTime}">
        <c:if test="${not empty endTime}">
            <p>查询区间：
                <fmt:formatDate value="${endTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/> 之前
            </p>
        </c:if>
    </c:if>
    <c:if test="${empty startTime}">
        <c:if test="${empty endTime}">
            <p>查询区间：无时间限制</p>
        </c:if>
    </c:if>
    <h2>该段时间内该销售管理已完成的工单列表</h2>
    <div style="margin:20px 0;">
        <table class="easyui-datagrid" id="departure-list-in-grid"
               data-options="singleSelect:true,collapsible:true,emptyMsg:'该段时间没有工单完成'">
            <thead>
            <tr>
                <th field="orderNumber">工单号</th>
                <th field="orderAddTime">生成时间</th>
                <th field="orderFinishTime">完成时间</th>
                <th field="orderPaymentStatus">支付状态</th>
                <th field="orderTasksProcess">工单加工流程</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${orders}" var="order">
                <tr>
                    <td>${order.orderNumber}</td>
                    <td>
                        <fmt:formatDate value="${order.orderAddTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
                    </td>
                    <td>
                        <fmt:formatDate value="${order.orderFinishTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
                    </td>
                    <td>
                        <c:set var="sum" value="${sum+order.orderPrice}"/>
                        ￥${order.orderPrice}：
                        <c:choose>
                            <c:when test="${order.orderPaymentStatus==-1}">
                                未收
                            </c:when>
                            <c:otherwise>
                                清欠
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <c:forEach items="${order.orderTasks}" var="task">
                            ${task.taskProcess.processName}&nbsp;
                        </c:forEach>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <h4>该段时间内该销售管理的总销售额为:￥${sum}</h4>
    </div>
</div>