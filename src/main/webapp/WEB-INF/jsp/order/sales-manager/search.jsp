<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div style="padding: 10px;">
    <div class="easyui-panel" style="width:400px;padding-left: 50px;padding-right: 50px;"
         data-options="title:'查询销售管理工单销售总和'">
        <form id="searchSaW-total-form" class="easyui-form"
              data-options="novalidate:true,ajax:true">
            <table cellpadding="5" style="width: 300px;font-size: 14px;">
                <tr>
                    <td>销售管理*:</td>
                    <td><select id="searchSaSManager" class="easyui-combobox" style="width:173px;"
                                name="orderChargeManagerId"
                                data-options="required:true,missingMessage:'必须设置销售管理'">
                        <option value=""></option>
                        <c:forEach items="${salesManagers}" var="salesManager">
                            <option value="${salesManager.userId}">${salesManager.userRealName}[${salesManager.userLoginName}]</option>
                        </c:forEach>
                    </select>
                </tr>
                <tr>
                    <td>开始时间:</td>
                    <td>
                        <input id="searchSaWStartTime" class="easyui-datetimebox" type="text"
                               style="width:173px;"
                               name="searchSaWStartTime">
                    </td>
                </tr>
                <tr>
                    <td>结束时间:</td>
                    <td>
                        <input id="searchSaWEndTime" class="easyui-datetimebox" type="text"
                               style="width:173px;"
                               name="searchSaWEndTime">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="hidden" name="X_Requested_With" value="XMLHttpRequest">
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitSSearch()">查询</a>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script>
    function submitSSearch() {
        $('#searchSaW-total-form').form('submit', {
            onSubmit: function (param) {
                var isOk = $(this).form('enableValidation').form('validate');
                if (isOk) {
                    var smanage = $('#searchSaSManager');
                    var id = smanage.textbox("getValue");
                    var st = $('#searchSaWStartTime');
                    var et = $('#searchSaWEndTime');
                    var startTime = st.textbox('getValue');
                    var endTime = et.textbox('getValue');
                    var node = {
                        text: '销售管理[' + smanage.textbox('getText') + ']',
                        attributes: {
                            url: '/order/sales-manager/' + id + '/order-info.model?startTime=' + startTime + '&endTime=' + endTime
                        }
                    };
                    tabs.turnTab(node);
                }
                return false;
            }
        });
    }
</script>