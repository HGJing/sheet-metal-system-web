<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div style="padding: 10px;">
    <div class="easyui-panel" style="width:400px;padding-left: 50px;padding-right: 50px;"
         data-options="title:'查询工单销售总和'">
        <form id="searchW-total-form" class="easyui-form"
              data-options="novalidate:true,ajax:true">
            <table cellpadding="5" style="width: 300px;font-size: 14px;">
                <tr>
                    <td>开始时间:</td>
                    <td>
                        <input id="searchWStartTime" class="easyui-datetimebox" type="text"
                               style="width:173px;"
                               name="searchWStartTime">
                    </td>
                </tr>
                <tr>
                    <td>结束时间:</td>
                    <td>
                        <input id="searchWEndTime" class="easyui-datetimebox" type="text"
                               style="width:173px;"
                               name="searchWEndTime">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="hidden" name="X_Requested_With" value="XMLHttpRequest">
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitSSearch()">查询</a>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script>
    function submitSSearch() {
        $('#searchW-total-form').form('submit', {
            onSubmit: function (param) {
                var st = $('#searchWStartTime');
                var et = $('#searchWEndTime');
                var startTime = st.textbox('getValue');
                var endTime = et.textbox('getValue');
                $.ajax({
                    url: window.projectPath + '/order/total-price?startTime=' + startTime + '&endTime=' + endTime,
                    method: 'get',
                    success: function (res) {
                        res = typeof res == 'string' ? $.parseJSON(res) : res;
                        if (!res.error) {
                            $.messager.alert("获取工单销售总和", '[' + startTime + '到' + endTime + ']该时间段的工单总和为：￥' + res.price, 'info');
                        } else {
                            $.messager.alert("获取[" + startTime + "-" + endTime + "]的工单销售总和", res.msg, 'error');
                        }
                    }
                });
                return false;
            }
        });
    }
</script>