<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div style="padding: 10px;">
    <c:if test="${not empty startTime}">
        <p>查询区间：
            <c:if test="${not empty endTime}">
                <fmt:formatDate value="${startTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
                到
                <fmt:formatDate value="${endTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
            </c:if>
            <c:if test="${empty endTime}">
                <fmt:formatDate value="${startTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/> 之后
            </c:if>
        </p>
    </c:if>
    <c:if test="${empty startTime}">
        <c:if test="${not empty endTime}">
            <p>查询区间：
                <fmt:formatDate value="${endTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/> 之前
            </p>
        </c:if>
    </c:if>
    <c:if test="${empty startTime}">
        <c:if test="${empty endTime}">
            <p>查询区间：无时间限制</p>
        </c:if>
    </c:if>
    <h2>该段时间内该小组已完成的工单列表</h2>
    <table class="easyui-datagrid" id="departure-list-in-grid"
           data-options="singleSelect:true,collapsible:true,emptyMsg:'该段时间没有工单完成'">
        <thead>
        <tr>
            <th field="orderNumber">工单号</th>
            <th field="orderAddTime">生成时间</th>
            <th field="orderFinishTime">完成时间</th>
            <th field="orderPaymentStatus">支付状态</th>
            <th field="orderTasksProcess">工单加工流程</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${captain.groupOrderInfo.orders}" var="order">
            <tr>
                <td>${order.orderNumber}</td>
                <td>
                    <fmt:formatDate value="${order.orderAddTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
                </td>
                <td>
                    <fmt:formatDate value="${order.orderFinishTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
                </td>
                <td>
                    <c:set var="sum" value="${sum+order.orderPrice}"/>
                    ￥${order.orderPrice}：
                    <c:choose>
                        <c:when test="${order.orderPaymentStatus==-1}">
                            未收
                        </c:when>
                        <c:otherwise>
                            清欠
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <c:forEach items="${order.orderTasks}" var="task">
                        ${task.taskProcess.processName}&nbsp;
                    </c:forEach>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <p style="height: 50px;line-height: 50px">
        <a href="javascript:;" onclick="turnTap('groupInfo',${captain.groupId})">查看该小组成员</a>
    </p>
    <table cellspacing="0" cellpadding="12" border="1">
        <thead>
        <tr>
            <th colspan="4" style="font-size: 22px;">
                工人组信息统计
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>组长姓名</td>
            <td width="100">${captain.userRealName}</td>
            <td>组长登录名</td>
            <td width="100">${captain.userLoginName}</td>
        </tr>
        <tr>
            <td>组长邮箱</td>
            <td>${captain.userEmail}</td>
            <td>组长电话</td>
            <td>${captain.userPhoneNumber}</td>
        </tr>
        <tr>
            <td width="100">该段时间内该工人组加工的总工单数</td>
            <td>
                <fmt:formatNumber value="${captain.groupOrderInfo.orders.size()}" pattern="0" maxFractionDigits="0"/>
            </td>
            <td width="100">该段时间内该工人组加工的工单总价值(￥)</td>
            <td>
                <fmt:formatNumber value="${sum}" pattern="0.00" maxFractionDigits="2"/>
            </td>
        </tr>
        </tbody>
    </table>
</div>