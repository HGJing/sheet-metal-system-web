<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div style="padding: 10px;">
    <div class="easyui-panel" style="width:400px;padding-left: 50px;padding-right: 50px;"
         data-options="title:'查询工人组详情'">
        <form id="searchGW-worker-group-form" class="easyui-form"
              data-options="novalidate:true,ajax:true">
            <table cellpadding="5" style="width: 300px;font-size: 14px;">
                <tr>
                    <td>查询工人组*:</td>
                    <td>
                        <select id="searchGWWorkerId" class="easyui-combobox" style="width:173px;"
                                name="worker-groupId"
                                data-options="required:true,missingMessage:'查询的工人组不能为空'">
                            <option value=""></option>
                            <c:forEach items="${captains}" var="captain">
                                <option value="${captain.groupId}">(id=${captain.groupId})组长：${captain.userRealName}[${captain.userLoginName}](人数:${captain.groupSize})</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>开始时间:</td>
                    <td>
                        <input id="searchGWStartTime" class="easyui-datetimebox" type="text"
                               style="width:173px;"
                               name="searchGWStartTime">
                    </td>
                </tr>
                <tr>
                    <td>结束时间:</td>
                    <td>
                        <input id="searchGWEndTime" class="easyui-datetimebox" type="text"
                               style="width:173px;"
                               name="searchGWEndTime">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="hidden" name="X_Requested_With" value="XMLHttpRequest">
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitSearch()">查询</a>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script>
    function submitSearch() {
        $('#searchGW-worker-group-form').form('submit', {
            onSubmit: function (param) {
                var isOk = $(this).form('enableValidation').form('validate');
                if (isOk) {
                    var workerGroup = $('#searchGWWorkerId');
                    var st = $('#searchGWStartTime');
                    var et = $('#searchGWEndTime');
                    var id = workerGroup.textbox('getValue');
                    var startTime = st.textbox('getValue');
                    var endTime = et.textbox('getValue');
                    var node = {
                        text: '工人组[' + workerGroup.textbox('getText') + ']',
                        attributes: {
                            url: '/order/worker-group/' + id + '/order-info.model?startTime=' + startTime + '&endTime=' + endTime
                        }
                    };
                    tabs.turnTab(node);
                }
                return false;
            }
        });
    }
</script>