<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div style="padding: 10px;">
    <c:if test="${not empty startTime}">
        <p>查询区间：
            <c:if test="${not empty endTime}">
                <fmt:formatDate value="${startTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
                到
                <fmt:formatDate value="${endTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/>
            </c:if>
            <c:if test="${empty endTime}">
                <fmt:formatDate value="${startTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/> 之后
            </c:if>
        </p>
    </c:if>
    <c:if test="${empty startTime}">
        <c:if test="${not empty endTime}">
            <p>查询区间：
                <fmt:formatDate value="${endTime}" pattern="yyyy年MM月dd日 HH点mm分ss秒"/> 之前
            </p>
        </c:if>
    </c:if>
    <c:if test="${empty startTime}">
        <c:if test="${empty endTime}">
            <p>查询区间：无时间限制</p>
        </c:if>
    </c:if>
    <table cellspacing="0" cellpadding="12" border="1">
        <thead>
        <tr>
            <th colspan="4" style="font-size: 22px;">
                工人信息统计
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>工人姓名</td>
            <td width="100">${info.workerInfo.userRealName}</td>
            <td>工人登录名</td>
            <td width="100">${info.workerInfo.userLoginName}</td>
        </tr>
        <tr>
            <td>工人邮箱</td>
            <td>${info.workerInfo.userEmail}</td>
            <td>工人电话</td>
            <td>${info.workerInfo.userPhoneNumber}</td>
        </tr>
        <tr>
            <td width="100">该段时间内工人加工的总工单数</td>
            <td>
                <fmt:formatNumber value="${info.orderNumSum}" pattern="0.00" maxFractionDigits="2"/>
            </td>
            <td width="100">该段时间内工人加工的工单总价值(￥)</td>
            <td>
                <fmt:formatNumber value="${info.orderPriceSum}" pattern="0.00" maxFractionDigits="2"/>
            </td>
        </tr>
        </tbody>
    </table>
</div>