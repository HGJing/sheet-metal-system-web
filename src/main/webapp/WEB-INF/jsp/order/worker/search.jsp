<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div style="padding: 10px;">
    <div class="easyui-panel" style="width:400px;padding-left: 50px;padding-right: 50px;"
         data-options="title:'查询工人详情'">
        <form id="searchW-worker-form" class="easyui-form"
              data-options="novalidate:true,ajax:true">
            <table cellpadding="5" style="width: 300px;font-size: 14px;">
                <tr>
                    <td>查询工人*:</td>
                    <td>
                        <select id="searchWWorkerId" class="easyui-combobox" style="width:173px;"
                                name="workerId"
                                data-options="required:true,missingMessage:'查询的工人不能为空'">
                            <option value=""></option>
                            <c:forEach items="${workers}" var="worker">
                                <option value="${worker.userId}">${worker.userRealName}[${worker.userLoginName}]</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>开始时间:</td>
                    <td>
                        <input id="searchWStartTime" class="easyui-datetimebox" type="text"
                               style="width:173px;"
                               name="searchWStartTime">
                    </td>
                </tr>
                <tr>
                    <td>结束时间:</td>
                    <td>
                        <input id="searchWEndTime" class="easyui-datetimebox" type="text"
                               style="width:173px;"
                               name="searchWEndTime">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="hidden" name="X_Requested_With" value="XMLHttpRequest">
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitSearch()">查询</a>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script>
    function submitSearch() {
        $('#searchW-worker-form').form('submit', {
            onSubmit: function (param) {
                var isOk = $(this).form('enableValidation').form('validate');
                if (isOk) {
                    var worker = $('#searchWWorkerId');
                    var st = $('#searchWStartTime');
                    var et = $('#searchWEndTime');
                    var id = worker.textbox('getValue');
                    var startTime = st.textbox('getValue');
                    var endTime = et.textbox('getValue');
                    var node = {
                        text: '工人[' + worker.textbox('getText') + ']',
                        attributes: {
                            url: '/worker/' + id + '/departure-info.model?startTime=' + startTime + '&endTime=' + endTime
                        }
                    };
                    tabs.turnTab(node);
                }
                return false;
            }
        });
    }
</script>