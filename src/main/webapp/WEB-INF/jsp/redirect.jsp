<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>加载中请稍候...</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <script src="${pageContext.request.contextPath}/resources/h5/app/lib/anjular/angular.js"></script>
    <script src="${pageContext.request.contextPath}/resources/h5/app/lib/anjular/angular-route.js"></script>
    <script src="${pageContext.request.contextPath}/resources/h5/app/lib/sui/zepto.js"></script>
    <script src="${pageContext.request.contextPath}/resources/h5/app/page/js-cover/cover-all.js"></script>
    <script src="${pageContext.request.contextPath}/resources/h5/app/lib/sui/sm.js"></script>
    <script type='text/javascript' src='${pageContext.request.contextPath}/resources/h5/app/lib/sui/sm-extend.min.js'
            charset='utf-8'></script>
</head>
<body>
<h3>第一次加载会比较慢，请耐心等候...</h3>
<script>
    sessionStorage.setItem("sms_net_path", '${projectPath}');
    location.href = '${projectPath}${url}';
</script>
</body>
</html>
