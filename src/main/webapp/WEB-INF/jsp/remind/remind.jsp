<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<div style="padding: 10px">
    <h2>提醒列表</h2>
    <div style="margin:20px 0;"></div>
    <table class="easyui-datagrid" id="remind-list-grid" title="通知信息"
           data-options="singleSelect:true,collapsible:true,pagination:true" style="width: 830px;">
    </table>
    <script>
        var rememberGrid = $('#remind-list-grid');
        window.readRemind = function (index) {
            if (!rememberGrid.innerRows) {
                return;
            }
            var rows = rememberGrid.innerRows.rows;
            var row = rows[index];
            var id = row.remindId;

            $.ajax({
                url: "${pageContext.request.contextPath}/remind/" + id + '/read',
                method: 'post',
                success: function (res) {
                    res = typeof res == 'string' ? $.parseJSON(res) : res;
                    if (!res.error) {
                        rememberGrid.datagrid("reload");
                        var not = $("#notReadCount");
                        var count = parseInt(not.html());
                        not.html(count - 1);
                    } else {
                        $.messager.alert('设置提醒已读', res.msg, 'error');
                    }
                }
            });
        };
        window.remindDetail = function (index) {
            var row = rememberGrid.innerRows.rows[index];
            if (row.remindType == 2) {
                // 这里本来该是查看订单详情，这里暂时查看留言
                $.ajax({
                    url: window.projectPath + '/order/' + row.remindTargetId + '/detail',
                    method: 'get',
                    success: function (res) {
                        res = typeof res == 'string' ? $.parseJSON(res) : res;
                        if (!res.error) {
                            $.messager.alert("获取工单["+res.order.orderNumber+"]留言", '留言(最新)：' + res.order.orderCustomerMessage, 'info');
                        } else {
                            $.messager.alert("获取工单留言", res.msg, 'error');
                        }
                    }
                });
            }else {
                $.messager.alert("操作被阻止", '该接口暂时未开放', 'info');
            }
        };
        rememberGrid.datagrid({
            url: window.projectPath + "/remind/list",
            method: 'get',
            loadFilter: function (data) {
                // console.log(data);
                var result = {
                    rows: [],
                    total: 0
                };
                if (!data.error) {
                    result.total = data.pageInfo.total;
                    result.rows = data.pageInfo.list;
                } else {
                    $.messager.alert('加载错误', '加载提醒列表出错!' + data.msg, 'error');
                }
                rememberGrid.innerRows = result;
                return result;
            },
            columns: [[
                {field: 'remindId', title: 'id'},
                {
                    field: 'remindTime', title: '时间',
                    formatter: function (value, row, index) {
                        var date = new Date(value);
                        return date.format('yyyy-MM-dd hh:mm:ss');
                    }
                },
                {field: 'remindContent', title: '内容'},
                {
                    field: 'remindStatus', title: '状态',
                    formatter: function (value, row, index) {
                        switch (value) {
                            case 0:
                                return "已读";
                            case -1:
                                return "未读";
                            default:
                                return "未知状态"
                        }
                    }
                },
                {
                    field: 'option', title: '操作',
                    formatter: function (value, row, index) {
                        var eles = '';
                        var type = row.remindTypy;
                        if (row.remindType != 0) {
                            eles = eles +
                                '<a href="javascript:;" style="color: #005;" onclick="remindDetail(' + index + ')" >查看详情</a>&nbsp;';
                        }
                        if (row.remindStatus != 0) {
                            eles = eles +
                                '<a href="javascript:;" style="color: #005;" onclick="readRemind(' + index + ')" >标为已读</a>';
                        }
                        return eles;
                    }
                }
            ]]
        });
    </script>
</div>