<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div style="padding: 10px;">
    <div class="easyui-panel" style="width:400px;padding-left: 50px;padding-right: 50px;"
         data-options="title:'修改用户密码'">
        <form id="modify-password-form" class="easyui-form" method="post"
              action="${pageContext.request.contextPath}/user/password/modify"
              data-options="novalidate:true,ajax:true">
            <table cellpadding="5" style="width: 300px;font-size: 14px;">
                <tr>
                    <td>原密码*:</td>
                    <td><input class="easyui-textbox" style="width:173px;" type="password"
                               name="oldPassword"
                               data-options="required:true,missingMessage:'原密码不能为空'"></td>
                </tr>
                <tr>
                    <td>新密码*:</td>
                    <td><input class="easyui-textbox" style="width:173px;" type="password"
                               name="" id="password-new"
                               data-options="required:true,missingMessage:'新密码不能为空',
                               validType:'lengthBetween[6,16,\'新密码\']'"></td>
                </tr>
                <tr>
                    <td>核对新密码*:</td>
                    <td><input class="easyui-textbox" style="width:173px;" type="password"
                               name="newPassword"
                               validType="equals['#password-new']"
                               data-options="required:true,missingMessage:'核对密码不能为空',"></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="hidden" name="X_Requested_With" value="XMLHttpRequest">
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="modifyPassword()">确认修改</a>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script>
    function modifyPassword() {
        $('#modify-password-form').form('submit', {
            onSubmit: function (param) {
                var isOk = $(this).form('enableValidation').form('validate');
                if (isOk) {
                    $.messager.progress({
                        title: '请稍候',
                        msg: '修改密码中...'
                    });
                }
                return isOk;
            },
            success: function (data) {
                data = $.parseJSON(data);
                $.messager.progress('close');
                if (!data.error) {
                    $.messager.alert("修改密码", data.msg, 'info', function () {
                        window.location.reload();
                    });
                } else {
                    $.messager.alert("修改密码", data.msg, "error");
                }
            }, error: function () {
                $.messager.progress('close');
            }
        });
    }
</script>