<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="add-user" style="padding: 10px;">
    <div class="easyui-panel" style="width:400px;padding-left: 50px;padding-right: 50px;"
         data-options="title:'添加用户信息'">
        <form id="add-user-form" class="easyui-form" method="post"
              action="${pageContext.request.contextPath}/user/add"
              data-options="novalidate:true,ajax:true">
            <table cellpadding="5" style="width: 300px;font-size: 14px;">
                <tr>
                    <td>用户名*:</td>
                    <td><input class="easyui-textbox" style="width:173px;" type="text"
                               name="userLoginName"
                               data-options="required:true,missingMessage:'用户名不能为空',
                               validType:['loginNameExist','loginName','lengthBetween[6,16,\'用户名\']']">
                    </td>
                </tr>
                <tr>
                    <td>密码*:</td>
                    <td><input class="easyui-textbox" style="width:173px;" type="password"
                               name="" id="password"
                               data-options="required:true,missingMessage:'密码不能为空',
                               validType:'lengthBetween[6,16,\'密码\']'"></td>
                </tr>
                <tr>
                    <td>核对密码*:</td>
                    <td><input class="easyui-textbox" style="width:173px;" type="password"
                               name="userPassword"
                               validType="equals['#password']"
                               data-options="required:true,missingMessage:'核对密码不能为空',"></td>
                </tr>
                <tr>
                    <td>角色*:</td>
                    <td><input id="add-role-selector" class="easyui-combobox" style="width:173px;"
                               name="userRole"
                               data-options="required:true,missingMessage:'角色不能为空'"></td>
                </tr>
                <tr>
                    <td>真实姓名*:</td>
                    <td>
                        <input class="easyui-textbox" type="text" style="width:173px;"
                               name="userRealName"
                               data-options="required:true,missingMessage:'真实姓名不能为空'">
                    </td>
                </tr>
                <tr>
                    <td>昵称:</td>
                    <td><input class="easyui-textbox" type="text" style="width:173px;"
                               name="userNickName"></td>
                </tr>
                <tr>
                    <td>电话:</td>
                    <td>
                        <input class="easyui-textbox" type="number" style="width:173px;"
                               data-options="validType:'reg[/^1[0-9]{10}$/,\'手机号码格式有误\']'"
                               name="userPhoneNumber">
                    </td>
                </tr>
                <tr>
                    <td>邮箱:</td>
                    <td>
                        <input class="easyui-textbox" type="email" style="width:173px;"
                               name="userEmail"
                               data-options="validType:'email',invalidMessage:'邮箱格式不正确'">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="hidden" name="X_Requested_With" value="XMLHttpRequest">
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitUserAddForm()">添加</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearUserAddForm()">清空</a>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script>
    /**
     * 角色选择框的加载
     */
    $('#add-role-selector').combobox({
        valueField: 'roleCode',
        textField: 'roleName',
        url: window.projectPath + '/user/roles',
        loadFilter: function (data) {
            if (!data.error) {
                return data.roles;
            } else {
                $.messager.alert('加载错误', '加载角色出错!' + data.msg, 'error');
                return [];
            }
        }
    });

    /**
     * 表单的提交
     */
    function submitUserAddForm() {
        $('#add-user-form').form('submit', {
            onSubmit: function (param) {
                var isOk = $(this).form('enableValidation').form('validate');
                if (isOk) {
                    $.messager.progress({
                        title: '请稍候',
                        msg: '添加用户信息中...'
                    });
                }
                return isOk;
            },
            success: function (data) {
                data = $.parseJSON(data);
                $.messager.progress('close');
                if (!data.error) {
                    $.messager.alert("添加用户", data.msg, 'info');
                    clearUserAddForm();
                } else {
                    $.messager.alert("添加用户", data.msg, "error");
                }
            }, error: function () {
                $.messager.progress('close');
            }
        });
    }
    function clearUserAddForm() {
        $('#add-user-form').form('clear');
    }
</script>