<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="add-user" style="padding: 10px;">
    <div class="easyui-panel" style="width:400px;padding-left: 50px;padding-right: 50px;"
         data-options="title:'编辑用户信息'">
        <form id="edit-user-form" class="easyui-form" method="post"
              action="${pageContext.request.contextPath}/user/edit"
              data-options="novalidate:true,ajax:true">
            <table cellpadding="5" style="width: 300px;font-size: 14px;">
                <tr>
                    <td>用户id:</td>
                    <td><input class="easyui-textbox" style="width:173px;" type="text"
                               name="userId"
                               value="${user.userId}"
                               data-options="required:true,readonly:true"></td>
                </tr>
                <tr>
                    <td>用户名*:</td>
                    <td><input class="easyui-textbox" style="width:173px;" type="text"
                               name="userLoginName"
                               value="${user.userLoginName}"
                               data-options="required:true,readonly:true,missingMessage:'用户名不能为空'"></td>
                </tr>
                <tr>
                    <td>角色*:</td>
                    <td><input id="edit-role-selector" class="easyui-combobox" style="width:173px;"
                               name="userRole"
                               value="${user.userRole}"
                               data-options="required:true,missingMessage:'角色不能为空'"></td>
                </tr>
                <tr>
                    <td>真实姓名*:</td>
                    <td>
                        <input class="easyui-textbox" type="text" style="width:173px;"
                               name="userRealName"
                               value="${user.userRealName}"
                               data-options="required:true,missingMessage:'真实姓名不能为空'">
                    </td>
                </tr>
                <tr>
                    <td>昵称*:</td>
                    <td><input class="easyui-textbox" type="text" style="width:173px;"
                               name="userNickName"
                               value="${user.userNickName}"
                               data-options="required:true,missingMessage:'昵称不能为空'"></td>
                </tr>
                <tr>
                    <td>电话:</td>
                    <td>
                        <input class="easyui-textbox" type="number" style="width:173px;"
                               value="${user.userPhoneNumber}"
                               data-options="validType:'reg[/^1[0-9]{10}$/,\'手机号码格式有误\']'"
                               name="userPhoneNumber">
                    </td>
                </tr>
                <tr>
                    <td>邮箱:</td>
                    <td>
                        <input class="easyui-textbox" type="email" style="width:173px;"
                               name="userEmail"
                               value="${user.userEmail}"
                               data-options="validType:'email',invalidMessage:'邮箱格式不正确'">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="hidden" name="X_Requested_With" value="XMLHttpRequest">
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitUserEditForm()">修改</a>
                    </td>
                </tr>
            </table>
        </form>
    </div>

</div>
<script>
    $('#edit-role-selector').combobox({
        valueField: 'roleCode',
        textField: 'roleName',
        url: window.projectPath + '/user/roles',
        loadFilter: function (data) {
            if (!data.error) {
                return data.roles;
            } else {
                $.messager.alert('加载错误', '加载角色出错!' + data.msg, 'error');
                return [];
            }
        }
    });

    function submitUserEditForm() {
        $('#edit-user-form').form('submit', {
            onSubmit: function (param) {
                var isOk = $(this).form('enableValidation').form('validate');
                if (isOk) {
                    $.messager.progress({
                        title: '请稍候',
                        msg: '修改用户信息中...'
                    });
                }
                return isOk;
            },
            success: function (data) {
                data = $.parseJSON(data);
                $.messager.progress('close');
                if (!data.error) {
                    $.messager.alert("修改用户", data.msg, 'info');
                } else {
                    $.messager.alert("修改用户", data.msg, "error");
                }
            }, error: function () {
                $.messager.progress('close');
            }
        });
    }
</script>