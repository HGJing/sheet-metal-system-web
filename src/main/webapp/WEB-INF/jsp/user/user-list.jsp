<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<div style="padding: 10px">
    <h2>用户列表</h2>
    <div style="margin:20px 0;">
        <div style=" padding-bottom: 10px;">
            <form>
                <label>
                    筛选姓名：
                    <input class="easyui-textbox" id="search-name" style="width:173px"/>
                </label>
                <label>
                    筛选角色：
                    <select id="search-roles" class="easyui-combobox" style="width:173px;">
                        <option value="">所有角色</option>
                        <option value="admin">管理员</option>
                        <option value="clerk">文员</option>
                        <option value="customer">客户</option>
                        <option value="customer_special">客户（月结）</option>
                        <option value="production_manager">生产管理</option>
                        <option value="sales_manager">销售管理</option>
                        <option value="worker">工人</option>
                        <option value="driver">司机</option>
                    </select>
                </label>
                <label>
                    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitParamsForUsers()">筛选</a>
                </label>
            </form>
        </div>
        <table class="easyui-datagrid" id="user-list-grid" title="用户信息"
               data-options="singleSelect:true,collapsible:true,pagination:true" style="width: 830px;">
        </table>
    </div>
    <script>
        var grid = $('#user-list-grid');
        window.deleteUser = function (index) {
            if (!grid.innerRows) {
                return;
            }
            var rows = grid.innerRows.rows;
            var id = rows[index].userId;
            var loginName = rows[index].userLoginName;
            $.messager.confirm("删除用户信息", "确定删除id为" + id + "，登录名为" + loginName + "的用户？", function (r) {
                if (r) {
                    $.ajax({
                        url: "${pageContext.request.contextPath}/user/delete/" + id,
                        method: 'post',
                        success: function (res) {
                            res = typeof res == 'string' ? $.parseJSON(res) : res;
                            if (!res.error) {
                                $.messager.alert("删除用户信息", res.msg, 'info');
                                grid.datagrid("reload");
                            } else {
                                $.messager.alert("删除用户信息", res.msg, 'error');
                            }
                        }
                    })
                }
            });
        };
        window.editUser = function (index) {
            if (!grid.innerRows) {
                return;
            }
            var rows = grid.innerRows.rows;
            var id = rows[index].userId;
            var name = rows[index].userLoginName;
            var node = {
                text: '[' + id + '-' + name + ']-[编辑用户]',
                attributes: {
                    url: '/user/user-editing.model?userId=' + id
                }
            };
            window.tabs.turnTab(node);
        };
        window.submitParamsForUsers = function () {
            var namebox = $('#search-name');
            var rolesbox = $('#search-roles');
            var name = namebox.textbox("getValue");
            var role = rolesbox.combobox("getValue");
            grid.datagrid({
                queryParams: {
                    userRealName: '%' + name + '%',
                    userRole: role
                }
            });
            grid.datagrid("reload");
        };
        grid.datagrid({
            url: window.projectPath + "/user/list",
            method: 'get',
            loadFilter: function (data) {
                // console.log(data);
                var result = {
                    rows: [],
                    total: 0
                };
                if (!data.error) {
                    result.total = data.pageInfo.total;
                    result.rows = data.pageInfo.list;
                } else {
                    $.messager.alert('加载错误', '加载用户列表出错!' + data.msg, 'error');
                }
                grid.innerRows = result;
                return result;
            },
            columns: [[
                {field: 'userId', title: 'id', width: 30},
                {field: 'userLoginName', title: '登录名', width: 100},
                {field: 'userNickName', title: '昵称', width: 120},
                {field: 'userRealName', title: '姓名', width: 70},
                {
                    field: 'userRoleEntity', title: '角色', width: 70,
                    formatter: function (value, row, index) {
                        return value.roleName;
                    }
                },
                {field: 'userEmail', title: '邮箱', width: 120},
                {field: 'userPhoneNumber', title: '电话', width: 120},
                {
                    field: 'userStatus', title: '状态', width: 70,
                    formatter: function (value, row, index) {
                        switch (value) {
                            default:
                            case 0:
                                return "正常";
                            case -1:
                                return "冻结";
                        }
                    }
                },
                {
                    field: 'option', title: '操作', width: 50,
                    formatter: function (value, row, index) {
                        return '\
                        <shiro:hasRole name="admin">\
                                <a class="fa fa-pencil fa-fw" style="cursor: pointer;" onclick="editUser(' + index + ')" title="编辑">&nbsp;</a>\
                                <a class="fa fa-trash-o fa-fw" style="color: #d13500;cursor: pointer;"  onclick="deleteUser(' + index + ')"title="删除">&nbsp;</a>\
                               </shiro:hasRole>';
                    }
                }
            ]]
        });
    </script>
</div>