<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="add-worker-group" style="padding: 10px;">
    <div class="easyui-panel" style="width:400px;padding-left: 50px;padding-right: 50px;"
         data-options="title:'添加工人组信息'">
        <form id="add-worker-group-form" class="easyui-form" method="post"
              action="${pageContext.request.contextPath}/user/worker-group/add"
              data-options="novalidate:true,ajax:true">
            <table cellpadding="5" style="width: 300px;font-size: 14px;">
                <tr>
                    <td>组长*:</td>
                    <td>
                        <select class="easyui-combobox" style="width:173px;"
                                name="leaderWorkerId"
                                data-options="required:true,missingMessage:'工人组长不能为空'">
                            <option value=""></option>
                            <c:forEach items="${workers}" var="worker">
                                <option value="${worker.userId}">${worker.userRealName}[${worker.userLoginName}]</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>组员*:</td>
                    <td>
                        <select class="easyui-combobox" style="width:173px;"
                                name="workersId"
                                data-options="required:true,missingMessage:'工人组员不能为空',
                                multiple:true">
                            <option value=""></option>
                            <c:forEach items="${workers}" var="worker">
                                <option value="${worker.userId}">${worker.userRealName}[${worker.userLoginName}]</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>加工流程*:</td>
                    <td>
                        <select class="easyui-combobox" style="width:173px;"
                                name="processId"
                                data-options="required:true,missingMessage:'该小组的加工流程不能为空'">
                            <option value=""></option>
                            <c:forEach items="${processes}" var="process">
                                <option value="${process.processId}">${process.processName}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="hidden" name="X_Requested_With" value="XMLHttpRequest">
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitGroupAddForm()">添加</a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearGroupAddForm()">清空</a>
                    </td>
                </tr>
            </table>
        </form>
    </div>

</div>
<script>


    /**
     * 表单的提交
     */
    function submitGroupAddForm() {
        $('#add-worker-group-form').form('submit', {
            onSubmit: function (param) {
                var isOk = $(this).form('enableValidation').form('validate');
                if (isOk) {
                    $.messager.progress({
                        title: '请稍候',
                        msg: '添加小组信息中...'
                    });
                }
                return isOk;
            },
            success: function (data) {
                data = $.parseJSON(data);
                $.messager.progress('close');
                if (!data.error) {
                    $.messager.alert("添加小组信息", data.msg, 'info');
                    clearGroupAddForm();
                } else {
                    $.messager.alert("添加小组信息", data.msg, "error");
                }
            }, error: function () {
                $.messager.progress('close');
            }
        });
    }
    function clearGroupAddForm() {
        $('#add-worker-group-form').form('clear');
    }
</script>