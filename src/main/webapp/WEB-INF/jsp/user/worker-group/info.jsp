<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="group-info" style="padding: 10px;">
    <p>工人组id：${workers[0].workerGroupId}</p>
    <table class="easyui-datagrid">
        <thead>
        <tr>
            <th field="name1">工人登录名</th>
            <th field="name2">是否组长</th>
            <th field="name3">工人名</th>
            <th field="name4">工人昵称</th>
            <th field="name5">工人电话</th>
            <th field="name6">工人邮箱</th>
        </tr>
        </thead>
        <tbody><c:forEach items="${workers}" var="worker">
            <tr>
                <td>${worker.worker.userLoginName}</td>
                <td>${worker.workerIsLeader}</td>
                <td>${worker.worker.userRealName}</td>
                <td>${worker.worker.userNickName}</td>
                <td>${worker.worker.userPhoneNumber}</td>
                <td>${worker.worker.userEmail}</td>
            </tr>
        </c:forEach></tbody>
    </table>
</div>