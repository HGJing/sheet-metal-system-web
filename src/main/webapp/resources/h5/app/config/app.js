/**
 * Created by Eoly on 2017/3/27.
 * Call http://palerock.cn
 */

var app = angular.module('app', ['ngRoute']);

// 设置页面地址文件夹
app.pagePath = 'page/';

// 设置项目后端根目录地址
// app.netPath = 'http://localhost:8080';
app.netPath = sessionStorage.getItem("sms_net_path");
if (app.netPath == null) {
    $.alert("配置出现错误，请从微信入口重新进入！")
}

app.config(function ($httpProvider) {

    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

});

// 设置模板页面后缀名
app.modelName = 'html';

// 路由的一个页面
app.Page = function (templateUrl) {
    this.path = '';
    this.templateUrl = app.pagePath + templateUrl;
    this.controller = '';

    var _this = this;
    var init = function () {
        var parts = templateUrl.split("/");
        var name = parts[parts.length - 1].split('.')[0];
        _this.path = '/' + name;
        _this.controller = name;
    };
    init();

    // 设置单个路由的页面路径
    this.setPath = function (path) {

        if (!path) {
            return this;
        }
        this.path = path;

        return this
    };

    // 设置单个路由的控制器名称
    this.setController = function (controller) {

        if (!controller) {
            return this;
        }
        this.controller = controller;

        return this
    }
};

// 路由里的一组页面
app.PageGroup = function () {

    // 页面组
    this.pages = [];

    // 默认页面路径
    this.defaultPath = '/test';

    // 设置默认页面路径
    this.setDefault = function (path) {
        for (var i = 0; i < this.pages.length; i++) {
            if (path == this.pages[i].path) {
                this.defaultPath = path;
                break;
            }
        }
    };

    // 向路由中添加页面
    this.addPage = function (templateUrl) {
        var page = new app.Page(templateUrl);
        this.pages.push(page);
        return page;
    };

    // 设置单个路由的页面路径
    this.setPath = function (index, path) {
        if (!index || !path) {
            return;
        }
        this.pages[index].path = path;
        console.log('modify path:' + path + ' successfully.')
    };

    // 设置单个路由的控制器名称
    this.setController = function (index, controller) {
        if (!index || !controller) {
            return;
        }
        this.pages[index].controller = controller;
        console.log('modify controller:' + controller + ' successfully.')
    };

    // 生成整体路由
    this.buildRouter = function (application) {
        var _this = this;
        application.config(
            function ($routeProvider) {
                for (var i = 0; i < _this.pages.length; i++) {
                    $routeProvider.when(_this.pages[i].path, {
                        templateUrl: _this.pages[i].templateUrl,
                        controller: _this.pages[i].controller
                    });
                    console.log("router page:" + _this.pages[i].path + " load successfully.");
                }
                $routeProvider.otherwise({
                    redirectTo: _this.defaultPath
                });
                console.log("default page:" + _this.defaultPath + " set successfully.");
            }
        );
        this.initController(application);
    };

    // 初始化控制器
    this.initController = function (application) {
        for (var i = 0; i < this.pages.length; i++) {
            application.controller(this.pages[i].controller, function () {
            })
        }
    }
};

// 实例化路由页面组，交移page-admin页面处理
app.pageGroup = new app.PageGroup();


// Sui框架初始化的党法，需要在每个模版页面的末尾添加（勿改动）
app.initCss = function (cssPlusPath) {
    // 修改css样式
    var cssPlus = document.getElementById("page-css");

    cssPlus.href = '';

    if (cssPlusPath) {
        cssPlus.href = app.pagePath + cssPlusPath;
    }

};
app.suiInit = function (jsPlusPath) {

    // 默认的init 方法
    var $page = $(".page-current");
    if (!$page[0]) $page = $(".page").addClass('page-current');
    var id = $page[0].id;
    if (!$page[0]) $page = $(document.body);
    var $content = $page.hasClass('content') ?
        $page :
        $page.find('.content');
    $content.scroller();  //注意滚动条一定要最先初始化

    $.initPullToRefresh($content);
    $.initInfiniteScroll($content);
    $.initCalendar($content);

    //extend
    // if ($.initSwiper) $.initSwiper($content);

    $page.trigger('pageInit', [id, $page]);

    // 增加js代码
    var jsPlus = document.getElementById("page-js");
    if (jsPlus.childNodes[0]) {
        jsPlus.removeChild(jsPlus.childNodes[0]);
    }
    if (jsPlusPath) {
        var script = document.createElement("script");
        script.src = app.pagePath + jsPlusPath;
        jsPlus.appendChild(script);
    }
};