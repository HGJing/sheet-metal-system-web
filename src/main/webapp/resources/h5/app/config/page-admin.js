/**
 * Created by Eoly on 2017/3/29.
 * Call http://palerock.cn
 */

var pageGroup = app.pageGroup;

// 添加路由页面
pageGroup.addPage('hello-page/hello.html');
pageGroup.addPage('hello-page/order.html');
pageGroup.addPage('hello-page/departure.html');
pageGroup.addPage('hello-page/task.html');
pageGroup.addPage('bind-page/bind.html');

// 设置默认页面
pageGroup.setDefault('/hello');

// 初始化路由
pageGroup.buildRouter(app);