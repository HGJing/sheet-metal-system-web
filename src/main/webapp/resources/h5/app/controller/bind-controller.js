/**
 * Created by Eoly on 2017/5/2.
 * Call http://palerock.cn
 */

app.controller('bind', function ($http, $scope) {

    $scope.userLoginName = "";
    $scope.userPassword = "";

    $scope.bind = function () {
        var userLoginName = $scope.userLoginName;
        var userPassword = $scope.userPassword;
        if (!userLoginName || !userPassword) {
            $.alert("登录名或密码不能为空！");
            return;
        }
        $http({
            method: 'post',
            params: {
                userLoginName: userLoginName,
                userPassword: userPassword
            },
            url: app.netPath + '/wechat/bind'
        }).then(function (res) {
            if (!res.data.error) {
                $.alert("绑定成功！", function () {
                    location.href = app.netPath + "/wechat/redirect";
                });
            } else {
                $.alert(res.data.msg);
            }
        }, function (res) {
            $.alert("网络或设置错误")
        })
    };
    // 更多...
});