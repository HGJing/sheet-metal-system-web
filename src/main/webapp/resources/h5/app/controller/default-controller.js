/**
 * Created by Eoly on 2017/5/2.
 * Call http://palerock.cn
 */

// 设置默认控制器（该控制器中使用$scope存放的数据在所有页面均可以共享）
app.controller('default', function ($http, $scope) {

    $scope.pageTitle = 'hello!';
    $scope.your = 'some content';
    // 初始化sui框架
    $scope.suiMobileInit = function (jsPath) {
        app.suiInit(jsPath);
    };

    $scope.initCss = function (cssPath) {
        app.initCss(cssPath);
    };

    $scope.$on('$viewContentLoaded', function (event) {
        var jsPlus = document.getElementById("page-js");
        if (jsPlus.childNodes[0]) {
            jsPlus.removeChild(jsPlus.childNodes[0]);
        }
        $(document).off('infinite', '.infinite-scroll');
    });

    $scope.setTitle = function (title) {
        $scope.pageTitle = title;
    };

    $scope.user = null;

    $scope.initUser = function () {
        $http({
            method: 'get',
            url: app.netPath + '/wechat/current-user'
        }).then(function (res) {
            if (!res.data.error) {
                $scope.user = res.data.user;
                // console.log($scope.user);
                // 暂时保存用户信息
                sessionStorage.setItem('wc_cu_user', JSON.stringify(res.data.user))
            } else {
                location.href = app.netPath + "/wechat/redirect";
            }
        }, function (res) {
            $.alert("网络或设置错误")
        })
    };

    // 更多...
});