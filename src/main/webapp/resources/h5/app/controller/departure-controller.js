/**
 * Created by Eoly on 2017/5/2.
 * Call http://palerock.cn
 */


app.controller('departure', function ($http, $scope) {


    $scope.departures = [];
    $scope.selectDeparture = null;
    var page = 1;
    var row = 10;
    var isLoading = false;

    $scope.init = function () {
        $(document).on('infinite', '.departure-list-content', function () {
            if (isLoading) {
                return;
            }
            $scope.nextDepartures();
        });
        $(document).on('refresh', '.pull-to-refresh-content', function (e) {
            $scope.departures = [];
            page = 1;
            isLoading = false;
            $('.infinite-scroll-preloader').show();
            $.attachInfiniteScroll($('.infinite-scroll'));
            $.toast("刷新中...", 500);
            $scope.nextDepartures();
            // 加载完毕需要重置
            $.pullToRefreshDone('.pull-to-refresh-content');
        });
        if (!$scope.user) {
            $scope.initUser();
        }
    };

    /**
     * 获取接下来的派车单
     */
    $scope.nextDepartures = function () {
        isLoading = true;
        $http({
            method: 'get',
            url: app.netPath + '/departure/driver/list',
            params: {
                page: page,
                rows: row
            }
        }).then(function (res) {
            var p = $('.infinite-scroll-preloader');
            var c = $('.content');
            if (!res.data.error) {
                for (var i = 0; i < res.data.pageInfo.list.length; i++) {
                    $scope.departures.push(res.data.pageInfo.list[i]);
                }
                page++;
                isLoading = false;
                if (p.offset().top < c.height()) {
                    $scope.nextDepartures();
                }
            } else {
                if (p.offset().top >= c.height()) {
                    $.toast("没有更多派车单了", 500);
                }
                // 加载完毕，则注销无限加载事件，以防不必要的加载
                $.detachInfiniteScroll($('.infinite-scroll'));
                // 删除加载提示符
                p.hide();
            }
        }, function (res) {
            // 加载完毕，则注销无限加载事件，以防不必要的加载
            $.detachInfiniteScroll($('.infinite-scroll'));
            // 删除加载提示符
            $('.infinite-scroll-preloader').hide();
        })
    };

    /**
     * 查看派车单详情
     * @param departure
     */
    $scope.openDepartureDetail = function (departure) {
        if (!departure) {
            return;
        }
        $scope.selectDeparture = departure;
        // console.log(departure);
        $.popup('.popup-detail');
    };

    /**
     * 确认接单
     * @param departure
     */
    $scope.receive = function (departure) {
        $.confirm('是否确认接单？', function () {
            $http({
                method: 'post',
                url: app.netPath + '/departure/' + departure.departureId + '/make-receipt'
            }).then(function (res) {
                $.toast(res.data.msg, 1200);
                if (!res.data.error) {
                    departure.departureStatus = 1;
                }
            }, function (res) {
                $.alert("网络或设置错误");
            });
        })
    };

    $scope.resultOfOvertime = function (departure) {
        $.prompt('请输入超时原因(50字内)：', '超时原因填写', function (msg) {
            if (msg == null || msg == '') {
                $.toast("超时原因不能为空！", 1000);
            }
            $http({
                method: 'post',
                url: app.netPath + '/departure/' + departure.departureId + '/fill-result',
                params: {
                    result: msg
                }
            }).then(function (res) {
                $.toast(res.data.msg, 1200);
                if (!res.data.error) {
                    departure.departureOvertimeResult = msg;
                }
            }, function (res) {
                $.alert("网络或设置错误");
            })
        });
    }
});
app.filter('getDepartureTitle', function () {
    return function (d) {
        return '到：' + d.departureAimPlace;
    }
});

app.filter('getDepartureStatus', function () {
    return function (d) {
        switch (d.departureStatus) {
            case 0:
                return '未接';
            case 1:
                return '已接';
            case 2:
                return '送货中';
            case 3:
                var overtime = '';
                if (d.departureRealCostTime != null || d.departureCostTime - d.departureRealCostTime < 0) {
                    overtime = '(超时)'
                }
                return '货到' + overtime;
            default:
                return '--';
        }
    }
});