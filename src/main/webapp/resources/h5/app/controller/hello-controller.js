/**
 * Created by Eoly on 2017/5/2.
 * Call http://palerock.cn
 */

app.controller('hello', function ($http, $scope) {

    $scope.init = function () {
        if (!$scope.user) {
            $scope.initUser();
        }
    };

    $scope.unbind = function () {
        $.confirm('是否确定要解绑？', function () {
            $http({
                method: 'post',
                url: app.netPath + '/wechat/unbind'
            }).then(function (res) {
                if (!res.data.error) {
                    $.alert(res.data.msg, function () {
                        location.href = app.netPath + "/wechat/redirect";
                    });
                } else {
                    $.alert(res.data.msg);
                }
            }, function (res) {
                $.alert("网络或设置错误");
            })
        })
    }
});

/**
 * 获取用户角色
 */
app.filter('getUserRole', function () {
    return function (u) {
        if (!u) {
            return;
        }
        switch (u.userRole) {
            case 'customer':
            case 'customer_special':
                return '客户';
            case 'worker':
                return '工人';
            case 'admin':
                return '管理员';
            case 'clerk':
                return '文员';
            case 'driver':
                return '司机';
            case 'production_manager':
                return '生产管理';
            case 'sales_manager':
                return '销售管理';
            default:
                return '未知角色';
        }
    }
});