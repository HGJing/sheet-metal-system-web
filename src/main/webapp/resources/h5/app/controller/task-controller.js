/**
 * Created by Eoly on 2017/5/2.
 * Call http://palerock.cn
 */


app.controller('task', function ($http, $scope) {


    $scope.tasks = [];
    $scope.selectTask = null;

    $scope.init = function () {
        $(document).on('refresh', '.pull-to-refresh-content', function (e) {
            $.toast("刷新中...", 500);
            $scope.nextTasks();
            // 加载完毕需要重置
            $.pullToRefreshDone('.pull-to-refresh-content');
        });
        if (!$scope.user) {
            $scope.initUser();
        }
    };

    /**
     * 获取接下来的任务流程
     */
    $scope.nextTasks = function () {
        $http({
            method: 'get',
            url: app.netPath + '/order/task/list/leader'
        }).then(function (res) {
            if (!res.data.error) {
                $scope.tasks = res.data.list;
            }
        }, function (res) {
        })
    };

    /**
     * 查看任务流程详情
     * @param task
     */
    $scope.openTaskDetail = function (task) {
        if (!task) {
            return;
        }
        $scope.selectTask = task;
        // console.log(task);
        $.popup('.popup-detail');
    };

    /**
     * 确认接单
     * @param task
     */
    $scope.finishTask = function (task) {
        $.confirm('是否确认完成？', function () {
            $http({
                method: 'post',
                url: app.netPath + '/order/task/' + task.taskAssignmentId
                + '/finish?groupId=' + task.taskWorkerGroupId
            }).then(function (res) {
                $.toast(res.data.msg, 1200);
                if (!res.data.error) {
                    $scope.nextTasks();
                    $.closeModal('.popup-detail');
                }
            }, function (res) {
                $.alert("网络或设置错误");
            });
        })
    };
});
app.filter('getTaskStatus', function () {
    return function (t) {
        switch (t.taskStatus) {
            case -1:
                var message = !t.preDemandNum ? '进行中' : '等待中';
                return "未完成：" + message;
            case 0:
                return "已完成";
            default:
                return "未知"
        }
    }
});