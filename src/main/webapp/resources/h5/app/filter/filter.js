/**
 * Created by Eoly on 2017/5/2.
 * Call http://palerock.cn
 */

// 过滤器
app.filter('statusFilter', function () {
    return function (e) {

        switch (e) {
            case 0:
                return '待完成';
            case 1:
                return '已完成';
            case 2:
                return '失效';
            default:
                return '未知状态'
        }
    }
});
app.filter('deadlineChange', function () {
        return function (e) {
            if (e) {
                if (e <= 0) {
                    return;
                }
                else {
                    return '截止时间还有' + e + '天';
                }
            }

        }
    }
);
app.filter('distanceChangeFilter', function () {
    return function (e) {
        return (e / 1000.00).toFixed(2) + 'km';
    }
});

app.filter('parsePriceFilter', function () {
    return function (e) {

        if (e.minPrice == null && e.maxPrice == null) {
            return '无限制'
        }

        if (e.minPrice == null) {
            return '<¥' + e.maxPrice;
        } else if (e.maxPrice == null) {
            return '>¥' + e.minPrice;
        }

        return '¥' + e.minPrice + '--' + e.maxPrice;
    }
});