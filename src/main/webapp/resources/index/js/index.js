/**
 * Created by Eoly on 2017/12/11.
 */
$(document).ready(function () {

    var tabs = window.tabs = $('#tab');

    /**
     * 跳转或新建tab
     * @param node {object}
     *        {
     *          text:标题,
     *          attributes:{
     *              url:链接地址
     *          }
     *        }
     */
    tabs.turnTab = function (node) {
        // console.log(node);
        var tab = tabs.tabs('getTab', node.text);
        if (tab) {
            // 选择被点击目录对应的选项卡
            tabs.tabs('select', node.text);
            tab.panel('refresh');
            return;
        }
        if (node.attributes && node.attributes.url) {
            tabs.tabs('add', {
                title: node.text,
                href: window.projectPath + node.attributes.url,
                closable: true
            });
        }
    };

    /**
     * 刷新指定标签
     * @param text
     */
    tabs.refreshTab = function (text) {
        var tab = tabs.tabs('getTab', text);
        if (tab) {
            tab.panel('refresh');
        }
    };

    tabs.refreshCurrent = function () {
        var tab = tabs.tabs('getSelected');
        if (tab) {
            tab.panel('refresh');
        }
    };


    tabs.closeCurrent = function () {
        var tab = tabs.tabs('getSelected');//获取当前选中tabs
        var index = tabs.tabs('getTabIndex', tab);//获取当前选中tabs的index
        tabs.tabs('close', index);//关闭对应index的tabs
    };

    window.turnTap = function (optionName, key) {
        var node = null;
        switch (optionName) {
            case 'groupInfo':
                node = {
                    text: '[' + key + ']-[查看小组信息]',
                    attributes: {
                        url: '/user/worker-group/info.model?groupId=' + key
                    }
                };
                break;
            case 'remind': {
                node = {
                    text: '通知提醒',
                    attributes: {
                        url: '/remind/remind.model'
                    }
                };
                break;
            }
            case 'departureDetail': {
                node = {
                    text: '[id=' + key + ']派车单详情',
                    attributes: {
                        url: '/departure/' + key + '/detail.model'
                    }
                };
                break;
            }
            default:
                break;
        }
        if (node) {
            tabs.turnTab(node);
        }
    };

    // 初始化操作树
    $('#option-tree').tree({
        url: window._directory_tree_path,
        method: 'get',
        loadFilter: function (data) {
            if (!data.error) {
                return data.dataList;
            } else {
                $.messager.alert('加载错误', '加载操作目录出错!' + data.msg, 'error');
                return [];
            }
        },
        onClick: function (node) {
            tabs.turnTab(node);
        }
    });

    // 初始化tab
    tabs.tabs('add', {
        title: '欢迎',
        href: window.projectPath + '/welcome.model'
    });


    // 窗口大小改变事件
    $(window).resize(function () {
        tabs.tabs('resize');
    });


    Date.prototype.format = function (format) {
        var o = {
            "M+": this.getMonth() + 1, //month
            "d+": this.getDate(), //day
            "h+": this.getHours(), //hour
            "m+": this.getMinutes(), //minute
            "s+": this.getSeconds(), //second
            "q+": Math.floor((this.getMonth() + 3) / 3), //quarter
            "S": this.getMilliseconds() //millisecond
        };
        if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
            (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)if (new RegExp("(" + k + ")").test(format))
            format = format.replace(RegExp.$1,
                RegExp.$1.length == 1 ? o[k] :
                    ("00" + o[k]).substr(("" + o[k]).length));
        return format;
    };
    Math.between = function (a, arr, cb) {
        var length = arr.length;
        for (var i = 0; i < length; i++) {
            if (a >= arr[i] && i + 1 < length && a < arr[i + 1]) {
                return cb.call(window, i, arr[i]);
            }
            if (a >= arr[i] && i == length - 1) {
                return cb.call(window, i, arr[i]);
            }
        }
    };

    // 注册注销
    $("#logout").click(function () {
        $.messager.confirm('提示', '确认注销?', function (r) {
            if (r) {
                $.ajax({
                    url: window.projectPath + "/user/logout",
                    method: 'get',
                    success: function (res) {
                        if (!res.error) {
                            location.reload();
                        }
                    }
                });
            }
        });
    })
});