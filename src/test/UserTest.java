import com.alibaba.fastjson.JSON;
import org.apache.shiro.SecurityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import web.sms.mvc.dao.*;
import web.sms.mvc.entity.Departure;
import web.sms.mvc.entity.TaskAssignment;
import web.sms.mvc.entity.WorkOrder;
import web.sms.mvc.entity.WorkerGroup;
import web.sms.mvc.service.UserService;
import org.apache.shiro.mgt.SecurityManager;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 时间： 2017/12/12
 *
 * @author Eoly
 */
@RunWith(SpringJUnit4ClassRunner.class)

@ContextConfiguration(locations = {
        "classpath:spring-config.xml",
        "classpath:spring-shiro.xml",
        "classpath:spring-mybatis.xml"
})
public class UserTest {

    Logger logger = LoggerFactory.getLogger(UserTest.class);

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private WorkOrderMapper workOrderMapper;

    @Autowired
    private WorkerGroupMapper workerGroupMapper;

    @Autowired
    private TaskAssignmentMapper taskAssignmentMapper;

    @Autowired
    private DepartureMapper departureMapper;

    @Test
    public void testSelectAuthenticationByLoginName() {
        logger.warn(JSON.toJSONString(userMapper.selectAuthenticationByLoginName("admin")));
    }

    @Test
    public void testSelectByPrimaryKey() {
        logger.warn(JSON.toJSONString(userMapper.selectByPrimaryKey(1)));
    }

    @Test
    public void testSelectByLoginName() {
        logger.warn(JSON.toJSONString(userMapper.selectByLoginName("admin")));
    }

    @Test
    public void testSelectUserId() {
        logger.warn(userMapper.selectUserIdByUserLoginName("asdsad").toString());
    }

    @Test
    public void testSelectOrderByCustomerId() {
        logger.warn(JSON.toJSONString(workOrderMapper.selectOrderByCustomerId(
                0, 4, 5)));
    }

    @Test
    public void testSelectAllOrders() {
        logger.warn(JSON.toJSONString(workOrderMapper.selectAllOrders(0, 4)));
    }

    @Test
    public void testSelectAllGroupCaptains() {
        logger.warn(JSON.toJSONString(workerGroupMapper.selectAllGroupCaptains()));
    }

    @Test
    public void testSelectOrderByWorkerId() {
        logger.warn(JSON.toJSONString(workOrderMapper.selectOrderByWorkerId(null, null, 14)));
    }

    @Test
    public void testSelectByLeaderId() {
        logger.warn(JSON.toJSONString(taskAssignmentMapper.selectByLeaderId(14)));
    }

    @Test
    public void testSelectByGroup() {
        logger.warn(JSON.toJSONString(taskAssignmentMapper.selectTasksByGroupId(13)));
    }

    @Test
    public void selectDriverDepartureInfo() {
        logger.warn(JSON.toJSONString(departureMapper.selectDriverDepartureInfo(
                8,
                new Date("Fri Jan 02 17:48:00 UTC 2017"),
                new Date("Fri Jan 15 17:48:00 UTC 2018")
        )));
    }

    @Test
    public void selectWorkerOrderInfoByWorkerId() {
        logger.warn(JSON.toJSONString(workOrderMapper.selectWorkerOrderInfoByWorkerId(
                14, null, null
        )));
    }

    @Test
    public void selectOrdersByAttribute() {
        WorkOrder order = new WorkOrder();
        order.setOrderPaymentStatus(WorkOrder.PAID_STATUS);
        logger.warn(JSON.toJSONString(workOrderMapper.selectOrdersByAttribute(order, 0, 10
        )));
    }

    @Test
    public void selectWorkerCaptainWithinTime() {
        logger.warn(JSON.toJSONString(workerGroupMapper.selectWorkerCaptainWithinTime(
                15, null, null, true)));
    }
}
